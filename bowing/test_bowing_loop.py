#!/usr/bin/env python3

from matplotlib import pyplot as plt
import numpy as np
import bowing_loop

a = np.array([[1,2,3,4],[5,6,-7,8.0]], dtype=np.float32)

bowing_loop.set_layer_weights(0, a)

#print(bowing_loop.run(a))

# y = bowing_loop.test_resonator_bank();
# plt.plot(y)
# plt.show()

# v = bowing_loop.test_vector(5)
# print(v)

# dvpfd = bowing_loop.view_bowing_policy()
# plt.plot(dvpfd[:,0], dvpfd[:,2])
# plt.show()

y = bowing_loop.test_bowing()
dv, bp, rv, bv = y.T
plt.subplot(221)
plt.plot(dv, label='dv')
plt.subplot(222)
plt.plot(bp, label='bp')
plt.subplot(223)
plt.plot(rv, label='rv')
plt.subplot(224)
plt.plot(bv, label='bv')
plt.legend()
plt.show()
