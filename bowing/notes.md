
## Notes on implementation

Before losing track, I'll note here some "tricks" employed during
development, of which many I still have to see if they are necessary
or not.

* Directly controlling each modal force, or control a general force
  amplitude value and a distribution over the modes normalized to 1.

* Maintain energy relations by using only velocity-controlling force
  to stimulate string.  Result: this is too limiting, as the string
  must be able to exert stored energy as well, the simulation has
  difficulty reaching sufficient amplitude.  However, removing this
  constraint allows too-strong signals.  Work in progress, have to
  find a way of limiting the energy, perhaps by energy tracking, or by
  ensuring the Coulomb inequality properly.

* Enforce constraint that relation π(0,p)=0 (passes through the
  origin) by an extra optimisation step on the actor.  This is to
  ensure that when there is no velocity difference, no forces are
  exerted, which is physically realistic for friction.  But it's not
  sure that this cannot simply be inferred, or if it needs to be
  enforced.  This represents some "prior" knowledge, but at the same
  time it is fundamental to the bowing paradigm and therefore makes
  sense to enforce in order to avoid finding "other" solutions
  available within the domain.

* Distance metrics: still unsure which is best, comparing
  Itakura-Saito with simple mean squared error, but originally was
  planning to use a learned distance.  In the latter category, I have
  tried an autoencoder but realized this sort of collapses to MSE in
  the single-sample case (delta function), which was not appropriate
  before with Hamming window and frequency guessing, but may work
  better with the harmonics matching and Hann window (as noted in my
  numerical evaluation which showed clear -- and surprising to me --
  disadvantages of Hamming window).  Ultimately starting to see the
  possible advantage of looking at Wasserstein distance since it would
  be a learned distance that does not collapse to MSE like the
  autoencoder and instead keep a wider gradient.  The idea of learned
  distance is of course to take into account a *family* of target
  spectra instead of just one.

* Within Itakura-Saito: using an extra punishment for when amplitude
  is completely different.  One that is strong when the overall
  amplitudes are different, but quickly diminishes to let the
  difference in spectral shape take over under some threshold.  Note
  that we are actually using log-Itakura-Saito since it has nicer
  slopes on simple sine tests, but perhaps this should be verified.
  
* Just implemented Wasserstein distance, seems to work quite well.
  
* Adhere to Coulomb's law by limiting the output range and multiplying
  by pressure.  Also enforce a symmetric relation by learning only
  positive differential velocities and post-applying the sign.  To
  test: this may make enforcing the π(0,p)=0 constraint unnecessary.
  Note, this appears to cause it to take advantage of the imposed hard
  boundary at zero.  I will try a soft constraint approach by training
  the actor to enforce symmetry.

* Replay buffer running out of space and forgetting old data.  Critic
  is poorly approximating the data that I see in the accumulated grid,
  but also the replay buffer is dropping old samples to maintain a
  memory cap.  Perhaps a new approach to collecting and training
  against the replay buffer is needed.  Could write to HDF5 so as not
  to run out of space while capping run-time memory usage.  I will at
  least save it to disk and try to train the critic independently,
  because it seems that it is coming up with an overly "smooth"
  approximation which is not properly describing the borders I see in
  the dv/f graph, particularly in the f-axis.  Note, it may *need* to
  forget old data, but due to the number of samples we require at this
  sampling frequency, I imagine it's just not enough, and increasing
  the buffer size makes me OOM, so direct-to-disk might be the only
  option.

* Critic needed more layers.  Worth spending the time saving the
  replay buffer and checking that the critic is actually learning it!
  Not sure how I would do this if I hadn't found a way to visualise
  it.

* Encourage symmetry by adding inverted dv observation and f action to
  the critic training data, because we know a priori it must lead to
  the same spectrum.

* With mean squared error on control of a single dv/f relation it
  seems to work well enough, but when trying to control all modes, or
  when using Wasserstein distance, it seems to prefer to create a pure
  damping effect, and keeps trying to exaggerate its steepness but
  never triggering or developing the Stribeck corner.  (Even
  surpressing it when it existed before.)  Possibly this is at the end
  of the day an easier gradient to follow, even if it never makes the
  target.  No choice but to inject some "prior knowledge" of how to
  form a Stribeck curve?  Idea for this, similar to the actor(0)[0]
  constraint, but constrain to [-1,0] and [1,0].  The other way would
  be to add a penalty to the reward.  The former is like helping it
  find the right mountain to climb, while the latter is like reshaping
  the mountains.  Remember, there really exists a "cliff" or "ridge"
  to find and hang onto, where the corner is triggered, but just the
  right amount.  The other effect is that the desired oscillations are
  found but are much too "loud" and it seems to get stuck there.  So
  these two failure modes need to be avoided.


Ultimately if I can get one configuration to work I can write a
tentative paper for December, so I am not concerned with resolving all
these unknowns right away.  Once they are narrowed down and understood
I can even do a long hyperparameter search to see what really works
best.

## On action noise

Concerned about introducing a lot of noise into the resulting spectra,
I initially moved the action noise out of the principal processing
loop and made it a single noisy constant added the selected action
throughout the whole loop.  However, to my surprise moving it back in
improved performance.  This can be understood because adding a
constant introduces a global offset, however the action function
should be symmetric about the origin.  Indeed, this caused
particularly bad learning issues when the symmetry constraint was
introduced.

Simply adding noise in the main loop is one solution, another is to
take advantage of that symmetric property and still only add noise
outside the loop, but add it as a random *scaling* instead of offset.
Adding an offset is weird in a way because the curve is not supposed
to have a global offset, but rather be centered at zero.  It seems
that this does not help it find useful non-linearities however, the
curve remains somewhat straight.  I will try logarithm noise for the
scaling to occasionally give it larger amplitudes.

A third idea is that although noise is beneficial, it also adds
uncertainty to the spectra because it destroys the link between the
action and the long-term spectral result.  An idea was to add noise
but keep the same "noise relationship" throughout the cycle, by
sampling noise and re-using it through the step as a function of dv.
This does help to find non-linearities but it's not clear that it
leads to "useful" noise.

I am back to adding simple noise, as it should help inform the
algorithm of the gradient with respect to dv, however I am trying very
*small* noise just to give it a hint but try not to disturb the
spectral result much.  However, it seems to be having difficulty
extending the amplitude of the dv/f curve.

In any case these experiments led me to understand that the noise is
critical not just for individual action steps, but for finding good
non-linearities, ie curve shapes.  In other words just adding Gaussian
noise or a random walk is not so interesting, what is needed is to
give the network examples of poentially useful shapes.  This is a bit
disappointing because ideally it would figure this out from scratch.
It does, in fact, figure it out from scratch but it is very slow to
find non-linearities and to amplify the function in the symmetric case
when offsets are not used.

Perhaps what is needed is not random *actions* but random *policies*,
ie. randomization in the actor weights.  We could also remove the
biases from the actor network since we want things to be centered at
zero anyway.  Looking into also whether batch normalization is
slowing down the learning rate of the actor network.

On the other hand, perhaps if the noise is added over the whole
window, it really amounts to just overlayed noise.. as long as the
noise doesn't completely overload the shape of the curve, it should
sort of average out both in the time and frequency domains.. true?
Maybe I should check this empirically outside the algorithm.
It's "ok" to be noisy, it averages out!

Maybe I should add the *clean* action to the replay buffer? No!
Because the idea is to develop a probability over a range of
actions. Not, an action can give several results, but changing actions
can change the result, on average.  Adding the clean action therefore
doesn't allow the actor weights to grow, because it never sees the
results for different values.

Another attempt: even though just normal additive noise seemed to work
best, it did not seem to lead to development of non-linearities.
Since the point of noise is to introduce more examples of possible
useful "shapes" to the curve, what we want is something that will give
*spurious* examples throughout the stream.  I tried replacing simply
normally-distributed noise with salt & pepper noise (an impulse
thresholded by some probability) and this seems to have helped, still
verifying.  It gives examples of a "single" spike in some direction,
rather than something that can be too easily averaged out.  Now I
realize that what is needed is spurious examples in time and frequency
-- this spurious impulse should also be filtered by a
randomly-changing low-pass filter in order to allow for examples of
random peaks in frequency/amplitude space, i.e., specifically examples
of random locations in the amplitude spectrum.

No, okay finally the salt & pepper noise, Gaussian-shaped or not, did
not really work.  I think it's because, although it does succeed in
giving a more variety of "examples", they are essentially "lies", the
recorded data says "if you use this force you will get this spectrum",
but this is not really true since the spectrum is the result of the
modified curve over the whole step.  In any case, this goes in the
replay buffer, but these "noisy" forces are not evenly distributed,
and so don't get canceled out.  The mean is no longer relevant.  On
the other hand pure normally-distributed noise seems to work better,
probably because it adds variety but in such a way that many samples
will lead to a mean that has significance for the resulting spectra.

## Controlling multiple modes

The algorithm seems to succeed for a single action value and a static
distribution of modal forces. However, when given the liberty to
control all forces, it appears to produce "curvy" shapes but does not
develop corners.  This is basically because since it has direct
control over the modes, it does not need a sharp non-linearity to
stimulate higher modes.  The curves are likely only due to the
action(0)[0] constraint anyway.

To be seen if this is somehow different in the distribution-control
mode.  (Not sure of the best name for this idea--one control value,
and a single "multiplier" that is sensitive to differential velocity.)

In the latter case, it also seems to never trigger much in the way of
higher modes, however, I believe this is just due to the fact that I
am multiplying two initially very small values, therefore it never
stimulates the string sufficiently to get much of a response.  I am
trying now to drastically increase the noise to compensate.

Later: I now understand a bit better the role of action noise here.
Especially in the pre-defined distribution condition where the modes
are excited at the correct (relative) amplitudes, pure noise already
pretty well stimulates the modes with the correct profile, and it is
just a matter of finding the right global amplitude.  The idea now of
trying input noise is to see if the network can use the noise to tune
the amplitudes of each mode while still regulating the dv/f curve.  In
my head it makes sense that it would tune the modal distribution just
in response to noise, and as the dv/f curve approaches a useful shape,
reliance on noise should decrease.  However, this has yet to be shown.

A problem seems to be that it does sufficiently well just pushing
either a constant force or a noisy force at the modes to be able to
simulate the spectrum that I want, so getting it to develop a corner
is not easy.  Perhaps pinning the sides and center is the only way,
but let's keep trying.

## Reward structure, symmetry, etc.

Found that adding "input noise" instead of action noise might help,
still to prove.  However, training with a symmetry-enforcing gradient
seems to stiffle ability to develop the corner. INterestingly the
critic seems to have a small gradeitn towards the bottom-right, in
other words it's possible the gradient indicates an advantage to the
corner, but the symmetry constrain is blocking it from going in that
direction.  I will next try with the other symmetry method, by data
augmentation.

I am starting to understand the reward/advantage idea using TD-gamma.
So, when you know something for sure, you can give it a reward, giving
it a smaller reward and making up for it by the predicted value allows
a kind of "soft evidence", where you allow hard data to stand out but
slowly it fills in the missing information by an estimate.  So having
a bit of that seems to help but it's not sure.  It also works well
always using a "hard" evidence approach (pure reward).

Note that for the parameter noise to work, if we see the curve as a
piecewise linear approximation, the nodes have to already be
relatively in the correct position so that pushing it up and down and
side to side *around the correct location* actually provides useful
examples.  This explains why the network needs to go through several
cycles of correct and incorrect behaviour -- during less good
behaviour, it may be simply pushing nodal points around in a way that
does not show much difference but is locating them such that the
gradient can eventually push them in more useful directions.  In that
sense perhaps a smaller and simpler network might actually converge
faster.

Another point, since the network doesn't have access to the internal
state of the string (and we don't want to do that), it may be strongly
affected by previous steps.  This is fine in most cases since we want
to develop a continuous control law anyways, but in the case that the
algorithm has been recently testing very strong, constant stimulation,
it seems to continue to oscillate very strongly even after the curve
has moved back toward the center and is no longer applying strong
forces.  This leads to "bad" data being entered into the replay
buffer.  Now trying a general penalty for action amplitude to
encourage it to only apply strong forces sparingly. So far, along with
symmetric data augmentation, this seems to be forcing it to shape the
action somehow, although it is trying to cheat by applying an offset
to one side.. have to wait longer for real results.  Already at 200
steps (windows, should I call them frames?) the action appears to be a
mix of signal and noise.  It does go to one side but eventually
corrects itself which is good, but still no corner.  One slowly
developing, let's see.  Hopefully it can find ways to "cheat" the
penalty by lowering parts of the curve that are outside the range
(though not sure since they are sampled less), which would lead to a
corner.  In other words it should eventually find ways to apply force
when it wants but go closer to zero away from the important part,
which is exactly what we want.  It still wants to apply an offset
though, trying again with stronger penalty.

This didn't really work well, in fact it became a very strong
"sigmoid" curve.  It seems the weights were pushing it higher and
higher and it was only limited by the action bounds.  After a reset,
it had a hard time recovering, presumably because the weights were
just too high.  It made me realize that what I really want is to stop
it from growing without bound.  So now trying a weight penalty
instead, hopefully it has a similar effect to the force magnitude
penalty but is even more global.  This should also have the bonus
effect of making it eventually want to diminish the noise weight.

Nope, it ended up again in the extreme configuration.  It just finds a
way to get there while diminishing the weights.  Trying now with
directly using reward instead of the n-step "advantage" but this
doesn't appear to be too promising.  Also doesn't work.  I see no
alternative but to constrain to zero at the edges of the dv space.

Okay I just discovered I forgot to take the square of the weights so I
was never testing the weight penalty properly.  Also to test, L1
weight penalty for sparsity.  This might force it to "choose" between
the curve amplitude and the noise.  Oh my god, in fact, the fact that
I was just minimizing the weights is probably what drove it to very
large forces.  The whole day wasted.. XD

Okay for the night, at 330 steps the L2 weight penalty is doing
something interesting but it still wants to push the forces very high.
I might still work, but in the interests of hopefully seeing a good
result tomorrow morning I will leave it running with the 0, edge, and
symmetry constraints turned on.  I will try again without them during
the day.  I want to verify that at least the input noise is an
interesting idea, that it can learn to use it and then diminish it at
the end.

Didn't really work..

Just thinking, the critic is based on estimating predicted reward
based on the current observation and an action.  Due to noise, this
prediction is quite bad, however noise is necessary for exploration.
Basically we take the next observation and we predict the target Q for
what action would be taken next.  I wonder if we could improve this
prediction if the critic had access to the internal state of the
string, even if the actor doesn't have it.  In theory the actor should
be able to come up with a perfect action if it knows everything going
on the string, although that is not what we are looking for.  However,
there is no problem giving the *critic* access to this information to
help it determine the *value* of an action.

When I finally do a series of tests, to include:

* Penalties:
  - Weight decay; L2, L1, or none.
  - Action penalty
  - Weird penalty combining dv and action; ($-dv^2 * sqrt(a)$)
* Constraints, combinations of:
  - 0
  - edges
  - symmetric
* Noise, combinations of:
  - input noise
  - action noise (normal)
  * parameter noise (laplace)
* Reward type:
  - direct reward
  - n-step td-gamma advantage
  - zeros (+penalty) and final reward in each step
  - zeros for all steps except last, short episodes
    (can we get away with shorter steps in that case? -- due to inertia the effects of a policy are cumulative so maybe it can learn to assign blame to an earlier step)
* Conditions:
  - veldiff_only
  - modal distribution
  - different spectra
  - multiple spectra
  - wassertein distance; continuous, staggered, pre-trained
  - window size (x1, x2, x3, x4..)
  - policy blending to avoid impulses
  - reset at each step or not
  - reset after detecting high amplitude

## References

The equation for REINFORCE given in the drawing paper is also found in
[this earlier NIPS
paper](http://incompleteideas.net/papers/SMSM-NIPS99-submitted.pdf),
definitely to read more thoroughly.

[This
answer](https://stats.stackexchange.com/questions/340987/how-can-i-understand-reinforce-with-baseline-is-not-a-actor-critic-algorithm?rq=1)
outlines the key difference between REINFORCE and Actor-Critic, it is
about waiting or not for the episode to be complete.

It seems to indicate that it is important for the critic to update
faster than the actor.

## Papers addressing physical modeling and machine learning related to audio

[This paper](https://arxiv.org/abs/1811.00334) discusses the use of
machine learning to emulate a tube amplifier by modifying outputs of
the SPICE model.

The physical model selected for bowed string synthesis is based on
[work by Jean-Loup Florens of
ACROE](https://hal.archives-ouvertes.fr/file/index/docid/910532/filename/Flo02_Conf_ACUSTICUM.pdf).
There is also an [LNCS
paper](https://link.springer.com/chapter/10.1007%2F978-3-540-24598-8_45#citeas),
not sure which is better to cite.  Maybe the [CMJ
paper](https://www.jstor.org/stable/pdf/3679813.pdf?seq=1#page_scan_tab_contents)
is better.  Need to check which one gives more details.

