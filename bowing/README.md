This is a playground for testing the use of policy gradient
reinforcement learning over continuous action spaces to construct
audio synthesis algorithms.

In particular it is working towards synthesis of haptic-audio control
structures.

Evaluating deep deterministic policy gradients as described in [this
blog](https://pemami4911.github.io/blog/2016/08/21/ddpg-rl.html) by
Patrick Emami.

But ultimate the work and writeup would probably benefit from
reproducing the method in [Synthesizing Programs for Images using
Reinforced Adversarial Learning](https://arxiv.org/abs/1804.01118).
