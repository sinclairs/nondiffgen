""" 
Adapted from Patrick Emami's
Implementation of DDPG - Deep Deterministic Policy Gradient
"""

import matplotlib
matplotlib.use('Agg')

import tensorflow as tf
import numpy as np
import gym
from gym import wrappers
import tflearn
import sys, argparse, pickle, time
import pprint as pp
from scipy.signal import detrend

from replay_buffer import ReplayBuffer, ReplayBufferHDF5

import os, importlib
import persample_env
importlib.reload(persample_env)
import bowing_env
importlib.reload(bowing_env)
import autogan
importlib.reload(autogan)

roff = 0.5
roff = 2

# ===========================
#   Actor and Critic DNNs
# ===========================

class ActorNetwork(object):
    """
    Input to the network is the state, output is the action
    under a deterministic policy.

    The output layer activation is a tanh to keep the action
    between -action_bound and action_bound
    """

    def __init__(self, sess, state_dim, state_bound, action_dim, action_bound,
                 learning_rate, tau, batch_size, env, constrain0, constrainSymmetric,
                 adam=True):
        self.sess = sess
        self.s_dim = state_dim
        self.a_dim = action_dim
        self.state_bound = state_bound
        self.action_bound = action_bound
        self.learning_rate = learning_rate
        self.tau = tau
        self.batch_size = batch_size
        self.env = env
        self.constrain0 = constrain0
        self.constrainSymmetric = constrainSymmetric

        # Actor Network
        nvar = len(tf.trainable_variables())
        self.inputs, self.out, self.scaled_out, self.clean_weights = self.create_actor_network("a")
        print('actor has',self.out.shape,'outputs')

        self.network_params = tf.trainable_variables()[nvar:]

        self.flattened_params = tf.concat([tf.reshape(x, (1,-1))
                                           for x in self.network_params], axis=1)

        # Target Network
        self.target_inputs, self.target_out, self.target_scaled_out, _ = self.create_actor_network("ta")

        self.target_network_params = tf.trainable_variables()[
            len(self.network_params)+nvar:]

        # Noisy Actor Network
        self.noisy_inputs, self.noisy_out, self.noisy_scaled_out, self.noisy_weights = self.create_actor_network("na")

        self.noisy_network_params = tf.trainable_variables()[
            len(self.network_params)+len(self.target_network_params)+nvar:]

        self.flattened_params_noisy = tf.concat([tf.reshape(x, (1,-1))
                                                 for x in self.noisy_network_params], axis=1)

        # Previous Actor Network for cross-fading
        self.prev_inputs, self.prev_out, self.prev_scaled_out, self.prev_weights = self.create_actor_network("prev")

        self.prev_network_params = tf.trainable_variables()[
            len(self.network_params)+len(self.target_network_params)+len(self.noisy_network_params)+nvar:]

        # Labels for pretraining
        self.labels = tf.placeholder(tf.float32, [None, self.a_dim])
        self.pretrain_op = tf.train.AdamOptimizer(self.learning_rate)\
                                   .minimize(tf.reduce_mean(
                                       tf.square(self.labels - self.scaled_out)))
        self.pretrain = lambda inp,lab: self.sess.run(
            self.pretrain_op, feed_dict={self.inputs: inp, self.labels: lab})

        # Op for periodically updating target network with online network
        # weights
        self.update_target_network_params = \
            [self.target_network_params[i].assign(tf.multiply(self.network_params[i], self.tau) +
                                                  tf.multiply(self.target_network_params[i], 1. - self.tau))
                for i in range(len(self.target_network_params))]

        # This gradient will be provided by the critic network
        self.action_gradient = tf.placeholder(tf.float32, [None, self.a_dim])

        # Combine the gradients here
        self.unnormalized_actor_gradients = tf.gradients(
            self.scaled_out, self.network_params, -self.action_gradient)
        self.actor_gradients = [None if x is None else tf.div(x, self.batch_size)
                                for x in self.unnormalized_actor_gradients]
        # self.actor_gradients = [tf.div(x, self.batch_size)
        #                         for x in self.unnormalized_actor_gradients]

        # Optimization Op
        if adam:
            self.optimize = tf.train.AdamOptimizer(self.learning_rate).\
                            apply_gradients([x for x in zip(self.actor_gradients,
                                                            self.network_params)
                                             if x is not None])
        else:
            self.optimize = tf.train.GradientDescentOptimizer(self.learning_rate).\
                            apply_gradients([x for x in zip(self.actor_gradients,
                                                            self.network_params)
                                             if x is not None])

        # Extra optimisation to constrain action(0)[0] == 0
        # (No force when no difference in velocity)
        c0 = cS = 0
        if self.constrain0:
            if self.env.distributed_modal_forces:
                # only constrain global force amplitude, the others
                # are per-mode multipliers
                c0 = tf.reduce_mean(tf.square(self.scaled_out[:,0]))
            else:
                # constrain all force values direct to each mode
                c0 = tf.reduce_mean(tf.square(self.scaled_out))
            if adam:
                self.optimizeC0 = tf.train.AdamOptimizer(self.learning_rate/4)\
                                          .minimize(c0)
            else:
                self.optimizeC0 = tf.train.GradientDescentOptimizer(self.learning_rate)\
                                          .minimize(c0)
        if self.constrainSymmetric:
            # input must be linspace(-1,1,200)
            if self.env.distributed_modal_forces:
                n = 1
            else:
                n = None
            cS = (tf.reduce_mean(tf.square(self.scaled_out[:100,:n]
                                           + self.scaled_out[-1:100-1:-1,:n]))
                  + (tf.square(self.scaled_out[0,:1])
                     + tf.square(self.scaled_out[-1,:1]))
                     # + tf.square(self.scaled_out[100,:1]))
            )
            if adam:
                self.optimizeCS = tf.train.AdamOptimizer(self.learning_rate)\
                                          .minimize(cS)
            else:
                self.optimizeCS = tf.train.GradientDescentOptimizer(self.learning_rate)\
                                          .minimize(cS)

        self.num_trainable_vars = len(
            self.network_params) + len(self.target_network_params)

        # Op for updating noisy actor network by copying weights + noise
        self.weight_noise = [tf.placeholder(shape=self.noisy_network_params[i].shape,
                                            dtype=tf.float32)
                             for i in range(len(self.noisy_network_params))]
        self.update_noisy_network_params = \
            [self.noisy_network_params[i].assign(self.network_params[i] + self.weight_noise[i])
             for i in range(len(self.noisy_network_params))]

        # Ops for updating previous actor network from either other network by copying weights
        self.update_prev_network_params_clean = \
            [self.prev_network_params[i].assign(self.network_params[i])
             for i in range(len(self.prev_network_params))]
        self.update_prev_network_params_noisy = \
            [self.prev_network_params[i].assign(self.noisy_network_params[i])
             for i in range(len(self.prev_network_params))]

        # self.weight_mean_squared_clean = tf.reduce_mean(
        #     [tf.reduce_mean(tf.square(j)) for w in self.clean_weights for j in w])

        # self.weight_mean_squared_noisy = tf.reduce_mean(
        #     [tf.reduce_mean(tf.square(j)) for w in self.noisy_weights for j in w])

        self.weight_mean_squared_clean = tf.reduce_mean(
            [tf.reduce_mean(tf.square(j)) for w in self.clean_weights for j in w])

        self.weight_mean_squared_noisy = tf.reduce_mean(
            [tf.reduce_mean(tf.square(j)) for w in self.noisy_weights for j in w])

    def create_actor_network(self, name):
        inputs = tflearn.input_data(shape=[None, np.min([self.s_dim,2])])
        weight_set = []
        inp = (inputs - self.state_bound[0][:2]) / (self.state_bound[1][:2] - self.state_bound[0][:2])*2-1
        net = tflearn.fully_connected(inp[:,0:1], 400, bias=True,
                                      weights_init=tflearn.initializations.xavier())
        print('net',net)
        #bias_init=tflearn.initializations.xavier())
        weight_set.append((net.W, net.b))
        # if self.env.distributed_modal_forces:
        #     net2 = tflearn.fully_connected(inp[:,2:]*0+1, 40, bias=True,
        #                                    weights_init=tflearn.initializations.xavier())
        #     weight_set.append((net2.W, net2.b))
        #     net2 = tflearn.activations.relu(net2)
        #bias_init=tflearn.initializations.xavier())
        # net = tflearn.layers.normalization.batch_normalization(net)
        # weight_set.append((net.beta, net.gamma))
        # net = tflearn.layers.dropout(net, 0.5)
        net = tflearn.activations.relu(net)

        # ## optional?
        net = tflearn.fully_connected(net, 300, bias=True,
                                      weights_init=tflearn.initializations.xavier())
        print('net',net)
        weight_set.append((net.W, net.b))
        net = tflearn.activations.relu(net)

        ## optional?
        # net = tflearn.fully_connected(net, 100, bias=True,
        #                               weights_init=tflearn.initializations.xavier())
        # print('net',net)
        # weight_set.append((net.W, net.b))
        # net = tflearn.activations.relu(net)

        #bias_init=tflearn.initializations.xavier())
        if self.env.distributed_modal_forces:
            net2 = tflearn.fully_connected(tf.ones([tf.shape(inp)[0],1]), #inp[:,2:]*0+1
                                           300, bias=True,
                                           weights_init=tflearn.initializations.xavier())
            print('net2',net2)
            weight_set.append((net2.W, net2.b))
            net2 = tflearn.activations.relu(net2)

        # Final layer weights are init to Uniform[-3e-3, 3e-3]
        # w_init = tflearn.initializations.uniform(minval=-0.003, maxval=0.003)
        w_init = tflearn.initializations.xavier()

        if self.env.distributed_modal_forces:
            # instead of normal scaling, the first output should be
            # bounded but the rest should be normalized to 1, we will try
            # softmax for this, but that means a linear input
            # (relu because must be non-negative)
            out = tflearn.fully_connected(
                net, 1, activation='linear', weights_init=w_init)
            weight_set.append((out.W, out.b))
            amplitude = tf.nn.sigmoid(out)
            print('out',out)

            b_init = tflearn.initializations.normal(mean=0.5, stddev=0.01)
            # out2 = tflearn.fully_connected(
            #     inp[:,2:], self.a_dim-1, activation='sigmoid',
            #     weights_init=w_init, bias_init=b_init)
            out2 = tflearn.fully_connected(
                net2, self.a_dim-1, activation='linear',
                weights_init=w_init, bias_init=b_init)
            print('out2',out2)

            # distribution = tf.nn.softmax(out2, axis=1)

            # use only bias, no input
            # weight_set.append((out2.b,))
            # mag = tf.reshape(tf.reduce_sum(out2.b), [-1, 1])
            # distribution = out2.b / mag * tf.ones([tf.shape(inputs)[0],1])

            # use weight matrix too
            # weight_set.append((out2.W, out2.b))
            # mag = tf.reshape(tf.reduce_sum(out2, axis=1), [-1, 1])
            # distribution = out2 / mag * tf.ones([tf.shape(inputs)[0],1])

            weight_set.append((out2.W, out2.b,))
            if bool(args['use_softmax']):
                distribution = tf.nn.softmax(out2, axis=1)
            else:
                out2 = tf.nn.sigmoid(out2)
                mag = tf.reshape(tf.reduce_sum(out2, axis=1), [-1, 1])
                distribution = out2 / mag

            # w = tflearn.variable(name+"/noise_w", initializer=0.001)
            # out += inp[:,:1]*w
            # weight_set.append((w,))

            # out2 = tf.nn.sigmoid(out2)+1e-3 # sigmoid?
            # distribution = tf.divide(out2, tf.reshape(tf.reduce_sum(out2, axis=1), (-1,1)))

            out = tf.concat([amplitude, distribution], axis=1)

            low, high = self.action_bound[0], self.action_bound[1]
            scaled_out = tf.multiply(out, (high-low)) + low
        else:
            # normal scaling of sigmoid output
            w_init = tflearn.initializations.uniform(minval=-0.001, maxval=0.001)
            b_init = tf.initializers.constant(-10.0)
            out = tflearn.fully_connected(
                net, 1, activation='linear', weights_init=w_init, bias=True)#, bias_init=b_init)
            weight_set.append((out.W, out.b))
            out = tflearn.fully_connected(
                out, self.a_dim, activation='tanh', weights_init=w_init, bias=False)#, bias_init=b_init)
            weight_set.append((out.W,))# out.b))

            # out2 = tflearn.fully_connected(
            #     net2, self.a_dim-1, activation='sigmoid', weights_init=w_init)
            # out = tf.concat([out, out2], axis=1)

            # w = tflearn.variable(name+"/noise_w", initializer=0.001)
            # out += inp[:,:1]*w
            # weight_set.append((w,))

            # Scale output to -action_bound to action_bound
            scaled_out = (tf.multiply(out, (self.action_bound[1]-self.action_bound[0]))
                          + self.action_bound[0])
            scaled_out = out

        return inputs, out, scaled_out, weight_set

    def get_clean_mean_squared_weight(self):
        return self.sess.run(self.weight_mean_squared_clean)

    def get_noisy_mean_squared_weight(self):
        return self.sess.run(self.weight_mean_squared_noisy)

    def get_noise_weight(self):
        return self.sess.run(self.clean_weights[-1][0])

    def get_flattened_params(self):
        return self.sess.run(self.flattened_params)[0,:]

    def get_flattened_params_noisy(self):
        return self.sess.run(self.flattened_params_noisy)[0,:]

    def train(self, inputs, a_gradient):
        self.sess.run(self.optimize, feed_dict={
            self.inputs: inputs,
            self.action_gradient: a_gradient
        })
        if self.constrain0:
            if False:
                inp = np.vstack([np.hstack([np.zeros_like(inputs[:,0:1]),
                                            inputs[:,1:]]),
                                 np.hstack([np.ones_like(inputs[:,0:1])*2,
                                            inputs[:,1:]]),
                                 np.hstack([-np.ones_like(inputs[:,0:1])*2,
                                            inputs[:,1:]])])
            else:
                inp = np.vstack([np.hstack([np.zeros_like(inputs[:,0:1]),
                                            inputs[:,1:]])])
            self.sess.run(self.optimizeC0, feed_dict={
                self.inputs: inp
            })

        if self.constrainSymmetric: # and False:
            inp = np.linspace(-2,2,200).reshape((-1,1))
            if self.inputs.shape[1]==3:
                inp = np.hstack([inp, np.ones((200,1))*0.2])
            inp = np.hstack([np.zeros((200,1)), inp])
            self.sess.run(self.optimizeCS, feed_dict={self.inputs: inp})

    def general_predict(self, out, inp, inputs):
        # a = self.sess.run(self.scaled_out, feed_dict={
        #     self.inputs: inputs
        # })
        # return np.abs(a) * np.sign(np.array(inputs)[:,0:1])

        # inputs = np.array(inputs)
        # a = self.sess.run(out, feed_dict={
        #     inp: np.abs(inputs)
        # })
        # #return a * np.sign(inputs[:,0:1])
        # return np.sqrt(a * np.abs(inputs[:,0:1])) * np.sign(inputs[:,0:1])

        # inputs = np.array(inputs)
        # i = inputs[:1,:]
        # i[0,0] = 0.0
        # a = self.sess.run(out, feed_dict={
        #     inp: np.vstack([np.abs(inputs), i])
        # })
        # return (a[:np.shape(inputs)[0]] - a[-1])*np.sign(inputs[:,0:1])
    
        return self.sess.run(out, feed_dict={
            inp: inputs
        })
            
    def predict(self, inputs):
        return self.general_predict(self.scaled_out, self.inputs, inputs)

    def predict_target(self, inputs):
        return self.general_predict(self.target_scaled_out, self.target_inputs, inputs)
        a = self.sess.run(self.target_scaled_out, feed_dict={
            self.target_inputs: inputs
        })
        return np.abs(a) * np.sign(np.array(inputs)[:,0:1])
        # return self.sess.run(self.target_scaled_out, feed_dict={
        #     self.target_inputs: inputs
        # })

    def predict_noisy(self, inputs):
        return self.general_predict(self.noisy_scaled_out, self.noisy_inputs, inputs)
        a = self.sess.run(self.noisy_scaled_out, feed_dict={
            self.noisy_inputs: inputs
        })
        return np.abs(a) * np.sign(np.array(inputs)[:,0:1])
        # return self.sess.run(self.noisy_scaled_out, feed_dict={
        #     self.noisy_inputs: inputs
        # })

    def predict_prev(self, inputs):
        return self.general_predict(self.prev_scaled_out, self.prev_inputs, inputs)
        a = self.sess.run(self.prev_scaled_out, feed_dict={
            self.prev_inputs: inputs
        })
        return np.abs(a) * np.sign(np.array(inputs)[:,0:1])
        # return self.sess.run(self.prev_scaled_out, feed_dict={
        #     self.prev_inputs: inputs
        # })

    def update_target_network(self):
        self.sess.run(self.update_target_network_params)

    def update_noisy_network(self, noise_function):
        self.weight_noise_instance = {self.weight_noise[i]: noise_function(w.shape)
                                      for i,w in enumerate(self.weight_noise)}
        self.sess.run(self.update_noisy_network_params,
                      feed_dict=self.weight_noise_instance)

    def update_noisy_network_same_as_before(self):
        self.sess.run(self.update_noisy_network_params,
                      feed_dict=self.weight_noise_instance)

    def update_prev_network(self, noisy=False):
        if noisy:
            self.sess.run(self.update_prev_network_params_noisy)
        else:
            self.sess.run(self.update_prev_network_params_clean)

    def get_num_trainable_vars(self):
        return self.num_trainable_vars


class CriticNetwork(object):
    """
    Input to the network is the state and action, output is Q(s,a).
    The action must be obtained from the output of the Actor network.

    """

    def __init__(self, sess, state_dim, state_bound, action_dim, action_bound,
                 learning_rate, tau, gamma, num_actor_vars, adam=True):
        self.sess = sess
        self.s_dim = state_dim
        self.a_dim = action_dim
        self.state_bound = state_bound
        self.action_bound = action_bound
        self.learning_rate = learning_rate
        self.tau = tau
        self.gamma = gamma

        # Create the critic network
        nvar = len(tf.trainable_variables())
        self.inputs, self.action, self.out = self.create_critic_network()

        self.network_params = tf.trainable_variables()[nvar:]
        print(len(self.network_params),'network params')

        # Target Network
        self.target_inputs, self.target_action, self.target_out = self.create_critic_network()

        self.target_network_params = tf.trainable_variables()[(len(self.network_params) + nvar):]
        print(len(self.target_network_params),'target network params')

        # Op for periodically updating target network with online network
        # weights with regularization
        self.update_target_network_params = \
            [self.target_network_params[i].assign(tf.multiply(self.network_params[i], self.tau) \
            + tf.multiply(self.target_network_params[i], 1. - self.tau))
                for i in range(len(self.target_network_params))]

        # Network target (y_i)
        self.predicted_q_value = tf.placeholder(tf.float32, [None, 1])

        # Define loss and optimization Op
        self.loss = tflearn.mean_square(self.predicted_q_value, self.out)
        if adam:
            self.optimize = tf.train.AdamOptimizer(
                self.learning_rate).minimize(self.loss)
        else:
            self.optimize = tf.train.GradientDescentOptimizer(
                self.learning_rate).minimize(self.loss)

        # Get the gradient of the net w.r.t. the action.
        # For each action in the minibatch (i.e., for each x in xs),
        # this will sum up the gradients of each critic output in the minibatch
        # w.r.t. that action. Each output is independent of all
        # actions except for one.
        self.action_grads = tf.gradients(self.out, self.action)

    def create_critic_network(self):
        inputs = tflearn.input_data(shape=[None, self.s_dim])
        action = tflearn.input_data(shape=[None, self.a_dim])
        scaled_act = (action - self.action_bound[0])*2/(self.action_bound[1]-self.action_bound[0])-1

        net = (inputs - self.state_bound[0]) / (self.state_bound[1] - self.state_bound[0])*2-1
        net = tflearn.fully_connected(net, 300)
        net = tf.contrib.layers.batch_norm(net) # but this version is ok??
        net = tflearn.activations.relu(net)

        # Add the action tensor in the 2nd hidden layer
        # Use two temp layers to get the corresponding weights and biases
        # t1 = tflearn.fully_connected(net, 300)
        # t2 = tflearn.fully_connected(action, 300)
        # net = tf.matmul(net, t1.W) + tf.matmul(action, t2.W) + t2.b

        net = tflearn.fully_connected(tf.concat([net, scaled_act], axis=1), 300,
                                      weights_init=tflearn.initializations.xavier())

        # note: batch norm causes very small action gradients!
        # note: worse, batch norm seems to mess with the target
        #       network -- weights are copied but output values
        #       differ?!
        #net = tflearn.layers.normalization.batch_normalization(net)

        # attempt to use batchnorm on all but the distribution, probably a bad idea
        # bn = tf.contrib.layers.batch_norm(net[:,:self.s_dim]) # but this version is ok??
        # net = tf.concat([bn, net[:,self.s_dim:]], axis=1)

        net = tf.contrib.layers.batch_norm(net) # but this version is ok??
        #net = tflearn.layers.dropout(net, 0.5)
        net = tflearn.activations.relu(net)

        net = tflearn.fully_connected(net, 100,
                                      weights_init=tflearn.initializations.xavier())
        net = tf.contrib.layers.batch_norm(net) # but this version is ok??
        # net = tflearn.layers.normalization.batch_normalization(net)
        # net = tflearn.layers.dropout(net, 0.5)
        net = tflearn.activations.relu(net)

        net = tflearn.fully_connected(net, 50)
        net = tf.contrib.layers.batch_norm(net) # but this version is ok??
        # net = tflearn.layers.normalization.batch_normalization(net)
        # net = tflearn.layers.dropout(net, 0.5)
        net = tflearn.activations.relu(net)

        # linear layer connected to 1 output representing Q(s,a)
        # Weights are init to Uniform[-3e-3, 3e-3]
        w_init = tflearn.initializations.uniform(minval=-0.003, maxval=0.003)
        out = tflearn.fully_connected(net, 1, weights_init=w_init)
        return inputs, action, out

    def train(self, inputs, action, predicted_q_value):
        a,b= self.sess.run([self.out, self.optimize], feed_dict={
            self.inputs: inputs,
            self.action: action,
            self.predicted_q_value: predicted_q_value
        })

        return a,b

    def predict(self, inputs, action):
        return self.sess.run(self.out, feed_dict={
            self.inputs: inputs,
            self.action: action
        })

    def predict_target(self, inputs, action):
        return self.sess.run(self.target_out, feed_dict={
            self.target_inputs: inputs,
            self.target_action: action
        })

    def action_gradients(self, inputs, actions):
        return self.sess.run(self.action_grads, feed_dict={
            self.inputs: inputs,
            self.action: actions
        })


    def update_target_network(self):
        self.sess.run(self.update_target_network_params)


class GeneratorNetwork(object):
    """Input to the network is the flattened action network parameters,
    output is the predicted final output generated by the simulation,
    i.e., the log spectrum.
    """

    def __init__(self, action_network, distance_network, learning_rate, adam=True):
        self.actor = action_network
        self.dist = distance_network
        self.sess = self.actor.sess
        self.learning_rate = learning_rate
        self.adam = adam

        self.input_dim = self.actor.flattened_params.shape[1]
        print('generator input dims', self.input_dim)
        self.output_dim = self.dist.inputs.shape[1]
        print('generator output dims', self.output_dim)

        # Create the generator network
        nvar = len(tf.trainable_variables())
        self.inputs = tflearn.input_data(shape=[None, self.input_dim])
        self.labels = tflearn.input_data(shape=[None, self.output_dim])
        self.out_inputs = self.create_generator_network(self.inputs)
        self.network_params = tf.trainable_variables()[nvar:]
        print(len(self.network_params),'generator network params')

        self.loss = tf.reduce_mean(tf.square(self.out_inputs - self.labels))
        self.optimize = tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss)

        self.out_actor = self.create_generator_network(
            tf.expand_dims(self.actor.flattened_params, 0), reuse=True)

        # Link with the distance function by providing generator
        # output as input to a new network sharing the discriminator
        # weights
        self.dist_inputs = self.dist.create_network_with_input(self.out_inputs)
        self.dist_actor = self.dist.create_network_with_input(self.out_actor)

        # provide optimization op that only optimizes the input layer,
        # ie the actor weights, with respect to discrminiation.  We
        # want to *maximize* the value because 'real' samples have
        # positive output from the discriminator.
        self.actor_loss = -tf.reduce_mean(self.dist_actor)
        self.op_actor = tf.train.AdamOptimizer(self.learning_rate, name='Adam_op_actor').\
                        minimize(self.actor_loss, var_list = self.actor.network_params)
        self.op_grad_actor = tf.gradients(self.actor_loss, self.actor.network_params)

    def create_generator_network(self, inputs, reuse=False):

        net = tflearn.fully_connected(inputs, 300, reuse=reuse, scope='genfc1')
        # net = tf.contrib.layers.batch_norm(net)
        net = tflearn.activations.relu(net)

        net = tflearn.fully_connected(net, 100, reuse=reuse, scope='genfc2')
        # net = tf.contrib.layers.batch_norm(net)
        net = tflearn.activations.relu(net)

        net = tflearn.fully_connected(net, 50, reuse=reuse, scope='genfc3')
        # net = tf.contrib.layers.batch_norm(net)
        net = tflearn.activations.relu(net)

        out = tflearn.fully_connected(net, self.output_dim)
        return out

    def train(self, inputs, spectra):
        a,b= self.sess.run([self.out_inputs, self.optimize], feed_dict={
            self.inputs: inputs,
            self.labels: spectra,
        })
        return a,b

    def predict(self, inputs):
        return self.sess.run(self.out_inputs, feed_dict={
            self.inputs: inputs,
        })

    def optimize_actor(self):
        # print('actor grad', self.sess.run(self.op_grad_actor))
        return self.sess.run([self.op_actor, self.actor_loss])[1]


# Taken from https://github.com/openai/baselines/blob/master/baselines/ddpg/noise.py, which is
# based on http://math.stackexchange.com/questions/1287634/implementing-ornstein-uhlenbeck-in-matlab
class OrnsteinUhlenbeckActionNoise:
    def __init__(self, mu, sigma=0.3, theta=.15, dt=1e-2, x0=None):
        self.theta = theta
        self.mu = mu
        self.sigma = sigma
        self.dt = dt
        self.x0 = x0
        self.reset()

    def __call__(self):
        x = self.x_prev + self.theta * (self.mu - self.x_prev) * self.dt + \
                self.sigma * np.sqrt(self.dt) * np.random.normal(size=self.mu.shape)
        self.x_prev = x
        return x

    def reset(self):
        self.x_prev = self.x0 if self.x0 is not None else np.zeros_like(self.mu)

    def __repr__(self):
        return 'OrnsteinUhlenbeckActionNoise(mu={}, sigma={})'.format(self.mu, self.sigma)

# This function generates a non-overlapping train of randomly-spaced,
# random-amplitude gaussian envelopes.  The idea is to stimulate a
# variety of "sharpness" variations for the dv/f curve.
class GaussianImpulseNoise():
    def __init__(self):
        self.dt = 0
        self.t = 0
        self.ta = 0

    def __call__(self):
        n = np.random.uniform(0,1)
        a = np.random.normal(0,0.3)
        f = np.random.exponential(1e-3)
        imp = (n < 0.1)
        if imp > 0 and self.dt <= 0:
            self.dt = f
            self.ta = a
        if self.t < 1:
            self.t += self.dt
        else:
            self.t = 0
            self.dt = 0
        return np.exp(-(((self.t-0.5)*30)**2)) * self.ta

# ===========================
#   Tensorflow Summary Ops
# ===========================

def build_summaries():
    episode_reward = tf.Variable(0.)
    tf.summary.scalar("Reward", episode_reward)
    episode_ave_max_q = tf.Variable(0.)
    tf.summary.scalar("Qmax_Value", episode_ave_max_q)

    summary_vars = [episode_reward, episode_ave_max_q]
    summary_ops = tf.summary.merge_all()

    return summary_ops, summary_vars

# ===========================
# Critic labels from reward
# ===========================

def calculate_critic_labels(args, env, r_batch, t_batch, target_q, gamma):
    # Note: penalty is _not_ added here

    reward_type = args['reward']

    if reward_type == 'full':
        # always give full reward for every step
        y_i = r_batch
    elif reward_type == 'discounted':
        # always give a discounted reward for every step
        y_i = r_batch + gamma * target_q
    elif reward_type == 'discounted_with_final':
        # give a discounted reward for every step, but non-discounted
        # for final
        y_i = [r if (int(n) == int(args['max_episode_len']))
               else r + gamma*tq
               for (n,k,p),r,tq in zip(t_batch,r_batch,target_q)]
    elif reward_type == 'final':
        # only give full reward in final step
        y_i = [gamma * target_q[n]
               if t_batch[n][0] < int(args['max_episode_len']) else r
               for n,r in enumerate(r_batch)]
    elif reward_type == 'partial':
        # always give a small partial reward
        y_i = [(r*(1-gamma) + gamma * target_q[n])
               for n,r in enumerate(r_batch)]
    elif reward_type == 'nstep':
        # give an increasing reward based on time step
        y_i = [((r*(1-gamma**int(t_batch[n][0])))
                + (gamma**int(t_batch[n][0])) * target_q[n])
               for n,r in enumerate(r_batch)]
    elif reward_type == 'nstep_with_final':
        # increasing reward based on time step, but full final reward
        y_i = [((r*(1-gamma**int(t_batch[n][0])))
                 + (gamma**int(t_batch[n][0])) * target_q[n])
               if t_batch[n][0] < int(args['max_episode_len']) else r
               for n,r in enumerate(r_batch)]
    elif reward_type == 'partial_with_final':
        # give a small reward plus a full reward only on last step
        y_i = [(r*(1-gamma) + gamma * target_q[n])
               if t_batch[n][0] < int(args['max_episode_len']) else r
               for n,r in enumerate(r_batch)]
    elif reward_type == 'nsamp-partial_final_with_final':
        # give no reward but an n-sample reward on final step plus
        # full reward on last sample
        y_i = [r if (int(k) == env.store_width
                     and int(n) == int(args['max_episode_len']))
               else
                 (r*(1-gamma**int(k)) * (int(n) == int(args['max_episode_len']))
                  + gamma**int(k) * tq)
               for (n,k,p),r,tq in zip(t_batch,r_batch,target_q)]
    elif reward_type == 'nsamp-partial_final':
        # always give an n-sample partial reward plus full reward on
        # last sample of each step
        y_i = [r if int(k) == env.store_width
               else
                 (r*(1-gamma**int(k)) + gamma**int(k) * tq)
               for (n,k,p),r,tq in zip(t_batch,r_batch,target_q)]
    elif reward_type == 'nsamp_new':
        # new attempt at per-sample n-step reward
        y_i = [(gamma**(int(args['max_episode_len'])-int(k-1)) * tq
                + gamma**(int(args['max_episode_len'])-int(k)) * r)
               for (n,k,p),r,tq in zip(t_batch,r_batch,target_q)]
    elif reward_type == 'nsamp-episode-partial_with_final':
        # always give an n-sample partial reward plus full reward on
        # last sample of the entire episode
        l = int(args['max_episode_len'])
        y_i = [r if (int(k) == env.store_width and int(n) == l)
               else
                 (r*(1-gamma**(int(k)+int(n)*l))
                  + gamma**(int(k)+int(n)*l) * tq)
               for (n,k,p),r,tq in zip(t_batch,r_batch,target_q)]
    else:
        assert False and "Unknown reward type."

    return y_i

# Sanity check, try replacing the reward with mean squared error to
# perfect action
def sanity_check(env, o, a):
    aperf = env.perfect_policy([[o[0],o[1]]])[0][:len(a)]
    #print(a, 'perf', aperf, 'error', np.mean((np.array(aperf) - np.array(a))**2))
    return np.mean((np.array(aperf) - np.array(a))**2)

# ===========================
#   Agent Training
# ===========================

def train(sess, env, args, actor, critic, generator, actor_noise, dist):

    # Set up summary Ops
    summary_ops, summary_vars = build_summaries()

    sess.run(tf.global_variables_initializer())
    writer = tf.summary.FileWriter(args['summary_dir'], sess.graph)
    initial_time = t0 = time.time(), -1

    # Initialize target network weights
    actor.update_target_network()
    critic.update_target_network()

    # Initialize replay memory
    # replay_buffer = ReplayBuffer(int(args['buffer_size']), int(args['random_seed']))
    replay_buffer = ReplayBufferHDF5(int(args['buffer_size']), int(args['random_seed']),
                                     filename=os.path.join(args['monitor_dir'],'..',
                                                           'replay_buffer.hdf5'))

    # Needed to enable BatchNorm. 
    # This hurts the performance on Pendulum but could be useful
    # in other environments.
    tflearn.is_training(True)

    action_trace_clean = [list() for _ in range(actor.a_dim)]
    action_trace_noisy = [list() for _ in range(actor.a_dim)]
    reward_trace = []
    out_file = None #open('waveout.dat', 'w')
    out_file_counter = 0
    # print('; Sample Rate 48000', file=out_file)
    # print('; Channels 1', file=out_file)

    pressure_noise = OrnsteinUhlenbeckActionNoise(mu=np.zeros(1), sigma=np.ones(1)*0.02)
    velocity_noise = OrnsteinUhlenbeckActionNoise(mu=np.zeros(1), sigma=np.ones(1)*0.01)

    offset_noise = OrnsteinUhlenbeckActionNoise(mu=np.zeros(1), sigma=np.ones(1)*1.0)
    scaling_noise = OrnsteinUhlenbeckActionNoise(mu=np.zeros(1), sigma=np.ones(1)*100.0)

    gaussian_impulse_noise = GaussianImpulseNoise()

    histogram_value = np.zeros((30,30))
    histogram_count = np.ma.MaskedArray(np.zeros((30,30)), mask=np.ones((30,30)))

    dist_trace = [[],[]]

    # Pre-training
    if bool(int(args['pretrain'])):
        print('Pre-training.')
        for i in range(3000):
            y = np.random.uniform(-1,1,(100,1))
            z = np.ones((100,1))*0.2
            x = np.hstack([y,z])
            if env.distributed_modal_forces:
                actor.pretrain(x, env.perfect_policy(x))
            else:
                actor.pretrain(x, env.perfect_policy(x)[:,:1])

    # Pre-reward
    if bool(int(args['prereward'])):
        print('Pre-rewarding.')
        env.reset()
        N = int(args['max_episode_len'])*env.hop + env.NWINDOW
        nz = (np.random.normal(0,0.01,(N,env.action_space.shape[0]))
              * (env.action_space.high - env.action_space.low))
        step_trace, a_clean, a_noisy = \
            env.inner_loop(actor, False, noise=None, steps=N,
                           fade=env.hop, out_file=None,
                           perfect='both')
        s2 = env.log_spectrum()
        si = replay_buffer.add_spectrum(s2)
        r = -dist.reconstruction_loss(s2.reshape((1,-1)))
        # from matplotlib import pyplot as plt
        # plt.subplot(221)
        # plt.plot([a[0] for (o,a,o2) in step_trace[-env.store_width:]], label='f')
        # plt.plot([o[1] for (o,a,o2) in step_trace[-env.store_width:]], label='dv')
        # plt.plot([o[2] for (o,a,o2) in step_trace[-env.store_width:]], label='p')
        # plt.legend()
        # plt.subplot(222)
        # plt.scatter([o[1] for (o,a,o2) in step_trace[-env.store_width:]],
        #             [a[0] for (o,a,o2) in step_trace[-env.store_width:]], marker='.')
        # plt.subplot(223)
        # plt.plot(env.bv_trace)
        # plt.subplot(224)
        # pw = lambda x: '%f'%(np.mean((x - np.mean(x))**2))
        # plt.plot(env.baf_trace, label=pw(env.baf_trace[-env.store_width:]))
        # plt.legend()
        # plt.show()
        # exit()

        for n, (o, a, o2) in enumerate(step_trace[-env.store_width:]):
            penalty = 0
            replay_buffer.add(o, a, si, (int(args['max_episode_len']),n,penalty), o2)

            # Track all values added to replay buffer in a 2D
            # histogram
            i,j = int((o[0]+1)/2*30), int((a[0]+1)/2.0*30)
            if i < 30 and j < 30 and i >= 0 and j >= 0:
                y = calculate_critic_labels(args, env, [r-penalty],
                                            [[int(args['max_episode_len']),n,penalty]],
                                            [0], critic.gamma)
                histogram_value[i,j] += y
                c = histogram_count[i,j]
                histogram_count[i,j] = c+1 if c else 1

            if actor.constrainSymmetric:
                neg_o = np.hstack([-o[0], o[1:]])
                neg_o2 = np.hstack([-o2[0], o2[1:]])
                if env.distributed_modal_forces:
                    neg_a = np.hstack([-a[0], a[1:]])
                else:
                    neg_a = -a
                replay_buffer.add(neg_o, neg_a, si, (int(args['max_episode_len']),
                                                     n,penalty), neg_o2)

                i,j = int((-o[0]+1)/2*30), int((-a[0]+1)/2.0*30)
                if i < 30 and j < 30 and i >= 0 and j >= 0:
                    y = calculate_critic_labels(args, env, [r-penalty],
                                                [[int(args['max_episode_len']),n,penalty]],
                                                [0], critic.gamma)
                    histogram_value[i,j] += y
                    c = histogram_count[i,j]
                    histogram_count[i,j] = c+1 if c else 1

    for episode in range(int(args['max_episodes'])):

        env.reset()
        veldir = np.random.randint(2)*2-1

        ep_reward = 0
        ep_ave_max_q = 0

        actor.update_noisy_network(lambda shape: np.random.laplace(1,0.5,shape))

        step_trace = []
        for step in range(int(args['max_episode_len'])):
            # env.reset()
            is_last_step = (step == (int(args['max_episode_len'])-1))

            # is_viz_step = step % 5 == 4
            is_viz_step = True
            # is_viz_step = is_last_step

            tflearn.is_training(False)

            # Pick a random action offset for each window
            # snz = scaling_noise()# * (env.action_space.high - env.action_space.low)
            # onz = offset_noise() #* (env.action_space.high - env.action_space.low)
            scale = scaling_noise()*(5**(offset_noise()))

            # instead of per-sample noise, action is a function, so
            # let's make a functional noise, i.e. random polynomial.
            # If it's stable during the whole step then it should give
            # useful results.
            dvnoise = [actor_noise() * (env.action_space.high - env.action_space.low)
                       for _ in range(200)]
            dvpoly = np.polyfit(np.linspace(-2,2,200), dvnoise, 20)
            # TODO, seems to work but not sure if it's beneficial

            # Pick a random target bow velocity and bow pressure
            if bool(int(args['wander'])):
                # env.bow_pressure.set_target(np.abs(pressure_noise()+0.1))
                env.target_bow_velocity.set_target(velocity_noise()+0.2*veldir)
            else:
                env.target_bow_velocity.set_target(env.target_bow_velocity.target*veldir)

            # Step forward by two windows + warmup time while applying
            # the policy on each sample.

            # To test: can we get away with a shorter hop size here?
            # (Seems not, very noisy.. sometimes learns but not sure
            # it's worth it)

            # Update noisy actor network weights.  Copies network
            # weights and adds a random value.  The noise function we
            # provide is called for each vector of weights and biases.
            # We use a Laplacian distribution because we want random
            # negative and positive offsets, with occasionally larger
            # values.
            actor.update_noisy_network(lambda shape: np.random.laplace(0,0.01,shape))
            # actor.update_noisy_network(lambda shape: np.random.normal(0,0.01,shape))

            # How many samples to execute
            # N = env.NWINDOW//2 # + env.NWINDOW*5*(is_last_step)
            N = env.hop #+ env.NWINDOW*(step==0)

            # Add exploration noise only when not evaluating for visualization
            nz = None
            use_noisy_predict = False
            if args['noise'] is not None: # and not is_last_step:
                if 'act' in args['noise']: #not is_last_step: #is_viz_step:
                    # a *= 1 + scale
                    # a += actor_noise()
                    # a = a*(1+snz) #+ onz

                    #a += o[0] * scale
                    #a += (o[0]**3) * scale
                    # a += np.polyval(dvpoly, o[0])

                    # a = actor_noise()
                    if 'norm' in args['noise']:
                        nz = (np.random.normal(0,0.0001,(N,env.action_space.shape[0]))
                              * (env.action_space.high - env.action_space.low))
                    elif 'laplace' in args['noise']:
                        nz = (np.random.laplace(0,0.0001,(N,env.action_space.shape[0]))
                              * (env.action_space.high - env.action_space.low))
                    else:
                        assert False and 'Unknown noise ' + args['noise']

                    # salt & pepper noise
                    # a += (np.abs(actor_noise()) < 0.001) * actor_noise() * 10

                    # better(?) variant of the above
                    # a += gaussian_impulse_noise()

                if 'param' in args['noise']:
                    use_noisy_predict = True #not is_last_step #True
                    # nz = (np.random.laplace(0,0.01,(N,env.action_space.shape[0]))
                    #       * (env.action_space.high - env.action_space.low))
                    # nz = (np.random.normal(0,0.001,(N,env.action_space.shape[0]))
                    #       * (env.action_space.high - env.action_space.low))

            step_trace, a_clean, a_noisy = \
                env.inner_loop(actor, use_noisy_predict, noise=nz,
                               steps=N, fade=env.hop, out_file=None,
                               perfect=args['perfect'])

            [action_trace_clean[l].append(a_clean[-1][l]) for l in range(actor.a_dim)]
            [action_trace_noisy[l].append(a_noisy[-1][l]) for l in range(actor.a_dim)]

            # Sample the spectrum after, all samples of window share
            # the spectrum and reward.
            s2 = env.log_spectrum()

            # The current spectrum is considered the "result" of the
            # policy during the last window, thus all
            # observation/action pairs in the trace have the same
            # reward.  We save the spectrum as well as the reward
            # directly since it needs to be recalculated as needed.
            # It must be purely a function of the spectrum, but we
            # recalculate since that function is subject to change as
            # more data becomes available.
            tflearn.is_training(False)
            r = -dist.reconstruction_loss(s2.reshape((1,-1)))
            # if use_noisy_predict:
            #     avg_w2 = actor.get_noisy_mean_squared_weight()
            # else:
            #     avg_w2 = actor.get_clean_mean_squared_weight()

            # Summarize rewards
            if True: #not is_viz_step:
                # penalty = avg_w2
                # penalty += np.mean([(o[1]**2)*np.sqrt(np.abs(a[0]))
                #                     for o,a,o2 in step_trace[-env.NWINDOW//2:]])
                # penalty += np.mean([a[0]**2 for o,a,o2 in step_trace[-env.NWINDOW//2:]])

                # Penalize incorrect power for the "bow action force"
                pw = lambda x: np.mean(detrend(x)**2)
                #penalty = 1e8*((pw(env.baf_trace[-env.store_width:]) - 0.00014)**2)
                #penalty = (np.log10(0.05) - np.log10(pw(env.vd_trace[-env.store_width:])))**2
                penalty = np.log10((0.05 - pw(env.vd_trace[-env.store_width:]))**2 + 1e-4)
                # penalty = 0

                ep_reward += r-penalty
                #reward_trace.append((r,avg_w2,r-penalty))
                reward_trace.append(r)

            si = replay_buffer.add_spectrum(s2)

            if is_last_step and use_noisy_predict:
                replay_buffer.add_actor_params(actor.get_flattened_params_noisy(), si, r, penalty)
            elif is_last_step:
                replay_buffer.add_actor_params(actor.get_flattened_params(), si, r, penalty)

            for n, (o, a, o2) in enumerate(step_trace[-env.store_width:]):
                # if step < 10: continue

                # Current we sample way too much outside the range of
                # interest, so this is to see if we get better
                # representation in the critic of the interesting area
                # if we limit data put into the replay buffer
                if (np.any(o < env.observation_space.low)
                    or np.any(o > env.observation_space.high)):
                    continue

                # warning, replaced r with s2, t with r!
                apen = (np.sign(a) != np.sign(o[0])).sum()*5
                replay_buffer.add(o, a, si, (step+1,n+1,penalty+apen), o2)
                # replay_buffer.add(o, a, si, (int(is_last_step),penalty), o2)

                # data augmentation
                if actor.constrainSymmetric:
                    neg_o = np.hstack([-o[0], o[1:]])
                    neg_o2 = np.hstack([-o2[0], o2[1:]])
                    if env.distributed_modal_forces:
                        neg_a = np.hstack([-a[0], a[1:]])
                    else:
                        neg_a = -a
                    replay_buffer.add(neg_o, neg_a, si, (step+1,n+1,penalty+apen), neg_o2)

                # Track all values added to replay buffer in a 2D
                # histogram
                i,j = int((o[0]+1)/2*30), int((a[0]+1)/2.0*30)
                if i < 30 and j < 30 and i >= 0 and j >= 0:
                    # r = -sanity_check(env, o, a)
                    y = calculate_critic_labels(args, env, [r-penalty], [[step+1,n+1,penalty]],
                                                [0], critic.gamma)
                    histogram_value[i,j] += y
                    c = histogram_count[i,j]
                    histogram_count[i,j] = c+1 if c else 1

                if actor.constrainSymmetric:
                    i,j = int((-o[0]+1)/2*30), int((-a[0]+1)/2.0*30)
                    if i < 30 and j < 30 and i >= 0 and j >= 0:
                        y = calculate_critic_labels(args, env, [r-penalty], [[step+1,n+1,penalty]],
                                                    [0], critic.gamma)
                        histogram_value[i,j] += y
                        c = histogram_count[i,j]
                        histogram_count[i,j] = c+1 if c else 1

                # for i in range(30):
                #     for j in range(30):
                #         x,y = i/30*2-1, j/30*2-1
                #         histogram_value[i,j] = (-x**2)*(np.sqrt(np.abs(y)))
                #         histogram_count[i,j] = 1


            if bool(int(args['generator'])) and replay_buffer.actor_params_size() > int(args['minibatch_size']):
                p_batch, s2_batch, r_batch, penalty_batch = \
                    replay_buffer.sample_batch_actor_params(int(args['minibatch_size']))
                generator.train(p_batch, s2_batch)

            # Keep adding experience to the memory until
            # there are at least minibatch size samples
            if replay_buffer.size() > int(args['minibatch_size']):

                # Note change of notation from original source, we use
                # 'o' for observation (intead of calling it state) and
                # use 's' for spectrum (where before it was 'r' for
                # reward, which is calculated below.)
                o_batch, a_batch, s2_batch, t_batch, o2_batch = \
                    replay_buffer.sample_batch(int(args['minibatch_size']))

                # Train distance function.
                train_dist_continuous = True
                if step%100==2 and not train_dist_continuous:
                    tflearn.is_training(True)
                    for k in range(100):
                        d = dist.train(
                            replay_buffer.sample_batch(int(args['minibatch_size']))[2])
                        if d is not None:
                            dist_trace[0].append(d[0])
                            dist_trace[1].append(d[1])

                elif train_dist_continuous:
                    d = dist.train(
                        replay_buffer.sample_batch(int(args['minibatch_size']))[2])
                    if d is not None:
                        dist_trace[0].append(d[0])
                        dist_trace[1].append(d[1])

                # Calculate rewards using updated function
                tflearn.is_training(False)
                r_batch = [None if s is None else -dist.reconstruction_loss([s])[0]-penalty+roff
                           for (n,k,penalty),s in zip(t_batch,s2_batch)]
                # r_batch = [None if s is None else -sanity_check(env, o, a)
                #            for (o,a,s) in zip(o_batch, a_batch, s2_batch)]

                # Calculate targets
                target_q = critic.predict_target(
                    o2_batch, actor.predict_target(o2_batch[:,:2]))

                y_i = calculate_critic_labels(args, env, r_batch, t_batch, target_q, critic.gamma)

                # Update the critic given the targets
                tflearn.is_training(True)
                predicted_q_value, _ = critic.train(
                    o_batch, a_batch, np.reshape(y_i, (-1,1)))

                ep_ave_max_q += np.amax(predicted_q_value)

                # Update the actor policy using the sampled gradient
                tflearn.is_training(False)
                # o_train = np.hstack([np.linspace(-1,1,256).reshape((-1,1)),
                #                      np.ones((256,1))*0.2])
                # a_outs = actor.predict(o_train)
                # grads = critic.action_gradients(o_train, a_outs)
                # tflearn.is_training(True)
                # actor.train(o_train, grads[0])

                if bool(int(args['generator'])):
                    # Generator actor training via generator/discrminator gradient
                    p = actor.get_flattened_params()
                    print('generator.optimize_actor:', generator.optimize_actor())
                    print('actor.params changed by:', np.abs(p-actor.get_flattened_params()).mean())

                else:
                    # DDPG actor training via critic action gradient
                    a_outs = actor.predict(o_batch[:,:2])
                    grads = critic.action_gradients(o_batch, a_outs)
                    tflearn.is_training(True)
                    actor.train(o_batch[:,:2], grads[0])

                # Update target networks
                actor.update_target_network()
                critic.update_target_network()

                # TODO code to understand if target network is updated
                # properly apparently batchnorm was messing this up!
                # tflearn.is_training(False)
                # a = actor.predict(o2_batch[:,:3])
                # q = critic.predict(o2_batch[:,1:], a)
                # q2 = critic.predict(o2_batch[:,1:], a)
                # target_q = critic.predict_target(o2_batch[:,1:], a)
                # print(a[0],q[0],q2[0],target_q[0],np.mean((q-target_q)**2),
                #       sess.run(critic.network_params[0])[0,0],
                #       sess.run(critic.target_network_params[0])[0,0])

                # Update noisy network (keeping same parameter noise
                # but applied to newly trained actor)
                actor.update_noisy_network_same_as_before()

            tflearn.is_training(False)

            if is_viz_step and replay_buffer.size() > int(args['minibatch_size']):
                # pickle.dump(replay_buffer, open('rebuf.pickle','wb'))

                if args['render_env']:
                    env.render(args, episode, step, env, dist, actor, critic, s2, r, penalty,
                               [step_trace, reward_trace, action_trace_noisy,
                                action_trace_clean, dist_trace],
                               [o_batch, a_batch, o2_batch, r_batch, s2_batch,
                                y_i, predicted_q_value],
                               histogram_value / histogram_count,
                               len(replay_buffer.buffer), use_noisy_predict)

                summary_str = sess.run(summary_ops, feed_dict={
                    summary_vars[0]: float(ep_reward),
                    summary_vars[1]: float(ep_ave_max_q) / float(step+1)
                })

                writer.add_summary(summary_str, episode)
                writer.flush()

                t1 = time.time(), step+int(args['max_episode_len'])*episode
                seconds_per_step = (t1[0]-t0[0])/(t1[1]-t0[1])
                total_time_mins = int((t1[0] - initial_time[0])//60)
                print(('| Reward: {:.2f} | Episode: {:d} | Step: {:d}'
                       +' | Qmax: {:.4f} | Time: {:d}m | Time/step: {:.1f}s').
                      format(float(ep_reward), episode, step, (ep_ave_max_q / float(step)),
                             total_time_mins, seconds_per_step))
                t0 = t1

def main(args, env=None):

    # Options for training
    constrain0 = False
    constrainSymmetric = True

    if env is None:
        if hasattr(persample_env, args['env']):
            env = getattr(persample_env, args['env'])()
        elif hasattr(bowing_env, args['env']):
            env = getattr(bowing_env, args['env'])(110.0, num_modes=20,
                                                   distributed_modal_forces=False,
                                                   predefined_distribution=not 'dist' in args['problem'],
                                                   veldiff_only=False,
                                                   window_width = 2048,
                                                   hop_width = 2048*4, #(20 if bool(int(args['generator'])) else 1),
                                                   )#store_width = 1024)
        else:
            print('No environment.')
            return 1

    print(time.ctime())

    with tf.Session() as sess:

        np.random.seed(int(args['random_seed']))
        tf.set_random_seed(int(args['random_seed']))
        env.seed(int(args['random_seed']))

        if env.observation_space:
            state_dim = env.observation_space.shape[0]
            state_bound = env.observation_space.low, env.observation_space.high
        else:
            state_dim = 0
            state_bound = 0, 1

        action_dim = env.action_space.shape[0]
        action_bound = env.action_space.low, env.action_space.high

        if args['distance']=='AutoEncoder':
            dist = autogan.AutoEncoderDistance(sess, state_dim=env.NFFT, latent_dim=10,
                                               learning_rate=0.0001, real_sampler=env.sample_real)
        elif args['distance']=='Wasserstein':
            dist = autogan.WassersteinDistance(sess, state_dim=env.NFFT,
                                               learning_rate=0.001, real_sampler=env.sample_real,
                                               use_convnet = False)
        elif args['distance']=='LogItakuraSaito':
            dist = autogan.LogItakuraSaitoDistance(real_sampler=env.sample_real,
                                                   frequency_weighted=args['frequency_weighted'])
        elif args['distance']=='MeanSquare':
            dist = autogan.MeanSquareDistance(real_sampler=env.sample_real,
                                              frequency_weighted=args['frequency_weighted'])
        else:
            dist = getattr(autogan, args['distance'])(sess, real_sampler=env.sample_real)
        env.set_dist(dist)

        actor = ActorNetwork(sess,
                             state_dim, state_bound,
                             action_dim, action_bound,
                             float(args['actor_lr']), float(args['tau']),
                             int(args['minibatch_size']),
                             env,
                             constrain0=constrain0,
                             constrainSymmetric=constrainSymmetric,
                             adam=bool(int(args['adam'])))

        critic = CriticNetwork(sess,
                               state_dim, state_bound,
                               action_dim, action_bound,
                               float(args['critic_lr']), float(args['tau']),
                               float(args['gamma']),
                               actor.get_num_trainable_vars(),
                               adam=bool(int(args['adam'])))

        actor_noise = OrnsteinUhlenbeckActionNoise(mu=np.zeros(action_dim),
                                                   sigma=np.ones(action_dim)*0.1)

        generator = None
        if bool(int(args['generator'])):
            generator = GeneratorNetwork(actor, dist, learning_rate=0.0001,
                                         adam=bool(int(args['adam'])))

        if args['use_gym_monitor']:
            if not args['render_env']:
                env = wrappers.Monitor(
                    env, args['monitor_dir'], video_callable=False, force=True)
            else:
                env = wrappers.Monitor(env, args['monitor_dir'], force=True)

        try:
            train(sess, env, args, actor, critic, generator, actor_noise, dist)
        except KeyboardInterrupt:
            pass

        if args['use_gym_monitor']:
            env.monitor.close()

def parse_args():
    global args
    parser = argparse.ArgumentParser(description='provide arguments for DDPG agent')

    # parameters
    parser.add_argument('--pretrain', help='pretrain actor or not', default=False)
    parser.add_argument('--prereward', help='prereward actor or not', default=False)
    parser.add_argument('--wander', help='random-walk input parameters or not', default=False)
    parser.add_argument('--distance', help='type of distance metric', default='LogItakuraSaito')
    parser.add_argument('--frequency-weighted', help='is distance metric frequency-weighted', default=False)
    parser.add_argument('--description', help='message to add to generated graphs', default='(no description)')
    parser.add_argument('--use-softmax', help='use softmax or not', default=False)
    parser.add_argument('--perfect', help='force perfect policy (amp|dist|both)', default=None)
    parser.add_argument('--adam', help='use Adam optimiser or not', default=False)
    parser.add_argument('--noise', help='type of noise', default=None)
    parser.add_argument('--generator', help='whether to use generator', default=False)
    parser.add_argument('--problem', help='whether to train dvf, dist or both', default='dvf')

    # agent parameters
    parser.add_argument('--actor-lr', help='actor network learning rate', default=0.0001)
    parser.add_argument('--critic-lr', help='critic network learning rate', default=0.001)
    parser.add_argument('--gamma', help='discount factor for critic updates', default=0.999)
    parser.add_argument('--tau', help='soft target update parameter', default=0.001)
    parser.add_argument('--buffer-size', help='max size of the replay buffer', default=1000000)
    parser.add_argument('--minibatch-size', help='size of minibatch for minibatch-SGD', default=256)

    # critic parameters
    parser.add_argument('--reward', help='type of reward', default='partial_with_final')

    # run parameters
    parser.add_argument('--env', help='choose the env', default='BowingCpp')
    parser.add_argument('--random-seed', help='random seed for repeatability', default=1234)
    parser.add_argument('--max-episodes', help='max num of episodes to do while training', default=50000)
    parser.add_argument('--max-episode-len', help='max length of 1 episode', default=1)
    parser.add_argument('--render-env', help='render the gym env', action='store_true')
    parser.add_argument('--use-gym-monitor', help='record gym results', action='store_true')
    parser.add_argument('--monitor-dir', help='directory for storing gym results', default='./results/gym_ddpg')
    parser.add_argument('--summary-dir', help='directory for storing tensorboard info', default='./results/tf_ddpg')

    parser.set_defaults(render_env=True)
    #parser.set_defaults(use_gym_monitor=True)

    args = vars(parser.parse_args())

    pp.pprint(args)
    return args

if __name__ == '__main__':
    sys.exit(main(parse_args()))
