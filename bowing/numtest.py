#!/usr/bin/env python3

# Test something even simply: guess a number using
# policy gradients

import gym.spaces
import numpy as np
from matplotlib import pyplot as plt
import scipy.signal

y_target = 0.5

class GuessNum(object):
    """AI Gym-like environment"""
    def __init__(self):
        self.action_space = gym.spaces.Box( -np.ones(2), np.ones(2), dtype=np.float32)
        self.observation_space = gym.spaces.Box(-np.ones(2), np.ones(2),dtype=np.float32)
        self.reset()

    def reset(self):
        self.state = np.zeros(2, dtype=np.float)
        return self.observe()

    def reward(self):
        return -(float(self.state[0])-y_target)**2 + 1

    def seed(self, s):
        pass

    def observe(self):
        return self.state

    def step(self, action):
        self.state = action
        # self.state = np.tanh(self.state+action) #np.tanh(action)
        # print('action',action,end='\t')
        # print('state',self.state,end='\t')
        # print('reward',self.reward())
        # o = observe(self.state[:3]*240, self.state[3:6]/2+0.5)
        # self.render()
        return self.observe(), self.reward(), False, None

    def close(self):
        pass

    def render(self):
        print('state',self.state)

    def sample_real(self, num):
        # so random! /s
        return np.array([[y_target,0]]*num)
