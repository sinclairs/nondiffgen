\documentclass[twocolumn]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{color}
\usepackage{xspace}

\newcommand{\needref}{\textcolor{red}{\textbf{[ref]}}\xspace}
\newcommand{\todo}{\textcolor{red}{\textbf{(todo!)}}\xspace}

\title{Learning to bow by policy gradient}
\author{Stephen Sinclair}
\date{}

\begin{document}

\maketitle

\section{Introduction}

It has been shown that it is possible to reproduce the sound and feeling of bowing a string on a force feedback haptic device by executing a real time simulation of a vibrating string and introducing a non-linear coupling to a moving mass element\needref{}.

The tuning of such a model requires setting the resonator parameters (string tuning, stiffness, damping, bridge impedance), junction parameters (bow distance from the bridge), and coupling parameters (bow elasticity, friction characteristics).

Of these, the friction characteristics, namely a non-linear relationship between the differential velocity between bow and string and the force exchanged between them, has a very strong effect on the spectrum that can be difficult to predict. It typically features a static component, in which the bow and string are fully coupled and have the same velocity, and a dynamic component in which friction force sharply decreases with velocity difference, called the Stribeck effect as determined by \needref.  This sharp decrease, termed ``slip'' allows the string to oscillate independently until it comes back to a lower velocity difference, when ``sticking'' occurs again. The mechanism for this effect in the bow has been identified with melting and freezing of the resin material used to mediate friction \needref.  This so-called stick-slip cycle repeats at the natural frequency of the string. In practice for numerical stability the static portion is often replaced with a linear damping, see Fig.~\todo.

While past research has concentrated on modeling this friction relationship accurately \needref, here we propose an alternative method of determining this relationship by matching a generative model.  This approach allows, given some resonator and bow model, to determine a friction relation that generates a desired spectrum.  Thus, we can determine the bowed friction relationship necessary to generate a given sound, not only of stringed instruments, but of, to some approximation, any harmonic sound.

We achieve this goal by means of improving an estimate via iterative descent over the policy gradient.  Here, we take the relation $f=\rho(\Delta v,p) p$, where $f$ is the force of friction, and $p$ is the normal force, to be a control policy.  Force on the bow is thus $f_b=-f$ while force on the string is, equal and opposite, $f_s=f$.  When policy $\rho_\theta$, a non-linear function parameterized by $\theta$,  features an appropriate non-linearity, higher harmonics are stimulated, leading to the recognizable bowed harmonic resonator sound.

Given some measure of distance $D$ to the desired sound or class of sounds, and an initial guess $\rho_0$, we can find a locally optimal function $\rho^\star$ that best minimizes $D(S)$ where some mapping $\rho_\theta\rightarrow S$ is presumed to exist for a given bow velocity and pressure.  Here $S$ is a measure of the sound, for which we use the log spectrum in the current work.  Choices for $D$ are discussed below.

Finding $\rho^\star$ requires optimizing over $D$ with respect to policy parameters $\theta$. However, here we have no guarantee that the mapping $\rho_\theta \rightarrow S$ is continuous or differentiable since it involves running a simulation and calculating the resulting spectrum. For generality we cannot assume that a differentiable formulation is possible with respect to $\theta$. Thus, some gradient-free optimization method is required. Several such methods are available, including genetic algorithms, simulated annealing, particle-based optimization, and reinforcement learning.

\subsection{Deterministic Policy Gradient Optimization  }

In this work we explored a a variant of reinforcement learning called Advantage Actor-Critic (A2C) Deep Deterministic Policy Gradient (DDPG) learning. In the reinforcement learning setting, the policy optimization is formulated as a game in which,  for a given state, the policy selects the action which leads to the best long-term reward. A2C introduces a pair of functions, the \emph{critic}, which attempts to predict the \emph{advantage} of taking an action for a given state, i.e. the difference between the estimated long-term reward of that action and that of the previous state/action pair, and the \emph{actor}, which selects the action that maximizes the estimated advantage. Maximizing the advantage at each step is theorized to maximize chances of long-term reward. The DDPG formulation allows this action selection to be \emph{deterministic} in a \emph{continuous} control space, as opposed to discrete, probabilistic action selection. The ``deep'' nomenclature refers to a few tools that help this to work when using function approximators, including the common choice of multilayered neural networks \needref.

A2C-DDPG \todo has been shown to successfully learn to play Atari video games by looking only at pixel data \cite{mnih2015human}. Lately A2C has also been shown to be able to discover instructions for drawing letters and painting pictures \cite{ganin2018synthesizing}. It is an interesting candidate for determining policies for per-sample control of physical audio synthesis because, in the formulation given in this paper, it attempts to select optimal forces to apply at each step in order to achieve a desired long-term reward, in this case, to minimize the distance to a target spectrum. Effectively it allows to ``wrap'' the non-differentiable reward in a differentiable approximation that allows gradient descent over the solution space, without ever needing to predict the spectrum itself---only the distance to the target. The idea behind this paper is that by achieving a desired sound by formulating it as a physical control problem, we can not only produce the desired sound, but the physical context allows to additionally calculate force feedback signals for haptic interaction.% that are \emph{not} the same as the sound itself, but are somehow ``semantically'' related and physically consistent. 

Another interesting aspect of this method worth mentioning is that it is completely ``online''.  Essentially the physical model is stimulated continuously by a changing policy that we attempt to refine over time until the desired sound appears. We use a model of the device representing the `bow' for practical computational reasons, but theoretically there is no reason one could not include a real haptic device in the loop during optimisation, which might actually be an interesting idea since it would automatically take into account device dynamics and discretisation issues.

A brief description of A2C-DDPG follows for the purpose of defining terms, however we refer the reader to the cited papers for a complete description. The current work does not propose any fundamental changes to the algorithm, but we describe in the next section some configuration choices that worked well for our problem domain.

...

\section{Formulation for audio}

The reinforcement learning paradigm involves episodic trials divided into steps. At the end of each episode, the final reward is calculated and a value is assigned to each step reflecting its contribution to the total reward. In the policy gradients formulation (or just REINFORCE?), it is possible to assign a \emph{discounted} reward accounting for uncertainty in the contribution to the total reward before the episode terminates, allowing for online learning 
\todo 

In our formulation for audio, we add a second time scale for calculating spectra over a sliding window. Thus, each \emph{action} step consists of a single sample of PCM audio, but a common spectrum is assigned to every sample from the window.
%Since the reward is based on the spectrum, every step in the window gets the same (discounted) reward.
This implies that each step in the window is associated with the same final reward.

Some inefficiencies can be found here: assigning credit for the spectrum to any particular action is not possible; we know that the credit that should be assigned to the final action of a window is very small due to the Hann window we apply during log-spectrum calculation, but the final reward is not known until the whole window has been simulated.  We therefore use a so-called ``$n$-step'' formulation of the reward which backtracks from the final action assigning a consecutively more and more ``discounted'' reward value as we look backward in time.  Thus, for a given step $k$ of an $n$-step window, the assigned reward is,

\[ R_k = r_k + \gamma^{k-1} r_{k-1} + ... + \gamma^{k} V_{n}(s) \] \todo

The role of the value function $V$ is to estimate the ``difference'' that sums to the total reward for the window.

Since there are many correlated samples per reward, we make use of a replay buffer \cite{mnih2015human}. We store all observations ($\Delta v$ and $p$) and actions taken $f,$ the resulting spectrum $S$ and the next observation. These are then randomly sampled in mini-batches after each window for training the critic and actor. We recalculate the reward from the spectrum during buffer sampling to account for a varying $D$ function, addressed shortly. Also following \cite{mnih2015human} we use target networks for actor and critic.

Although the data in the replay buffer is noisy because many policies with different rewards may share $(\Delta v,f)$ pairs, after some time $n$ and given sufficient exploration, a picture of the optimal curve $\rho^\star\approx\rho_{\theta_n}(\Delta v)$ emerges that produces the target spectrum $S$. \todo{figure}.

We can also consider $S$ to be a set of spectra such that we wish to find $\rho$ that produces one or many elements $s\in S$. In this case a function $D$ is needed that calculates a closest distance to the set boundary. Ideally this distance has a sufficiently informative gradient. Following  \cite{ganin2018synthesizing}, we use the approximation of Wasserstein distance outlined in \cite{gulrajani2017improved}.

To summarize, the training process is comprised of the following. For each step $i$,
\begin{enumerate}
\item Evaluate samples $j\in [1\cdots N]$ using policy $f_j=\rho_{(\theta_i+\zeta_i)}(\Delta v_j,\,p_j)+\epsilon_j$ where $\zeta$ and $\epsilon$ are added parameter and action noise. \todo notation for $j$. The inputs are sampled from simulation $R$ modeling the bow and string reactions to the previous force, $(\Delta v_j,\,p_j)=R(f_{j-1} + f_L, f_p)$ where $f_L$ is the loading force that targets a desired bow velocity $v_\tau$ using a viscoelastic coupling and $f_p$ is the normal force of the bow in the secondary axis leading to pressure on the string at some position. The model $R$ in this work is implemented as in \needref such that the force on the string is distributed to a series of resonators simulating the independent modes of vibration of the string.
\item For the last $W$ samples, $W\leq N$, calculate spectrum $S_i$, and store each tuple $(\Delta v_j,\, p_j,\, f_j,\, S_i)$ in the replay buffer.
\item Randomly sample $M$ entries from the replay buffer.
\item Update $D_{\eta_{i+1}} \longleftarrow D_{\eta_i}(S_k)$ for each $k = [1 \cdots M]$ by gradient descent in order to maximize separation between real and generated samples subject to a Lipschitz constraint on the function's gradient \needref.
\item Calculate $D_k=D(S_k)$ for each, and expected reward $r_k=-D_k$. 
\item Update critic $C_{\gamma_{i+1}} \longleftarrow C_{\gamma_i}(\Delta_v, p, f)$. \todo loss function
\item Update actor policy $\rho_{\theta_{i+1}} \longleftarrow \rho_{\theta_i}(\Delta_v, p)$ by applying the critic's action gradient. \todo
\item Apply constraints, explained in the next section.
\end{enumerate}

\subsection{Achieving friction-like control}

% tentative section
\emph{
In practice we found that a completely open optimisation as described above was not sufficient to develop a friction-like control policy.  The policy often found tricks to apply constant force to individual harmonics without developing any significant ``corner'' vital to the Stribeck stick-slip effect. \todo Figure.  In order to encourage the desired behaviour, it was therefore found necessary to introduce some prior domain knowledge.  In particular we introduced constraints to encourage learning functions of the desired form; we constrained the policy $\rho$ to have a symmetric structure by additionally minimizing the squared difference between one side of the function and the negative of the other side of the function.  Since the action policy is updated by the critic's gradient, this was achieved post-hoc by applying a usual gradient descent update on this secondary loss after each $\theta_i$ update.
}

% tentative section
\emph{
Additionally, instead of giving $\rho$ complete informational freedom to independently decide how $f_s$ is applied to each mode, ie. $f_b=-f_s = -\sum f_m$ for each mode $m$, we restricted components $f_m$ to rely on certain information, which translated to a particular structure for $\rho$. In particular we divided it into a single output depending only on $\Delta v$ which was multiplied by a series of values $\lambda_j$ for each mode $j$. The sum $\sum \lambda_m$ was normalized to 1 so that the same force was applied in total to the string as was applied to the bow. In other words, $f_b=-f_s \frac{\lambda_m}{\sum \lambda_m} $ where $\lambda_m$ are restricted to be non-negative. Thus, the policy determines the scalar force magnitude $f=\rho^\Delta_\theta(\Delta v)$ only based on differential velocity, while the distribution of forces across the modes is only according to the pressure $\lambda_j=\rho^p_\theta(p)$. This avoids $\rho$ needing to approximate, independently, the same curve for each mode with varying magnitude, and ensures a physically-consistent policy over the resonator modes.
  }

\section{Experiments and results}

We tested this setup first to see if it could discover how to reproduce the bowing sound itself.  Secondly, we tested to see if it could discover a policy to reproduce non-bowing acoustic sounds using a friction-like mechanism.

This project is still in progress and thus far this method has shown some promise in recovering the original $\rho(\Delta v)$ used to generate a sound provided to it as a target spectrum when the distribution of forces to the individual resonator modes is provided.  It has quite a bit more difficulty assigning correct actions to individual modes if this distribution is left free, since it must infer tens of force values instead of a single one.  Therefore modulating the full spectrum is a drastically higher-dimensional problem.  In this sense the generality of the approach to synthesize any arbitrary sound is threatened in the performance witnessed so far.  However, the development of the correct $\rho$ policy, which results in a haptic force feedback signal that seems sensible with regards to haptic feedback in terms of its frequency distribution and amplitude range demonstrates the concept well.

From an initial set of small random parameters $\theta$, successful inference of $\rho^\star$  occurs after approximately N \todo episodes of 20 windows each of 2048 samples.  We set $\gamma=0.999$, such that for a full window, it decreases to $\gamma^2048 \approx 0.13$, allowing even the oldest samples in a window to have some influence on the critic target value.

In terms of exploration noise, we used small Laplacian action and parameter noise.  The choice of a Laplace distribution was due to the observation that noise tended to have a smoothing effect which discouraged the development of a sharp corner.  Having an exponential distribution allowed for noise with sudden impulses that gave the possibility of generating examples with non-smooth jumps between steps.  However, this result is informal at best and a full hyperparameter search is needed to be able to make empirical claims about the many ways we could vary choices of noise, network architectures, learning rates, and other hyper-parameters.  We also applied a smoothed random walk to the target bow velocity around 0.2, and randomly targetted the opposite direction on a per-episode basis in order to promote a symmetric $\rho$.  We found that adding symmetric examples to the replay buffer was not quite sufficient to guarantee symmetricity.

We found that when changing policies using parameter noise without resetting the episode, sudden impulses could be introduces which generated high-frequency oscillations in the string and lead to misleading training data.  This was mitigated nicely by adding ``policy cross-fading'', where for each action during an overlap period, two policies for successive windows are calculated and a cosing interpolation is used to smoothly vary from one to the next.

\bibliography{refs}
\bibliographystyle{abbrv}

\end{document}
