#!/usr/bin/env python

import tensorflow as tf
import numpy as np
import tflearn

class AutoEncoderDistance(object):
    """
    Input to the network is the observed state for real and generated
    samples, output is reconstruction error.
    """

    def __init__(self, sess, state_dim, latent_dim, learning_rate, real_sampler):
        self.sess = sess
        self.s_dim = state_dim
        self.latent_dim = latent_dim
        self.learning_rate = learning_rate
        self.real_sampler = real_sampler
        self.weight = 1

        # Create the encoder network
        self.inputs, self.labels, self.latent = self.create_encoder_network()

        # Create the decoder network
        self.outputs = self.create_decoder_network()

        # Define loss and optimization Op
        self.losses = tf.log(tf.reduce_mean((tf.square(self.inputs - self.outputs)), axis=1) + 1e-14)

        #self.mean_loss = tf.reduce_mean(tf.multiply(self.losses, self.labels))
        self.mean_loss = tf.reduce_mean(self.losses)
        self.optimize = tf.train.AdamOptimizer(self.learning_rate).minimize(self.mean_loss)
        self.optimize2 = tf.train.AdamOptimizer(self.learning_rate).minimize(-self.mean_loss)

    def create_encoder_network(self):
        inputs = tflearn.input_data(shape=[None, self.s_dim])
        labels = tflearn.input_data(shape=[None, 1])
        net = tflearn.fully_connected(inputs/40.0, 40)
        net = tflearn.layers.normalization.batch_normalization(net)
        net = tflearn.layers.dropout(net, 0.5)
        net = tflearn.activations.relu(net)
        net = tflearn.fully_connected(net, 30)
        net = tflearn.layers.dropout(net, 0.5)
        net = tflearn.activations.relu(net)
        latent = tflearn.fully_connected(net, self.latent_dim)
        return inputs, labels, latent

    def create_decoder_network(self):
        net = tflearn.fully_connected(self.latent, 30)
        net = tflearn.layers.normalization.batch_normalization(net)
        net = tflearn.layers.dropout(net, 0.5)
        net = tflearn.activations.relu(net)
        net = tflearn.fully_connected(net, 40)
        net = tflearn.layers.dropout(net, 0.5)
        net = tflearn.activations.relu(net)
        out = tflearn.fully_connected(net, self.s_dim)
        return out*40

    def train(self, inputs_generated):
        inputs_real = self.real_sampler(np.shape(inputs_generated)[0])
        self.sess.run([self.outputs, self.optimize], feed_dict={
            self.inputs: inputs_real,
            self.labels: np.ones((np.shape(inputs_real)[0],1))
        })
        self.sess.run([self.outputs, self.optimize2], feed_dict={
            self.inputs: inputs_generated,
            self.labels: np.ones((np.shape(inputs_generated)[0],1))
        })

    def reconstruct(self, inputs):
        return self.sess.run(self.outputs, feed_dict={
            self.inputs: inputs,
            self.labels: np.ones((inputs.shape[0],1)),
        })

    def reconstruction_loss(self, inputs):
        return self.sess.run(self.losses, feed_dict={
            self.inputs: inputs,
            self.labels: np.ones((np.shape(inputs)[0],1)),
        })

    def __call__(self, p):
        d = self.reconstruction_loss(p.reshape((-1,p.shape[0])))
        return d

class LogItakuraSaitoDistance(object):
    """
    Input to the network is the observed state for real and generated
    samples, output is reconstruction error.
    """

    def __init__(self, real_sampler, sess=None, state_dim=None, latent_dim=None,
                 learning_rate=None, frequency_weighted=False):
        # With same arguments as AutoEncoderDistance but we ignore
        # most of them, just for easy replacement.
        self.real_sampler = real_sampler
        self.frequency_weighted = frequency_weighted
        self.weight = 1

    def reconstruction_loss(self, inputs):
        assert len(np.shape(inputs))==2
        real = self.real_sampler(np.shape(inputs)[0])
        y = np.zeros(np.shape(inputs)[0])
        if self.frequency_weighted:
            if np.shape(self.weight) != np.shape(inputs)[1:]:
                self.weight = np.linspace(1,0,np.shape(inputs)[1]+1)[:-1]
                self.weight /= np.sum(self.weight)
        for i in range(np.shape(inputs)[0]):
            r = (10**(real[i]/20))/((10**inputs[i]/20) + 1e-14)
            y[i] = np.log(1/(2*np.pi)*np.mean((r-np.log(r)-1)*self.weight) + 1e-14)
        return (y-25)/25

    def train(self, inputs_generated):
        pass

    def reconstruct(self, inputs):
        return inputs*0

    def __call__(self, p):
        d = self.reconstruction_loss(p.reshape((-1,p.shape[0])))
        return d

class MeanSquareDistance(object):
    """
    Input to the network is the observed state for real and generated
    samples, output is reconstruction error.
    """

    def __init__(self, real_sampler, sess=None, state_dim=None, latent_dim=None,
                 learning_rate=None, frequency_weighted=False):
        # With same arguments as AutoEncoderDistance but we ignore
        # most of them, just for easy replacement.
        self.real_sampler = real_sampler
        self.frequency_weighted = frequency_weighted
        self.weight = 1

    def reconstruction_loss(self, inputs):
        assert len(np.shape(inputs))==2
        real = self.real_sampler(np.shape(inputs)[0])
        if self.frequency_weighted:
            if np.shape(self.weight) != np.shape(inputs)[1:]:
                self.weight = np.linspace(1,0,np.shape(inputs)[1]+1)[:-1]
                self.weight /= np.sum(self.weight)
        y = np.mean((inputs-real)**2 * self.weight, axis=1)
        return y / 1000 - 1

    def train(self, inputs_generated):
        pass

    def reconstruct(self, inputs):
        return inputs*0

    def __call__(self, p):
        d = self.reconstruction_loss(p.reshape((-1,p.shape[0])))
        return d

class WassersteinDistance(object):
    """
    Input to the network is the observed state for real and generated
    samples, output is wassertein critic distance.
    """

    def __init__(self, sess, state_dim, learning_rate, real_sampler, use_convnet=False):
        self.sess = sess
        self.s_dim = state_dim
        self.learning_rate = learning_rate
        self.real_sampler = real_sampler
        self.convnet = use_convnet

        # Create the classifier network (linear output)
        self.inputs = tflearn.input_data(shape=[None, self.s_dim])
        self.outputs, var = self.create_discriminator_network(self.inputs)

        self.network_params = var
        self.network_params_inputs = [tf.placeholder(shape=w.shape, dtype=tf.float32)
                                      for w in self.network_params]
        self.assign_network_params = [tf.assign(z, w) for z, w in
                                      zip(self.network_params, self.network_params_inputs)]

        # Define loss and optimization Op
        self.labels = tflearn.input_data(shape=[None, 1])
        self.losses = self.outputs * self.labels
        self.mean_loss = tf.reduce_mean(self.losses)

        # Gradient penalty (Lipschitz-1 constraint)
        norm_grad = tf.reduce_mean(tf.square(tf.gradients(self.losses, self.inputs)))
        self.gp = tf.reduce_mean(tf.square(norm_grad - 1))

        self.optimize = tf.train.AdamOptimizer(self.learning_rate).minimize(self.mean_loss
                                                                            + 10*self.gp)
        self.offset = 0
        self.scale = 1
        self.weight = 1

    def create_discriminator_network(self, inputs, reuse=False):
        var = []
        # note: removed batchnorm due to using gradient penalty
        # https://medium.com/@jonathan_hui/gan-wasserstein-gan-wgan-gp-6a1a2aa1b490
        inputs = tf.pow(10.0, inputs/20)
        if self.convnet:
            # try a conv net
            inp = tf.expand_dims(inputs, 2)
            net = tflearn.layers.conv.conv_1d(inp, 128, 3, reuse=reuse, scope='distconv1')
            var.append(net.W)
            # net = tflearn.layers.normalization.batch_normalization(net)
            net = tflearn.activations.relu(net)
            net = tflearn.layers.conv.max_pool_1d(net, 2)
            # net = tflearn.layers.dropout(net, 0.5)
            net = tflearn.layers.conv.conv_1d(net, 64, 3, reuse=reuse, scope='distconv2')
            var.append(net.W)
            # net = tflearn.layers.normalization.batch_normalization(net)
            net = tflearn.activations.relu(net)
            net = tflearn.layers.conv.max_pool_1d(net, 2)
            # net = tflearn.layers.dropout(net, 0.5)
            net = tflearn.layers.flatten(net)
        else:
            # fully-connected MLP
            net = tflearn.fully_connected(inputs, 400, reuse=reuse, scope='distfc1')
            var.append(net.W)
            # net = tflearn.layers.normalization.batch_normalization(net)
            # net = tflearn.layers.dropout(net, 0.5)
            net = tflearn.activations.relu(net)
            net = tflearn.fully_connected(net, 100, reuse=reuse, scope='distfc2')
            var.append(net.W)
            # net = tflearn.layers.normalization.batch_normalization(net)
            # net = tflearn.layers.dropout(net, 0.5)
            net = tflearn.activations.relu(net)
            net = tflearn.fully_connected(net, 50, reuse=reuse, scope='distfc3')
            var.append(net.W)
            # net = tflearn.layers.normalization.batch_normalization(net)
            # net = tflearn.layers.dropout(net, 0.5)
            net = tflearn.activations.relu(net)
        net = tflearn.fully_connected(net, 16, reuse=reuse, scope='distfcshared')
        var.append(net.W)
        # net = tflearn.layers.normalization.batch_normalization(net)
        # net = tflearn.layers.dropout(net, 0.5)
        net = tflearn.activations.relu(net)
        outputs = tflearn.fully_connected(net, 1, reuse=reuse, scope='distfcout')
        var.append(outputs.W)
        return outputs, var

    def create_network_with_input(self, inputs):
        return self.create_discriminator_network(inputs, reuse=True)[0]

    def train(self, inputs_generated):
        inputs_real = self.real_sampler(np.shape(inputs_generated)[0])
        labels = np.vstack([-np.ones((np.shape(inputs_real)[0],1)),
                            np.ones((np.shape(inputs_generated)[0],1))])
        tflearn.is_training(True)
        losses,_ = self.sess.run([self.losses, self.optimize], feed_dict={
            self.inputs: np.vstack([inputs_real, inputs_generated]),
            self.labels: labels,
        })

        # Weight clipping (Lipschitz-1 constraint)
        if False:
            w = self.sess.run(self.network_params)
            self.sess.run(self.assign_network_params,
                          feed_dict = {x: z for x,z in zip(self.network_params_inputs,
                                                           [np.clip(_, -0.001, 0.001)
                                                            for _ in w])})

        # Correct for offset on "real" data, we want the real data to
        # have a distance of zero.
        mean_real = np.mean([-l for l,lab in zip(losses,labels) if lab < 0])
        mean_fake = np.mean([ l for l,lab in zip(losses,labels) if lab > 0])
        # self.offset -= 0.5*(mean_real + self.offset)
        # self.scale += 0.5*(1/np.abs(mean_fake-mean_real) - self.scale)
        self.scale = 1.0/30000.0

        # Return the gap between average "real" and average
        # "generated" distances
        return ((np.mean([-l for l,lab in zip(losses,labels) if lab < 0]) + self.offset)*self.scale,
                (np.mean([ l for l,lab in zip(losses,labels) if lab > 0]) + self.offset)*self.scale)

    def reconstruction_loss(self, inputs):
        tflearn.is_training(False)
        return (self.sess.run(self.losses, feed_dict={
            self.inputs: inputs,
            self.labels: -np.ones((np.shape(inputs)[0],1)),
        })[:,0] + self.offset) * self.scale

    def __call__(self, p):
        d = self.reconstruction_loss(p.reshape((-1,p.shape[0])))
        return d


def test_autoencoder():
    with tf.Session() as sess:
        def sampler(n):
            return np.ones((n,1))*0.5
        tflearn.is_training(True)
        dist = AutoEncoderDistance(sess, state_dim=1, latent_dim=3,
                                   learning_rate=0.001, real_sampler=sampler)

        sess.run(tf.global_variables_initializer())
        x = np.random.uniform(-1,1,(10,1))
        x = np.sort(x, axis=0)

        from matplotlib import pyplot as plt
        plt.plot([x.min(), x.max()], [0.5,0.5], '--k')
        #plt.plot(x, [dist(y) for y in x], '-o')
        for i in range(1000):
            dist.train(np.random.uniform(-1,1,(10,1)))
        tflearn.is_training(False)
        plt.plot(x, [dist(y) for y in x], '-o')
        plt.show()

def test_wasserstein():
    with tf.Session() as sess:
        def sampler(n):
            return np.ones((n,1))*0.5
        dist = WassersteinDistance(sess, state_dim=1,
                                   learning_rate=0.001, real_sampler=sampler)

        sess.run(tf.global_variables_initializer())
        x = np.random.uniform(-1,1,(10,1))
        x = np.sort(x, axis=0)

        #plt.plot(x, [dist(y) for y in x], '-o')
        tflearn.is_training(True)
        for i in range(1000):
            dist.train(np.random.uniform(-1,1,(100,1)))
        tflearn.is_training(False)

        # from matplotlib import pyplot as plt
        # plt.plot(0.5, 0, 'xk')
        # x = np.linspace(-1,1,30).reshape((30,1))
        # plt.plot(x, [dist(y)[0] for y in x], '-o')
        # plt.show()

if __name__=='__main__':
    #test_autoencoder()
    test_wasserstein()
