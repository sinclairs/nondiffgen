#!/usr/bin/env python3

# Test positioning sine waves to emulate a desired spectrum using
# policy gradients

import gym.spaces
import numpy as np
from matplotlib import pyplot as plt
import scipy.signal

t = np.linspace(0,1.0,480)
w = scipy.signal.hamming(480)
def sinusoid_mixture(fs,As):
    return np.sum([np.cos(2.0*np.pi*int(f)*t)*a for f,a in zip(fs,As)], axis=0)
def observe(fs,As):
    #return np.abs(np.fft.fft(w*sinusoid_mixture(fs,As))[:t.shape[0]//2])
    return 20*np.log10(np.abs(np.fft.fft(w*sinusoid_mixture(fs,As))[:t.shape[0]//2])+1e-14)

y_params = [60,100,160], [1.0,0.5,0.9]
y_params = [100], [0.9]
y_state = np.hstack([(np.array(y_params[0])/240.0-0.5)*2,
                     (np.array(y_params[1])-0.5)*2]).reshape((len(y_params[0])*len(y_params),))
y_target = observe(*y_params)
# y_target = observe([100], [1.0])

def presum_dist(fs,As):
    x = observe(fs, As)
    # return np.abs(y_target * np.log(np.abs(y_target / x)))
    # return np.abs(x*np.log(np.abs(x / y_target)))
    return (x-y_target)**2

def dist(s):
    # fs,As = (s[::2]/2+0.5)*240, s[1::2]/2+0.5
    # return np.mean(presum_dist(fs,As))
    return np.mean((s-y_target)**2)

def freqsweep():
    theta = np.linspace(1,241,241)
    d = theta.copy()
    for n,th in enumerate(theta):
        d[n] = dist([th],[0.9])
    plt.clf()
    plt.plot(theta, d)
    plt.plot(y_target*100)
    plt.show()

class SineTest(object):
    """AI Gym-like environment"""
    def __init__(self):
        self.action_space = gym.spaces.Box( -np.ones(2), np.ones(2), dtype=np.float32)
        # self.observation_space = gym.spaces.Box( -10000*np.ones(241),
        #                                          10000*np.ones(241), dtype=np.float)
        # self.observation_space = gym.spaces.Box( -np.ones(2), np.ones(2), dtype=np.float)
        self.observation_space = gym.spaces.Box(-40*np.ones(240), 40*np.ones(240),dtype=np.float32)
        self.dist = dist
        self.reset()

    def set_dist(self, dist):
        """Change the distance function"""
        self.dist = dist

    def reset(self):
        self.state = np.zeros(2, dtype=np.float)+0.5
        self.state[0] = 0.0
        self.state[1] = 0.0
        # self.state[2] = 0.5
        return self.observe()

    def reward(self):
        #return -dist((self.state[:3]/2+0.5)*240, self.state[3:6]/2+0.5)
        fa = np.array([((f/2+0.5)*240, (a/2+0.5))
                       for f,a in zip(self.state[::2],self.state[1::2])])
        f, a = fa[:,::2], fa[:,1::2]
        d = self.dist(self.observe())
        # d = self.dist(fa.reshape((fa.shape[0]*fa.shape[1],))))
        return (-d + 500)/500
        # if d > 40*40: d = 40*40 # cutoff at 40 dB of difference
        # return -d/(40*40)+1.0 + (np.sum(a) > 1e-4)*0.1

    def seed(self, s):
        pass

    def observe(self):
        #o = self.state
        # return self.state
        p = (self.state[:1]/2+0.5)*240, self.state[1:2]/2+0.5
        o = observe(*p).astype(np.float32)
        # o = o * (o>-40) * (o<40)
        return o

    def step(self, action):
        self.state = np.tanh(action)
        # self.state = np.tanh(self.state+action) #np.tanh(action)
        # print('action',action,end='\t')
        # print('state',self.state,end='\t')
        # print('reward',self.reward())
        # o = observe(self.state[:3]*240, self.state[3:6]/2+0.5)
        # self.render()
        return self.observe(), self.reward(), False, None

    def close(self):
        pass

    def render(self):
        # print('render state',self.state)
        plt.figure(1).clear()
        plt.subplot(211)
        #p = (self.state[:3]/2+0.5)*240, self.state[3:6]/2+0.5
        p = (self.state[:1]/2+0.5)*240, self.state[1:2]/2+0.5
        plt.plot(observe(*p))
        plt.plot(y_target)
        plt.subplot(212)
        plt.plot(presum_dist(*p))
        plt.draw()
        plt.pause(0.0001)
        # plt.show()

    def sample_real(self, num):
        # so random! /s
        return np.array([y_target]*num)
        # return np.array([y_state]*num)

if __name__=='__main__':
    s = SineTest()
    #s.render()
    r = np.zeros(1000)
    for n,i in enumerate(np.linspace(-1,1,1000)):
        s.state = np.array([i,0.0])
        r[n] = s.reward()

    plt.clf()
    plt.plot(r)
    plt.show()
