#!/usr/bin/env python3

# Test positioning sine waves to emulate a desired spectrum using
# policy gradients

# In this version each step is a single sample, expected that the
# policy will be called to control every sample.

import gym.spaces
import numpy as np
import scipy.signal
from matplotlib import pyplot as plt

desired_frequency = 6375.0
desired_amplitude = 0.7

class OnlineSignalEnvironment(object):
    """Abstraction for an AI Gym-like environment for online signal
    generation."""
    def __init__(self, window_width=256, hop_width=64, sample_rate=48000, initial_action=None):
        self.NWINDOW = window_width
        self.NFFT = window_width//2
        self.w = scipy.signal.hann(self.NWINDOW)
        self.hop = hop_width
        self.fs = sample_rate
        self.nyquist = self.fs/2
        self.buffer = np.zeros(self.NWINDOW)
        self.bufferpos = 0
        # TODO: observation space = spectrum?  i think not.. must be different per-policy
        # self.observation_space = gym.spaces.Box(-40*np.ones(self.NWINDOW),
        #                                         40*np.ones(self.NWINDOW),dtype=np.float32)
        self.w_target = (np.cos(
            desired_frequency*2*np.pi*np.linspace(0,self.NWINDOW/self.fs,self.NWINDOW))
                         * desired_amplitude)
        self.y_target = self.log_spectrum(self.w_target)
        self.dist = self.default_dist
        self.action_vars = []
        self.initial_action = initial_action
        self.reset()

    def seed(self, s):
        pass

    def reset(self):
        self.buffer = np.zeros(self.NWINDOW)
        self.bufferpos = 0
        self.last_sample = 0
        return self.observe()

    def update(self):
        self.last_sample = 0

    def reward(self):
        d = self.dist(self.observe())
        return -d

    def observe(self):
        return []

    def log_spectrum(self, sig=None):
        if sig is None:
            W = np.hstack([self.buffer[self.bufferpos:],
                           self.buffer[:self.bufferpos]])
            self.W = W
        else:
            W = sig
        y = 20*np.log10(np.abs(np.fft.fft(self.w*W)[:self.NFFT])+1e-14)
        #y = np.abs(np.fft.fft(self.w*W)[:self.NFFT])
        #y *= (y>-40) * (y<40) - 40 * (y<=-40) * (y>=40)
        return y

    def step(self, action):
        return self.observe(), self.reward(), False, None

    def step_sample(self, action):
        self.apply_action(action)
        self.update()
        return self.last_sample

    def output_sample(self, sample):
        self.buffer[self.bufferpos] = sample
        self.bufferpos = (self.bufferpos+1)%self.NWINDOW
        self.last_sample = sample

    def get_buffer(self):
        return np.hstack([self.buffer[self.bufferpos:],
                          self.buffer[:self.bufferpos]])

    def close(self):
        pass

    def default_dist(self, y):
        """By default, use mean squared error to desired spectrum."""
        return np.mean((y - self.y_target)**2)

    def set_dist(self, dist):
        self.dist = dist

    def apply_action(self, action):
        for v,a in zip(self.action_vars, action):
            v.set_target(a)

    def run_with_policy(self, policy, steps=1):
        y = np.zeros(steps)
        for k in range(steps):
            y[k] = self.step_sample(policy([self.observe()])[0])
        return y

    def render(self):
        plt.figure(1).clear()
        plt.subplot(211)
        plt.plot(self.window)
        plt.subplot(212)
        plt.plot(self.observe())
        plt.draw()
        plt.pause(0.0001)
        # plt.show()

    def sample_real(self, n):
        return np.vstack([self.y_target]*n)
        # We want to be insensitive to phase so we randomize it.
        phi = np.random.uniform(-np.pi, np.pi, n)
        # To avoid spectral aliasing we also need to have some
        # variance in the example frequencies around the target.
        f = desired_frequency + np.random.normal(0,0.01,n)
        a = desired_amplitude + np.random.normal(0,0.001,n)
        ti = np.linspace(0,self.NWINDOW/self.fs,self.NWINDOW)
        w = np.zeros((n, self.NWINDOW))
        for i in range(n):
            w[i] = np.cos((f[i]*2*np.pi+phi[i])*ti) * a[i]
        y = self.log_spectrum(w)
        return y

class TargetFollower(object):
    def __init__(self, target, time_constant, fs, initial_state=0, trace_len=256):
        self.state = initial_state
        self.k = 1-np.exp(-(1/fs)/time_constant) # time to drop 36.8%
        self.target = target
        self.trace = np.ones(trace_len)*initial_state
        self.trace_pos = 0

    def set_target(self, target):
        self.target = float(target)

    def get_trace(self):
        return np.hstack([self.trace[self.trace_pos:],
                          self.trace[:self.trace_pos]])

    def get_last(self):
        return self.trace[(self.trace_pos-1)%self.trace.shape[0]]

    def update(self):
        self.trace[self.trace_pos] = self.state
        self.trace_pos = (self.trace_pos+1)%len(self.trace)
        self.delta = (self.target - self.state)*self.k
        self.state = self.state + self.delta
        return self.state

    def sample(self):
        return self.state

class Constant(TargetFollower):
    def __init__(self, value):
        super().__init__(value, 1, 1, value, 0)

    def update(self):
        self.state = self.target
        return self.state

class FrequencyTest(OnlineSignalEnvironment):
    """AI Gym-like environment; policy must set the frequency of a
    sinusoid to match a predetermined spectrum, it changes slowly by a
    low-pass filter when set."""
    def __init__(self, window_width=256, hop_width=64, sample_rate=48000, initial_action=None):
        self.action_space = gym.spaces.Box( np.array([0.0]), np.array([sample_rate/2]),
                                            dtype=np.float32 )
        self.observation_space = None
        self.observation_space = gym.spaces.Box( np.array([-2.0]), np.array([2.0]),
                                                 dtype=np.float32 )
        super().__init__(window_width, hop_width, sample_rate, initial_action)

    def perfect_policy(self, observations):
        return np.array([[desired_frequency]]*len(observations))

    def reset(self, initial_action=None):
        if initial_action is not None:
            self.initial_action = initial_action
        if self.initial_action is None:
            f = np.random.uniform(0, self.nyquist)
        else:
            f = self.initial_action[0]
        tc = self.NWINDOW/2/self.fs
        self.freq = TargetFollower(f, tc, self.fs, f, self.NWINDOW)
        self.amp = Constant(desired_amplitude)
        self.action_vars = (self.freq,)
        self.phase_rate = self.fs/(f*2*np.pi)
        self.phase = np.random.uniform(-np.pi, np.pi)
        self.frame_counter = 0
        return super().reset()

    def update(self):
        [v.update() for v in self.action_vars]
        self.phase_rate = 2*np.pi*self.freq.sample()/self.fs
        self.phase += self.phase_rate
        if self.phase > np.pi: self.phase -= 2*np.pi
        self.output_sample(np.cos(self.phase)*self.amp.sample())

    def observe(self):
        return np.random.uniform(0,1,(1,))

    def render(self, episode, step, env, dist, actor, critic, spectrum, traces,
               a_batch, y_i, predicted_q_value):
        step_trace, reward_trace, action_trace_noisy, action_trace_clean = traces
        if not hasattr(self, 'eval_spec'):
            self.eval_spec = np.zeros((960,self.NFFT))
            self.eval_fr = np.linspace(25,self.fs//2,self.eval_spec.shape[0])
            self.eval_ti = np.linspace(0,self.NWINDOW/self.fs,self.NWINDOW)
            for k,f in enumerate(self.eval_fr):
                self.eval_spec[k] = self.log_spectrum(np.cos(self.eval_ti*np.pi*2*f))
        fig = plt.figure(1, figsize=(5,8))
        fig.clear()
        ax = fig.add_subplot(611)
        ax.plot(np.linspace(0, 24000, spectrum.shape[0]), self.y_target)
        ax.plot(np.linspace(0, 24000, spectrum.shape[0]), spectrum)
        ax = fig.add_subplot(612)
        ax.plot(self.w_target)
        ax.plot(self.W)
        ax = fig.add_subplot(613)
        ax.plot([t[1] for t in step_trace])
        ax.plot(np.arange(self.hop+self.NWINDOW*3,self.hop+self.NWINDOW*4),
                self.freq.get_trace())
        ax = fig.add_subplot(614)
        ax.plot(action_trace_noisy[0], 'r', alpha=0.5,
                )#label='noisy=%0.01f'%long_trace_noisy[0][-1])
        ax.plot(action_trace_clean[0], 'b', alpha=0.5,
                label='%0.01f'%action_trace_clean[0][-1])
        ax.legend(loc=2)
        ax = fig.add_subplot(615)
        plt.plot(reward_trace)
        ax = fig.add_subplot(616)
        ax.plot(self.eval_fr, -dist.reconstruction_loss(self.eval_spec), alpha=0.4)
        ax.plot(self.eval_fr, critic.predict(np.random.uniform(0,1,(self.eval_fr.shape[0],1)),
                                             self.eval_fr.reshape((-1,1))), alpha=0.4)
        ax.plot(a_batch, np.array(y_i), 'yx', label='y_i')
        ax.plot(a_batch, predicted_q_value, 'rx', label='predicted_q')

        fig.suptitle('Episode %d, frame %d'%(episode,step))
        fig.savefig('results/now.png')
        fig.savefig('results/frame%06d.png'%self.frame_counter)
        self.frame_counter += 1


class FreqAmpTest(FrequencyTest):
    """AI Gym-like environment; policy must set the frequency and
    amplitude of a sinusoid to match a predetermined spectrum, it
    changes slowly by a low-pass filter when set."""
    def __init__(self, window_width=256, hop_width=64, sample_rate=48000, initial_action=None):
        self.action_space = gym.spaces.Box( np.array([0.0,0.0]),
                                            np.array([sample_rate/2,1.0]), dtype=np.float32 )
        self.observation_space = None
        self.observation_space = gym.spaces.Box( np.array([-2.0]), np.array([2.0]),
                                                 dtype=np.float32 )
        super().__init__(window_width, hop_width, sample_rate, initial_action)

    def reset(self, initial_action=None):
        if initial_action is not None:
            self.initial_action = initial_action
        if self.initial_action is None:
            f = np.random.uniform(0, self.nyquist)
            a = 1.0
        else:
            f, a = self.initial_action
        self.freq = TargetFollower(f, self.NWINDOW/2/self.fs, self.fs, f, self.NWINDOW)
        self.amp = TargetFollower(a, self.NWINDOW/2/self.fs, self.fs, a, self.NWINDOW)
        self.action_vars = (self.freq, self.amp)
        self.phase_rate = self.fs/(f*2*np.pi)
        self.phase = np.random.uniform(-np.pi, np.pi)
        return super().reset()

    def perfect_policy(self, observations):
        return np.array([[desired_frequency, desired_amplitude]]*len(observations))

    def observe(self):
        return np.random.uniform(0,1,(1,))

class ShapingTest(FrequencyTest):
    """AI Gym-like environment; policy must determine the output of the
    signal taking phase as input, i.e. determine how to transform
    phase by cos(phase)."""
    def __init__(self, window_width=256, hop_width=64, sample_rate=48000, initial_action=None):
        self.action_space = gym.spaces.Box( np.array([0.0,0.0]),
                                            np.array([sample_rate/2,1.0]), dtype=np.float32 )
        self.observation_space = gym.spaces.Box( np.array([0.0,]),
                                            np.array([1.0,]), dtype=np.float32 )
        super().__init__(window_width, hop_width, sample_rate, initial_action)

    def reset(self, initial_action=None):
        if initial_action is not None:
            self.initial_action = initial_action
        if self.initial_action is None:
            s = np.random.uniform(0, self.nyquist)
        else:
            s = self.initial_action[0]
        # TODO: slowly change frequency and amplitude randomly
        f = desired_frequency
        a = desired_amplitude
        self.freq = TargetFollower(f, self.NWINDOW/2/self.fs, self.fs, f, self.NWINDOW)
        self.amp = TargetFollower(a, self.NWINDOW/2/self.fs, self.fs, a, self.NWINDOW)
        self.phase_rate = self.fs/(f*2*np.pi)
        self.phase = np.random.uniform(-np.pi, np.pi)
        return super().reset()

    def perfect_policy(self, observations):
        # We are trying to have the policy model the cosine function
        return [np.cos(o) for o in observations]

    def apply_action(self, action):
        # Output is taken directly as the output of the policy
        self.last_action = action

    def observe(self):
        # Action is based on previously calculated phase so we must
        # observe it
        return self.phase

    def update(self):
        # Output policy result directly
        self.output_sample(self.last_action)

        # Prepare for next policy application by updating phase
        self.freq.update(); self.amp.update()
        self.phase_rate = 2*np.pi*self.freq.sample()/self.fs
        self.phase += self.phase_rate
        if self.phase > np.pi: self.phase -= 2*np.pi

class PhasorTest(FrequencyTest):
    """AI Gym-like environment; more difficult than FrequencyTest and
    ShapingTest, policy must determine the increment of a phasor to
    match a predetermined spectrum of a single frequency."""
    # TODO note this is not really different from finding the right
    # frequency, just one parameter to match. more difficult would be
    # a task that takes the phase and returns a new phase, then it
    # would need to form a function

    def __init__(self, window_width=256, hop_width=64, sample_rate=48000, initial_action=None):
        self.action_space = gym.spaces.Box( np.array([0.0,0.0]),
                                            np.array([sample_rate/2,1.0]), dtype=np.float32 )
        self.observation_space = None
        super().__init__(window_width, hop_width, sample_rate, initial_action)

    def reset(self, initial_action=None):
        if initial_action is not None:
            self.initial_action = initial_action
        if self.initial_action is None:
            pr = np.random.uniform(0, self.fs/(self.nyquist*2*np.pi))
        else:
            pr = self.initial_action[0]
        f = self.fs*(pr/(2*np.pi))
        self.freq = TargetFollower(f, self.NWINDOW/2/self.fs, self.fs, f, self.NWINDOW)
        self.phase_rate = TargetFollower(pr, self.NWINDOW/2/self.fs, self.fs, pr, self.NWINDOW)
        self.action_vars = (self.phase_rate,)
        self.phase = np.random.uniform(-np.pi, np.pi)
        OnlineSignalEnvironment.reset(self)

    def perfect_policy(self, observations):
        return np.array([[2*np.pi*self.freq.sample()/self.fs]]*len(observations))

    def update(self):
        [v.update() for v in self.action_vars]
        # informational
        self.freq.target = self.freq.state = self.fs*(self.phase_rate.sample()/(2*np.pi))
        self.freq.update()
        self.phase += self.phase_rate.sample()
        if self.phase > np.pi: self.phase -= 2*np.pi
        self.output_sample(np.cos(self.phase)*desired_amplitude)

class FollowFreqTest(FrequencyTest):
    """AI Gym-like environment; more difficult than FrequencyTest, policy
    must match a transform of the observed signal's frequency as it
    changes."""
    # TODO in the future, this test requires a distance function that
    # is conditional on the observation.  (Calculate distance from
    # frequency-dependent generated signal -- should be possible using
    # a conditional discriminator but let's leave it for future work.)

def run_test(env_class, initial_action, imperfect_action, imperfect_policy=None):
    from matplotlib import pyplot as plt

    env = env_class(initial_action=initial_action)
    fr = np.linspace(0, env.nyquist, env.NFFT)
    ti = np.linspace(0, env.NWINDOW/env.fs, env.NWINDOW)

    # Display target state
    plt.subplot(432)
    plt.text(ti[0]-ti[-1]/2, 0, 'target signal:', horizontalalignment='right')
    plt.plot(ti, env.w_target); plt.xticks([]); plt.ylim(-1,1)
    plt.subplot(433)
    plt.plot(fr, env.y_target); plt.xticks([]); plt.ylim(-70,70)

    # Run initial state
    y = env.run_with_policy(env.perfect_policy, env.NWINDOW)
    plt.subplot(434)
    plt.plot(ti, env.freq.get_trace()); plt.xticks([]); plt.ylim(2500,6500)
    plt.subplot(435)
    plt.plot(ti, y); plt.xticks([]); plt.ylim(-1,1)
    plt.subplot(436)
    plt.plot(fr, env.log_spectrum()); plt.xticks([]); plt.ylim(-70,70)
    plt.text(fr[-1], 65, 'r=%0.02f'%env.reward(), horizontalalignment='right',
             verticalalignment='top')

    # Show changing state
    if imperfect_action is not None:
        imperfect_policy = lambda observations: [imperfect_action]*len(observations)
    y = env.run_with_policy(imperfect_policy, env.NWINDOW)
    plt.subplot(437)
    plt.plot(ti, env.freq.get_trace()); plt.xticks([]); plt.ylim(2500,6500)
    plt.subplot(438)
    plt.plot(ti, y); plt.xticks([]); plt.ylim(-1,1)
    plt.subplot(439)
    plt.plot(fr, env.log_spectrum()); plt.xticks([]); plt.ylim(-70,70)
    plt.text(fr[-1], 65, 'r=%0.02f'%env.reward(), horizontalalignment='right',
             verticalalignment='top')

    # Continue changing state
    y = env.run_with_policy(imperfect_policy, env.NWINDOW)
    plt.subplot(4,3,10)
    plt.plot(ti, env.freq.get_trace()); plt.xticks([]); plt.ylim(2500,6500)
    plt.subplot(4,3,11)
    plt.plot(ti, y); plt.xticks([]); plt.ylim(-1,1)
    plt.subplot(4,3,12)
    plt.plot(fr, env.log_spectrum()); plt.xticks([]); plt.ylim(-70,70)
    plt.text(fr[-1], 65, 'r=%0.02f'%env.reward(), horizontalalignment='right',
             verticalalignment='top')

    plt.suptitle(env_class.__name__)
    plt.show()

if __name__=='__main__':
    selected_test = 'FrequencyTest'
    import sys
    if len(sys.argv) > 1:
        if sys.argv[1] == '--simple-prompt':
            if len(sys.argv) > 2:
                selected_test = sys.argv[2]
        else:
            selected_test = sys.argv[1]
    if selected_test == 'FrequencyTest':
        run_test(FrequencyTest, [desired_frequency], [3000.0])
    elif selected_test == 'FreqAmpTest':
        run_test(FreqAmpTest, [desired_frequency, desired_amplitude], [3000.0, 1.0])
    elif selected_test == 'ShapingTest':
        run_test(ShapingTest, [desired_frequency], None, lambda x: [np.cos(x[0]*1.1)])
    elif selected_test == 'PhasorTest':
        run_test(PhasorTest, [desired_frequency*2*np.pi/48000.0], [3000.0*2*np.pi/48000.0])
    elif selected_test == 'FollowFreqTest':
        run_test(FollowFreqTest, [desired_frequency], [3000.0])
    elif selected_test == 'Bowing':
        run_test(Bowing)
    else:
        print('No test: provide one of',', '.join([k for k in globals().keys() if 'Test' in k]))
