
#include <cmath>
#include <iostream>
#include <Eigen/Dense>
#include <pybind11/eigen.h>
#include <functional>
#include <optional>

namespace py = pybind11;

typedef Eigen::MatrixXf Mat;
typedef const Eigen::MatrixXf& cMat;
Eigen::IOFormat HeavyFmt(Eigen::FullPrecision, 0, ", ", ";\n", "[", "]", "[", "]");

std::vector< std::vector< std::pair<Mat, Mat> > > weights;

void set_layer_weights(unsigned int weights_set, unsigned int layer_index,
                       cMat layer_weights, cMat layer_biases)
{
    while (weights_set >= weights.size())
        weights.push_back(std::vector< std::pair<Mat, Mat> >());

    auto& set = weights[weights_set];

    if (layer_index >= set.size())
        set.push_back(std::make_pair<cMat,cMat>(layer_weights, layer_biases));
    else
        set[layer_index] = std::make_pair<cMat,cMat>(layer_weights, layer_biases);
}

static
void ReLU(Mat& x)
{
    for( int i = 0; i < x.rows(); ++i )
    {
        for( int j=0; j < x.cols(); ++j )
            if (x(i,j) < 0)
                x(i,j) = 0.0;
    }
}

Mat run_weight_set(unsigned int weight_set, const Mat& inputs)
{
    Mat result(inputs);
    unsigned int N = weights[weight_set].size();
    for (unsigned int i=0; i < N; i++)
    {
        const auto& layer = weights[weight_set][i];
        cMat w = layer.first;
        cMat b = layer.second;
        result = result*w + b.replicate(1,result.rows()).transpose();
        if (i < N-1)
            ReLU(result);
    }
    return result;
}

class InertialMass
{
public:
    float mass;
    float damping;
    float fs;
    float x, v;

    InertialMass(float _mass, float _damping, float _sampling_freq)
    {
        this->mass = _mass;
        this->damping = _damping;
        this->fs = _sampling_freq;
        this->reset();
    }

    void reset()
    {
        this->x = x;
        this->v = v;
    }

    float step(float external_force=0)
    {
        float f = - this->damping*this->v + external_force;
        this->v += f/this->mass;
        this->x += this->v;
        return this->x;
    }
};

class ResonatorBank
{
public:
    Mat mass;
    Mat frequency;
    Mat damping_ratio;
    Mat points;
    float fs;
    unsigned int NFREQS;
    Mat x, v;
    Mat theta;
    Mat damping;
    Mat a1, a2, y0, y1, y2;
    Mat stiffness;
    Mat P, R, F, A;
    Mat vPoints;

    ResonatorBank(cMat _mass, cMat _frequency, cMat _damping_ratio, cMat _points,
                  float _sampling_freq)
    {
        this->mass = _mass;
        this->frequency = _frequency;
        this->damping_ratio = _damping_ratio;
        this->points = _points.transpose();
        this->fs = _sampling_freq;
        this->NFREQS = _frequency.cols();
        this->tuning();
        this->reset();
    }

    void reset()
    {
        this->x = this->mass*0;
        this->v = this->mass*0;
    }

    void tuning()
    {
        this->theta = 2*M_PI*this->frequency.array()/this->fs;
        this->damping = 2*this->damping_ratio.cwiseProduct(this->theta);

        // coupling point coefficients
        this->P = Mat(this->points.rows(),this->NFREQS);
        for (unsigned int i=0; i < this->NFREQS; i++)
            for (unsigned int j=0; j < this->points.rows(); j++)
                this->P(j,i) = sinf(this->points(j,0) * ((i+1)*M_PI));

        this->tuning_filter();
        // this->tuning_spring_mass();

        this->F = Mat(1,1);
    }

    void tuning_filter()
    {
        // Filter formulation of resonator
        this->R = (this->damping/20.0f).unaryExpr([](float d){return powf(0.5, d);});
        this->a1 = -2*this->R.cwiseProduct(this->theta.array().cos().matrix());
        this->a2 = this->R.array().pow(2);

        // State variables
        this->y2 = this->y1 = this->y0 = Mat::Zero(1,this->NFREQS);
    }

    void tuning_spring_mass()
    {
        // Alternative formulation based on explicit integration of a
        // spring-mass system.  It is easier to understand but is less
        // stable than the filter formulation in higher modes.

        // Determine stiffnesses that give desired frequency for each mass.
        this->stiffness = this->mass*(this->theta.array().square().matrix());

        // State variables
        this->x = this->v = Mat::Zero(this->NFREQS,1);
    }

    float step(cMat extF)
    {
        // Calculate force at each point
        // (zero force on all but first point)
        // Force is a distribution
        this->F = this->P.cwiseProduct(extF.replicate(this->P.rows(),1)).colwise().sum();
        return this->step_filter();
        //return this->step_spring_mass()
    }

    float step(float external_force=0)
    {
        // Calculate force at each point
        // (zero force on all but first point)
        // Force is uniform

        Mat extF = Mat::Zero(this->P.rows(),1);
        extF(0,0) = external_force;
        this->F = this->P.cwiseProduct(extF.replicate(1, this->P.cols())).colwise().sum();
        return this->step_filter();
        //return this->step_spring_mass()
    }

    float step_spring_mass()
    {
        // Spring-mass formulation
        // (Just differs by damping)
        Mat f = this->F - this->stiffness*this->x - this->damping*this->v;
        this->v += f.cwiseQuotient(this->mass);
        this->x += this->v;
        this->vPoints = (this->P * this->v).rowwise().sum();
        return this->vPoints(0);
    }

    float step_filter()
    {
        // Acceleration
        this->A = this->F.cwiseQuotient(this->mass);

        // Update filter state
        this->y2 = this->y1;
        this->y1 = this->y0;
        this->y0 = this->A - this->a1.cwiseProduct(this->y1) - this->a2.cwiseProduct(this->y2);

        // Backward difference velocity
        this->vPoints = (this->P.cwiseProduct(this->y0.replicate(this->P.rows(),1)).rowwise().sum()
                         - this->P.cwiseProduct(this->y1.replicate(this->P.rows(),1)).rowwise().sum())
            .transpose();

        return this->vPoints(0,0);
    }
};

Mat test_resonator_bank()
{
    Mat mass(1,2), freq(1,2), damp(1,2), points(3,1);
    mass << 1, 1;
    freq << 100.0, 10.0;
    damp << 0.1, 0.1;
    points << 0.15, 0.25, 0.35;
    ResonatorBank res(mass, freq, damp, points, 48000);
    unsigned int N = 48000;
    // N = 10;
    Mat x(Mat::Zero(N,2)), y(N,1);
    x(0,0) = 1;
    x(0,1) = 0.1;
    for (unsigned int k=0; k < x.rows(); k++) {
        y(k,0) = res.step(x.row(k));
    }
    return y;
}

class TargetFollower
{
public:
    float state;
    float k;
    float target;
    Mat trace;
    unsigned int trace_pos;

    TargetFollower(float target, float time_constant, float fs,
                   float initial_state=0, unsigned int trace_len=256)
    {
        this->state = initial_state;
        this->k = 1.0f-expf(-(1/fs)/time_constant); // time to drop 36.8%
        this->target = target;
        this->trace = Mat::Zero(trace_len,1).array() + initial_state;
        this->trace_pos = 0;
    };

    void set_target(float _target)
    {
        this->target = _target;
    }

    Mat get_trace()
    {
        Mat result(this->trace.rows(),1);
        unsigned int l = this->trace_pos;
        unsigned int r = this->trace.rows()-this->trace_pos;
        result.block(0,0,r,1) = this->trace.block(l, 0, r, 1);
        result.block(r,0,l,1) = this->trace.block(r, 0, l, 1);
        return result;
    }

    float get_last()
    {
        return this->trace((this->trace_pos-1) % this->trace.rows(), 0);
    }

    float update()
    {
        this->trace(this->trace_pos,0) = this->state;
        this->trace_pos = (this->trace_pos+1) % this->trace.rows();
        float delta = (this->target - this->state)*this->k;
        this->state = this->state + delta;
        return this->state;
    }

    float sample()
    {
        return this->state;
    }
};

class Bowing
{
public:
    float fundamental_freq;
    unsigned int NFREQS;
    Mat freqs;
    Mat mass;
    Mat damping;
    Mat points;
    bool distributed_modal_forces=false;
    bool predefined_distribution=false;
    bool veldiff_only=true;
    float bow_resonator_veldiff;
    TargetFollower bow_pressure;
    unsigned int NWINDOW;
    unsigned int hop;
    Mat buffer;
    unsigned int bufferpos = 0;
    float last_sample = 0;
    Mat action_space;
    Mat observation_space;
    float f0, fs;
    float external_bow_force;
    Mat external_resonator_force;
    float loading_bow_force;
    TargetFollower target_bow_velocity;
    InertialMass bow;
    float velocity_coupling;
    ResonatorBank res;
    float veldiff_sign;
    Mat step_trace;
    Mat action_trace_clean;
    Mat action_trace_noisy;

    Bowing(float _fundamental_freq, unsigned int _window_width=2048,
           unsigned int _hop_width=64, float _sample_rate=48000,
           unsigned int _num_modes=1, bool _distributed_modal_forces=false,
           bool _predefined_distribution=false, bool _veldiff_only=true)
        : bow_pressure(0.2, _window_width/_sample_rate/2, _sample_rate, 0.0)
        , target_bow_velocity(0.2, _window_width/_sample_rate/2, _sample_rate, 0.0)
        , bow(300.0, 0.01, _sample_rate)
        , res(Mat::Zero(1,1), Mat::Zero(1,1), Mat::Zero(1,1), Mat::Zero(1,1), _sample_rate)
    {
        this->f0 = _fundamental_freq;
        if (_num_modes == 0)
            this->NFREQS = (int)(_sample_rate/2*0.9);
        else
            this->NFREQS = _num_modes;

        this->freqs = Mat(1,this->NFREQS);
        this->mass = Mat(1,this->NFREQS);
        this->damping = Mat(1,this->NFREQS);
        for (unsigned int i=1; i <= this->NFREQS; i++)
        {
            this->freqs(i-1,0) = this->f0 * i;
            this->mass(i-1,0) = 1.0;
            this->damping(i-1,0) = 0.12;
        }
        this->points = Mat(1,2); points << 0.1, 0.75;

        this->distributed_modal_forces = _distributed_modal_forces;
        this->predefined_distribution = _predefined_distribution;
        this->veldiff_only = _veldiff_only;
        if (_predefined_distribution)
        {
            this->action_space = Mat(2,1);
            // force amplitude
            this->action_space(0,0) = -1;
            this->action_space(1,0) = 1;
        }
        else if (_distributed_modal_forces)
        {
            this->action_space = Mat(2,this->NFREQS+1);
            // force amplitude
            this->action_space(0,0) = -1;
            this->action_space(1,0) = 1;
            // distribution
            for (unsigned int i=0; i < this->NFREQS; i++)
            {
                this->action_space(0,i+1) = 0;
                this->action_space(1,i+1) = 1;
            }
        }
        else
        {
            this->action_space = Mat(2,this->NFREQS);
            // force amplitude for each mode
            for (unsigned int i=0; i < this->NFREQS; i++)
            {
                this->action_space(0,i) = -1;
                this->action_space(1,i) = 1;
            }
        }

        if (_veldiff_only) {
            this->observation_space = Mat(2,1);
            this->observation_space(0,0) = -2;
            this->observation_space(1,0) = 2;
        }
        else
        {
            this->observation_space = Mat(2,2);
            // diffvel
            this->observation_space(0,0) = -2;
            this->observation_space(1,0) = 0;
            // pressure
            this->observation_space(0,1) = -2;
            this->observation_space(1,1) = 1;
        }

        //super().__init__(window_width, hop_width, sample_rate, initial_action);
        this->NWINDOW = _window_width;
        this->hop = _hop_width;
        this->fs = _sample_rate;
        this->buffer = Mat::Zero(this->NWINDOW,1);
        this->bufferpos = 0;
        this->reset();
    }

    void reset()
    {
        this->res = ResonatorBank(this->mass, this->freqs, this->damping,
                                  this->points, this->fs);

        // tune to haptic device parameters (~300g)
        this->bow = InertialMass(300.0, 0.01, this->fs);

        this->last_sample = 0;
        this->bufferpos = 0;

        // Target state of bow velocity, force is calculated by a
        // controller to match it; some small damping is built into
        // the inertial mass.
        this->target_bow_velocity = TargetFollower(0.2, this->NWINDOW/this->fs/2,
                                                   this->fs, 0.0);

        this->velocity_coupling = 6.0;
        this->bow_resonator_veldiff = 0.0;
        this->veldiff_sign = 1;

        // Bow pressure is a simple target follower, it goes where we
        // want with some given time constant.
        this->bow_pressure = TargetFollower(0.2, this->NWINDOW/this->fs/2,
                                            this->fs, 0.0);
    }

    typedef std::function<Mat(cMat)> bowing_policy;

    Mat perfect_policy(cMat observation)
    {
        // Typical bowed string velocity/pressure curve, linear within;
        // a small part near zero (stick) then drops off sharply (slip).;
        float act;
        Mat dist(this->NFREQS+1,1);
        float r = 0;
        for (unsigned int i=1; i<=this->NFREQS; i++)
        {
            dist(i,0) = 1.0/i;
            r += 1.0/i;
        }
        for (unsigned int i=1; i<=this->NFREQS; i++)
        {
            // Distribution of force across modes.  Sums to 1 so that
            // total force is not modified.
            dist(i,0) /= r;
        }
        r *= 0.06;
        float a = 1.3;
        float dv = observation(0,0);
        float p = observation(0,1);
        float c = 0.65 * p;
        float o = (tanh(c / a) + 1) * r;
        if (dv >= c)
            act = (tanh(-dv / a) * r + o);
        else if (dv <= -c)
            act = (tanh(-dv / a) * r - o);
        else
            act = dv / c * r;

        // Return a force amplitude and distribution for each mode.
        dist(0,0) = act;
        return dist;
    }

    void apply_action(cMat action)
    {
        // Scale by pressure to respect Coulomb's law and restore sign.
        float scale = this->bow_pressure.state; // * this->veldiff_sign

        if (this->predefined_distribution)
        {
            // We use the predefined 1/f relation from the "perfect
            // policy" for the distribution and only train overall
            // amplitude, this way we can train a single output value
            // for visualization purposes.
            Mat dist(1,this->NFREQS);
            int sum=0;
            for (unsigned int i=1; i<=this->NFREQS; i++)
                sum += i;
            for (unsigned int i=1; i<=this->NFREQS; i++)
            {
                dist(0,i-1) = i / (float)sum;
            }

            this->external_bow_force = -action(0,0) * scale;
            this->external_resonator_force = action(0,0) * scale * dist;
        }
        else if (this->distributed_modal_forces)
        {
            // Alternative
            this->external_bow_force = -action(0,0) * scale;
            this->external_resonator_force = action(0,0)
                * action.block(0,1,1,this->NFREQS) * scale;
        }
        else
        {
            // External force is taken directly as the output of the
            // policy.  Force on the bow is the first policy output
            // (negative of force on resonator).
            this->external_bow_force = -action.sum() * scale;

            // Force on the string is distributed over the rest of the
            // components, which must sum to 1, so we can multiply them by
            // the total force (first component) to get the distribution
            // over the modes.
            this->external_resonator_force = action * scale;
        }
    }

    void observe(Mat &obs)
    {
        // Action is based on current velocity difference
        obs(0,0) = this->bow_resonator_veldiff;
        if (!this->veldiff_only) {
            // Action is also based on pressure
            obs(0,1) = this->bow_pressure.state;
        }
    }

    float step_sample(cMat action)
    {
        this->apply_action(action);
        this->update();
        return this->last_sample;
    }

    void update()
    {
        // Calculate loading forces (bow velocity target)
        this->loading_bow_force = (float)((this->target_bow_velocity.state - this->bow.v)
                                          * this->velocity_coupling);

        // And overcome the mass's linear damping
        this->loading_bow_force += this->bow.damping*this->bow.v;

        // this->blf_trace.append(this->loading_bow_force);
        // this->baf_trace.append(this->external_bow_force);
        // this->rf_trace.append(this->external_resonator_force);

        // Step the independent systems
        this->bow.step(this->loading_bow_force + this->external_bow_force);
        this->res.step(this->external_resonator_force);

        // Output the resonator velocity
        this->output_sample(this->res.vPoints(0,1));

        // Update observations for next call to policy
        this->bow_pressure.update();
        this->target_bow_velocity.update();
        this->bow_resonator_veldiff = (this->bow.v - this->res.vPoints(0,0));
        this->veldiff_sign = this->bow_resonator_veldiff < 0 ? -1 : 1;

        // this->bv_trace.append(this->bow.v);
        // this->rv_trace.append(this->res.vPoints[1]);
        // this->vd_trace.append(this->bow_resonator_veldiff);
    }

    void output_sample(float sample)
    {
        this->buffer(this->bufferpos,0) = sample;
        this->bufferpos = (this->bufferpos+1) % this->NWINDOW;
        this->last_sample = sample;
    }

    Mat get_buffer()
    {
        Mat result(1,this->NWINDOW);
        unsigned int R = this->NWINDOW - this->bufferpos;
        unsigned int L = this->bufferpos;
        result.block(0,0,R,1) = this->buffer.block(L,0,R,1);
        result.block(R,0,L,1) = this->buffer.block(0,0,L,1);
        return result;
    }

    Mat actor_predict(Mat observation)
    {
        Mat obs = ((observation - this->observation_space.row(0)).cwiseQuotient(
            this->observation_space.row(1) - this->observation_space.row(0)) * 2).array() - 1;
        Mat act = run_weight_set(0, obs);
        act = (act.cwiseProduct(this->action_space.row(1) - this->action_space.row(0))
               + this->action_space.row(0));
        return act;
    }

    Mat actor_predict_noisy(Mat observation)
    {
        Mat obs = ((observation - this->observation_space.row(0)).cwiseQuotient(
            this->observation_space.row(1) - this->observation_space.row(0)) * 2).array() - 1;
        Mat act = run_weight_set(1, obs);
        act = (act.cwiseProduct(this->action_space.row(1) - this->action_space.row(0))
               + this->action_space.row(0));
        return act;
    }

    void inner_loop(bool use_noisy_predict=false,
                    const Mat *noise=nullptr, unsigned int steps=256,
                    std::ofstream* out_file=nullptr)
    {
        unsigned int a_dim = this->action_space.cols();
        unsigned int o_dim = this->observation_space.cols();

        // Observe
        Mat o(1,o_dim), o2(1,o_dim);
        this->observe(o);

        if (step_trace.rows() != steps || step_trace.cols() != 1) {
            step_trace = Mat::Zero(steps, a_dim + o_dim*2).array() - 1.0;
        }
        if (action_trace_clean.rows() != steps || action_trace_clean.cols() != 1)
            action_trace_clean = Mat(steps, a_dim);
        if (action_trace_noisy.rows() != steps || action_trace_noisy.cols() != 1)
            action_trace_noisy = Mat(steps, a_dim);

        static int l=0;

        for (unsigned int k=0; k < steps; k++)
        {
            // Choose an action
            Mat a = this->actor_predict(o);
            Mat a_clean = a;

            // Choose a noisy action if provided
            if (use_noisy_predict)
                a = this->actor_predict_noisy(o);

            // Add observation-independent noise if provided
            if (noise)
                a += (*noise).row(k);

            // Apply limits to action space
            for (unsigned int i=0; i < a.cols(); i++)
            {
                if (a(0,i) < this->action_space(0,i))
                    a(0,i) = this->action_space(0,i);
                if (a(0,i) > this->action_space(1,i))
                    a(0,i) = this->action_space(1,i);
            }

            this->action_trace_clean.row(k) = a_clean;
            this->action_trace_noisy.row(k) = a;

            // Apply action and calculate new sample
            this->step_sample(a);

            // Observe after;
            this->observe(o2);

            // Remember the observation-action-observation tuple;
            // these will be paired with the resulting spectrum and
            // used to train the critic.
            this->step_trace.block(k,0,1,o.cols()) = o;
            this->step_trace.block(k,o.cols(),1,a.cols()) = a;
            this->step_trace.block(k,o.cols()+a.cols(),1,o.cols()) = o2;

            // Update previous observation
            o = o2;

            // Record a wav file if we are doing that
            // if (out_file)
            // {
            //     print('% 20.11g % 20.11f'%(out_file_counter/this->fs, samp), file=out_file);
            //     out_file_counter += 1;
            // }
        }
    }
};

// TODO pass in optional policy
//Mat view_bowing_policy(Bowing::bowing_policy *policy=nullptr)
Mat view_bowing_policy()
{
    Bowing b(220.0, 2048, 64, 48000.0, 100, true, false, false);
    // auto perfect_policy = [&b](cMat obs){return b.perfect_policy(obs);};
    // if (!policy)
    //     policy = &perfect_policy;
    Mat result(1000, b.action_space.rows()+2);
    for (int i=0; i<1000; i++)
    {
        float dv = i/1000.0*20-10;
        float p = 0.2;
        Mat obs(1,2);
        obs << dv,p;
        result.block(i,0,1,2) << dv,p;
        //f = np.sum(policy(np.vstack([dv,p]).T), axis=1); // if outputting forces
        Mat f;
        // if (policy)
        //     f = (*policy)(obs); // if outputting force and dist
        // else
        f = b.perfect_policy(obs);
        result.block(i,2,1,b.action_space.rows()) = f.transpose();
    }
    return result;
}

Mat test_bowing()
{
    Bowing b(110.0, 2048, 64, 48000.0, 10, true, false, false);
    unsigned int steps = 48000;
    Mat obs(Mat::Zero(1,b.veldiff_only ? 1 : 2));
    Mat result(steps, 4);
    b.target_bow_velocity.set_target(0.2);
    b.bow_pressure.set_target(0.2);
    for (unsigned int k=0; k < steps; k++)
    {
        b.observe(obs);
        result(k,0) = obs(0,0); // dv
        result(k,1) = obs(0,1); // bp
        result(k,2) = b.step_sample(b.perfect_policy(obs)); // rv
        result(k,3) = b.bow.v; // bv
    // with open('out.dat','w') as out:;
    //     print('; Sample Rate 48000', file=out);
    //     print('; Channels 1', file=out);
    //     m = rv.max()*1.2;
    //     for i in range(steps):;
    //         print(' %0.08f %0.08g'%(i/48000.0, rv[i]/m), file=out);
    }
    return result;
}

/**
 * The actual bowing loop to be called
 */

Bowing g_bowing(220.0, 2048, 64, 48000.0, 20, false, true, true);

/// TODO pybind11 with optional pointer to Eigen matrix doesn't seem
/// to work, we'll use an empty matrix instead for now
Mat inner_loop(bool use_noisy_predict, cMat noise, unsigned int steps)
{
    if (noise.rows() > 0 && noise.rows() != steps)
    {
        printf("Got %ld rows of noise but there are %u steps.\n",
               noise.rows(), steps);
        abort();
    }

    g_bowing.inner_loop(use_noisy_predict,
                        noise.rows() == 0 ? nullptr : &noise,
                        steps);

    return g_bowing.step_trace;
}

Mat get_buffer()
{
    return g_bowing.get_buffer();
}


/**
 * Python module
 */
PYBIND11_PLUGIN(bowing_loop) {
    py::module m("bowing_loop", "Run the bowing algorithm");

    m.def("set_layer_weights", &set_layer_weights, "set layer weights");
    m.def("run_weight_set", &run_weight_set, "Run a set of weights and return output.");
    m.def("test_resonator_bank", &test_resonator_bank, py::return_value_policy::move,
          "Test the resonator bank.");
    m.def("view_bowing_policy", &view_bowing_policy, py::return_value_policy::move,
          "Test the bowing policy.");
    m.def("test_bowing", &test_bowing, "Test the bowing loop.");
    m.def("get_buffer", &get_buffer, py::return_value_policy::move,
          "Get last buffer of output samples from bowing.");
    m.def("inner_loop", &inner_loop,
          py::return_value_policy::move,
          "Bowing inner loop.");

    return m.ptr();
}
