""" 
Data structure for implementing experience replay

Author: Patrick Emami
"""
from collections import deque
import random
import numpy as np
import h5py

class ReplayBuffer(object):

    def __init__(self, buffer_size, random_seed=123):
        """
        The right side of the deque contains the most recent experiences 
        """
        self.buffer_size = buffer_size
        self.count = 0
        self.buffer = deque()
        random.seed(random_seed)

    def add(self, s, a, r, t, s2):
        experience = (s, a, r, t, s2)
        if self.count < self.buffer_size: 
            self.buffer.append(experience)
            self.count += 1
        else:
            self.buffer.popleft()
            self.buffer.append(experience)

    def add_spectrum(self, s2):
        return s2

    def add_action_params(self, actpar, s2, r):
        pass

    def size(self):
        return self.count

    def sample_batch(self, batch_size):
        batch = []

        if self.count < batch_size:
            batch = random.sample(self.buffer, self.count)
        else:
            batch = random.sample(self.buffer, batch_size)

        s_batch = np.array([_[0] for _ in batch])
        a_batch = np.array([_[1] for _ in batch])
        r_batch = np.array([_[2] for _ in batch])
        t_batch = np.array([_[3] for _ in batch])
        s2_batch = np.array([_[4] for _ in batch])

        return s_batch, a_batch, r_batch, t_batch, s2_batch


    def clear(self):
        self.buffer.clear()
        self.count = 0


class ReplayBufferHDF5(object):

    def __init__(self, buffer_size, random_seed=123, filename='replay_buffer.hdf5'):
        """
        The right side of the deque contains the most recent experiences 
        """
        self.buffer_size = buffer_size
        random.seed(random_seed)

        self.file = h5py.File(filename, 'a')
        try:
            self.buffer = self.file['buffer']
            self.o_dim = self.buffer.attrs['o_dim']
            self.a_dim = self.buffer.attrs['a_dim']
            self.r_dim = self.buffer.attrs['r_dim']
        except KeyError:
            self.buffer = None # will create when we know the size

        try:
            self.spectra = self.file['spectra']
        except KeyError:
            self.spectra = None # will create when we know the size

        try:
            self.actpars = self.file['actor_params']
        except KeyError:
            self.actpars = None # will create when we know the size

    def add_spectrum(self, s2):
        self.spectra_size = s2.shape[0]
        if self.spectra is None:
            self.spectra = self.file.create_dataset('spectra', shape=(1,self.spectra_size),
                                                    maxshape=(None,self.spectra_size),
                                                    chunks=True, dtype=np.float64)
            self.spectra.attrs['pos'] = 0
        i = self.spectra.attrs['pos']
        if i >= self.spectra.shape[0]:
            self.spectra.resize((self.spectra.shape[0]+1,) + self.spectra.shape[1:])
        self.spectra[i,:] = s2
        self.spectra.attrs['pos'] += 1
        return i

    def add(self, o, a, si, r, o2):
        experience = (o, a, si, r, o2)
        assert si==-1 or si < self.spectra.shape[0]

        if self.buffer is None:
            cols = len(o)*2+len(a)+1+len(r)
            self.buffer = self.file.create_dataset('buffer', shape=(1,cols),
                                                   maxshape=(None,cols),
                                                   chunks=True, dtype=np.float64)
            self.o_dim = self.buffer.attrs['o_dim'] = len(o)
            self.a_dim = self.buffer.attrs['a_dim'] = len(a)
            self.r_dim = self.buffer.attrs['r_dim'] = len(r)
            self.buffer.attrs['pos'] = 0

        i = self.buffer.attrs['pos']
        if i == self.buffer.shape[0]:
            self.buffer.resize((self.buffer.shape[0]+1,)+self.buffer.shape[1:])

        j = 0
        self.buffer[i,j:j+self.o_dim] = o;    j += self.o_dim
        self.buffer[i,j:j+self.a_dim] = a;    j += self.a_dim
        self.buffer[i,j:j+1] = si;            j += 1
        self.buffer[i,j:j+self.r_dim] = r;    j += self.r_dim
        self.buffer[i,j:j+self.o_dim] = o2;   j += self.o_dim
        self.buffer.attrs['pos'] += 1

    def add_actor_params(self, actpar, si, r, penalty):
        self.actpars_size = actpar.shape[0] + 3
        assert si==-1 or si < self.spectra.shape[0]

        if self.actpars is None:
            self.actpars = self.file.create_dataset('actor_params', shape=(1,self.actpars_size),
                                                    maxshape=(None,self.actpars_size),
                                                    chunks=True, dtype=np.float64)
            self.actpars.attrs['pos'] = 0
            self.p_dim = self.actpars.attrs['p_dim'] = len(actpar)
        i = self.actpars.attrs['pos']
        if i >= self.actpars.shape[0]:
            self.actpars.resize((self.actpars.shape[0]+1,) + self.actpars.shape[1:])
        self.actpars[i,:self.p_dim] = actpar
        self.actpars[i,self.p_dim+0] = si
        self.actpars[i,self.p_dim+1] = r
        self.actpars[i,self.p_dim+2] = penalty
        self.actpars.attrs['pos'] += 1
        return i

    def size(self):
        if self.buffer is None:
            return 0
        assert self.buffer.attrs['pos'] <= self.buffer.shape[0]
        return self.buffer.attrs['pos']

    def actor_params_size(self):
        if self.actpars is None:
            return 0
        assert self.actpars.attrs['pos'] <= self.actpars.shape[0]
        return self.actpars.attrs['pos']

    def sample_batch(self, batch_size):
        ixs = np.random.randint(0, self.buffer.attrs['pos'], batch_size)
        batch = [self.buffer[i,:] for i in ixs]

        o_batch = np.array([_[:self.o_dim] for _ in batch])
        a_batch = np.array([_[self.o_dim:self.o_dim+self.a_dim] for _ in batch])
        si_batch = np.array([_[self.o_dim+self.a_dim] for _ in batch]).astype(int)
        r_batch = np.array([_[self.o_dim+self.a_dim+1:self.o_dim+self.a_dim+1+self.r_dim]
                            for _ in batch])
        o2_batch = np.array([_[self.o_dim+self.a_dim+1+self.r_dim:] for _ in batch])

        s_batch = [(None if i < 0 else self.spectra[i,:]) for i in si_batch]

        return o_batch, a_batch, s_batch, r_batch, o2_batch

    def sample_batch_actor_params(self, batch_size):
        ixs = np.random.randint(0, self.actpars.attrs['pos'], batch_size)
        batch = [self.actpars[i,:] for i in ixs]

        p_batch = np.array([_[:self.p_dim] for _ in batch])
        si_batch = np.array([_[self.p_dim] for _ in batch]).astype(int)
        r_batch = np.array([_[self.p_dim+1:self.p_dim+2]
                            for _ in batch])
        penalty_batch = np.array([_[self.p_dim+2:] for _ in batch])

        s_batch = [(None if i < 0 else self.spectra[i,:]) for i in si_batch]

        return p_batch, s_batch, r_batch, penalty_batch


    def clear(self):
        if self.buffer is not None: self.buffer.attrs['pos'] = 0
        if self.spectra is not None: self.spectra.attrs['pos'] = 0

def test_replay_buffer(RB):
    rb = RB(10)

    rb.clear()
    assert rb.size() == 0

    entries = (np.random.uniform(-1,1,(10,2)),
               np.random.uniform(-1,1,(10,1)),
               np.random.uniform(-40,40,(10,256)),
               np.random.uniform(-5,5,(10,1)),
               np.random.uniform(-1,1,(10,2)))

    for o, a, s, r, o2 in zip(*entries):
        si = rb.add_spectrum(s)
        rb.add(o, a, si, r, o2)

    assert rb.size() == 10

    # rb.add(1,1,si,1,1) # ignore circular effect for now
    # assert len(rb.buffer)==10

    for i in range(100):
        z = rb.sample_batch(3)
        for col in range(len(entries)):
            assert z[col].shape[0] == 3
            assert z[col].shape[1:] == entries[col].shape[1:]
            for y in z[col]:
                assert y in entries[col]
    print('No errors.')
    return rb

if __name__=='__main__':
    test_replay_buffer(ReplayBuffer)
    test_replay_buffer(ReplayBufferHDF5)
