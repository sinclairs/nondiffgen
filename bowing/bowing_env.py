#!/usr/bin/env python3

import matplotlib
matplotlib.use('Agg')

import gym.spaces
import numpy as np
from scipy.signal import lfilter
from matplotlib import pyplot as plt
import persample_env
from persample_env import OnlineSignalEnvironment, TargetFollower

import os, importlib
importlib.reload(persample_env)

# Things to do
# * option for enforcing symmetry
# * move options to command-line options for parallel testing
# * implement wasserstein distance metric
# * make modal control only a function of pressure, non-linear curve
#   only a function of velocity difference

"""Implement a bowed string using a resonator bank and a non-linearity
In the style of CORDIS-ANIMA (ACROE)."""

class ResonatorBank(object):
    def __init__(self, mass, frequency, damping_ratio, points, sampling_freq):
        self.mass = np.array(mass, dtype=float)
        self.frequency = np.array(frequency, dtype=float)
        self.damping_ratio = np.array(damping_ratio, dtype=float)
        self.points = np.array([points]).T
        self.fs = sampling_freq
        self.NFREQS = np.shape(self.frequency)[0]
        self.tuning()
        self.reset()

    def reset(self):
        self.x = np.zeros_like(self.frequency)
        self.v = np.zeros_like(self.frequency)

    def tuning(self):
        self.theta = 2*np.pi*self.frequency/self.fs
        self.damping = 2*self.damping_ratio*self.theta

        # coupling point coefficients
        i = np.arange(1,self.NFREQS+1)
        self.P = np.sin(self.points * [i*np.pi])

        self.tuning_filter()
        #self.tuning_spring_mass()

    def tuning_filter(self):
        # Filter formulation of resonator
        self.R = pow(0.5, self.damping/20)
        self.a1 = -2*self.R*np.cos(self.theta)
        self.a2 = self.R**2

        # State variables
        self.y2 = self.y1 = self.y0 = 0

    def tuning_spring_mass(self):
        # Alternative formulation based on explicit integration of a
        # spring-mass system.  It is easier to understand but is less
        # stable than the filter formulation in higher modes.

        # Determine stiffnesses that give desired frequency for each mass.
        self.stiffness = self.mass*(self.theta**2)

        # coupling points coefficients
        i = np.arange(1,self.NFREQS+1)
        self.P = np.sin(self.points * [i*np.pi])

        # State variables
        self.x = self.v = 0

    def step(self, external_force=0):
        # Calculate force at each point
        # (zero force on all but first point)
        if hasattr(external_force, 'shape'):
            # Force is a distribution
            extF = np.vstack([external_force, np.zeros_like(self.P)[1:,:]])
        else:
            # Force is uniform
            extF = np.vstack([[external_force], np.zeros((self.P.shape[0]-1,1))])
        self.F = np.sum(extF*self.P, axis=0)
        return self.step_filter()
        #return self.step_spring_mass()

    def step_spring_mass(self):
        # Spring-mass formulation
        # (Just differs by damping)
        f = self.F - self.stiffness*self.x - self.damping*self.v
        self.v += f / self.mass
        self.x += self.v
        self.vPoints = np.sum(self.P * self.v, axis=1)
        return self.vPoints[0]

    def step_filter(self):
        # Acceleration
        self.A = self.F / self.mass

        # Update filter state
        self.y2 = self.y1
        self.y1 = self.y0
        self.y0 = self.A - self.a1*self.y1 - self.a2*self.y2

        # Backward difference velocity
        self.vPoints = np.sum(self.P * self.y0, axis=1) - np.sum(self.P * self.y1, axis=1)

        return self.vPoints[0]

def test_resonator_bank():
    from matplotlib import pyplot as plt
    res = ResonatorBank([1,1], [100,10], 0.1, [0.15,0.25,0.35], 48000)
    N = 48000
    x, y = np.zeros((N,2)), np.zeros(N)
    x[0,:] = 1.0,0.1
    #x[x.shape[0]//2] = 1
    for k in range(x.shape[0]):
        y[k] = res.step(x[k])
    plt.plot(x)
    plt.plot(y)
    plt.show()

class InertialMass(object):
    def __init__(self, mass, damping, sampling_freq):
        self.mass = float(mass)
        self.damping = float(damping)
        self.fs = sampling_freq
        self.reset()

    def reset(self, x=0.0, v=0.0):
        self.x = x
        self.v = v

    def step(self, external_force=0):
        f = - self.damping*self.v + external_force
        self.v += f/self.mass
        self.x += self.v
        return self.x

class Bowing(OnlineSignalEnvironment):
    """AI Gym-like environment; policy must determine the output of the
    signal taking phase as input, i.e. determine how to transform
    phase by cos(phase)."""
    def __init__(self, fundamental_freq, window_width=2048, hop_width=512,
                 sample_rate=48000, initial_action=None, num_modes=None,
                 distributed_modal_forces=False, predefined_distribution=False,
                 veldiff_only=True, store_width=2048):
        self.store_width = store_width
        self.f0 = fundamental_freq
        if num_modes is None:
            self.NFREQS = int((sample_rate/2*0.9)//self.f0)
        else:
            self.NFREQS = int(num_modes)

        self.distributed_modal_forces = distributed_modal_forces
        self.predefined_distribution = predefined_distribution
        self.veldiff_only = veldiff_only
        if predefined_distribution:
            L = np.array([-1])
            H = np.array([1])
        elif distributed_modal_forces:
            L = np.array([-1]+[0]*self.NFREQS) # force amplitude (positive), distribution
            H = np.array([1]+[1]*self.NFREQS)
        else:
            L = np.array([-1]*self.NFREQS) # force amplitude (positive), distribution
            H = np.array([1]*self.NFREQS)
        self.action_space = gym.spaces.Box(L, H, dtype=np.float32) 

        if veldiff_only:
            self.observation_space = gym.spaces.Box( np.array([-2]), #+self.NFREQS*2*[-1]+[-1,-1]),
                                                     np.array([2]), #+self.NFREQS*2*[1]+[1,1]),
                                                     dtype=np.float32 ) # diffvel only
        else:
            self.observation_space = gym.spaces.Box( np.array([-2, 0]), #+[-1]*100), #+self.NFREQS*2*[-1]+[-1,-1]),
                                                     np.array([2, 1.0]), #+[1]*100), #+self.NFREQS*2*[1]+[1,1]),
                                                     dtype=np.float32 ) # diffvel, pressure
        super().__init__(window_width, hop_width, sample_rate, initial_action)
        self.NFFT = 128 # cut spectrum short
        self.frame_counter = 0

        # Load pre-calculated target for the selected number of modes.
        # (This is what we are trying to learn to simulate by the policy.)
        fntime, fnspec = ['bowing_%02d_modes_%s.txt'%(self.NFREQS,s) for s in ('time','spec')]
        self.w_target = np.loadtxt(fntime, delimiter=',')
        self.y_target = np.loadtxt(fnspec, delimiter=',')
        # self.w_target /= 10**(50/20)  # had diminished the spectrum for the constrained
        # self.y_target -= 50           # condition, but no longer necessary.
        print('Loaded target',fntime,'and',fnspec)

    def reset(self, initial_action=None):
        self.res = ResonatorBank(mass = 1.0,
                                 frequency = (np.arange(self.NFREQS)+1) * self.f0,
                                 damping_ratio = 0.12,
                                 points = [0.1, 0.75],  # bow, sound coupling points
                                 sampling_freq = self.fs)

        # tune to haptic device parameters (~300g)
        self.bow = InertialMass(mass = 300.0, damping = 0.01, sampling_freq = self.fs)

        # Target state of bow velocity, force is calculated by a
        # controller to match it; some small damping is built into the
        # inertial mass.
        self.target_bow_velocity = TargetFollower(0.2, self.NWINDOW/2/self.fs,
                                                  self.fs, 0.0, self.NWINDOW)
        self.velocity_coupling = 6.0
        self.bow_resonator_veldiff = 0
        self.veldiff_sign = 1

        # Bow pressure is a simple target follower, it goes where we
        # want with some given time constant.
        self.bow_pressure = TargetFollower(0.2, self.NWINDOW/2/self.fs,
                                           self.fs, 0.0, self.NWINDOW)

        self.bv_trace = []
        self.rv_trace = []
        self.vd_trace = []

        self.blf_trace = []  # loading force
        self.baf_trace = []  # action force
        self.rf_trace = []   # resonator force

        if initial_action is not None:
            self.initial_action = initial_action
            # Do nothing with initial action

        return super().reset()

    def perfect_policy(self, observations):
        # Typical bowed string velocity/pressure curve, linear within
        # a small part near zero (stick) then drops off sharply (slip).
        act = np.zeros((np.shape(observations)[0],1))
        i = 1.0 / np.arange(1,self.NFREQS+1)
        r = 0.06 * np.sum(i)
        a = 1.3
        for k,obs in enumerate(observations):
            dv, p = obs
            c = 0.65 * p
            o = (np.tanh(c / a) + 1) * r
            if dv > -c and dv < c:
                act[k] = dv / c * r
            elif dv >= c:
                act[k] = (np.tanh(-dv / a) * r + o)
            elif dv <= -c:
                act[k] = (np.tanh(-dv / a) * r - o)

        # Distribution of force across modes.  Sums to 1 so that total
        # force is not modified.
        dist = [i / np.sum(i)]*np.shape(observations)[0]

        # Return a force value for each mode.
        return np.hstack([act, dist])

    def apply_action(self, action):
        # Scale by pressure to respect Coulomb's law and restore sign.
        scale = self.bow_pressure.state # * self.veldiff_sign

        if np.isnan(action).any():
            action = np.zeros(np.shape(action))

        if self.predefined_distribution:
            # We use the predefined 1/f relation from the "perfect
            # policy" for the distribution and only train overall
            # amplitude, this way we can train a single output value
            # for visualization purposes.
            i = 1.0 / np.arange(1,self.NFREQS+1)
            dist = i / np.sum(i)

            self.external_bow_force = -action[0] * scale
            self.external_resonator_force = action[0] * scale * dist

        elif self.distributed_modal_forces:
            # Alternative
            self.external_bow_force = -action[0] * scale
            self.external_resonator_force = action[0]*action[1:] * scale

        else:
            # External force is taken directly as the output of the
            # policy.  Force on the bow is the first policy output
            # (negative of force on resonator).
            self.external_bow_force = -np.sum(action) * scale # / self.NFREQS

            # Force on the string is distributed over the rest of the
            # components, which must sum to 1, so we can multiply them by
            # the total force (first component) to get the distribution
            # over the modes.
            self.external_resonator_force = action * scale #/ self.NFREQS

    def observe(self):
        if self.veldiff_only:
            # Action is based on current velocity difference only
            obs = np.abs(self.bow_resonator_veldiff,)
        else:
            # Action is based on current velocity difference and bow
            # pressure.
            obs = np.abs(self.bow_resonator_veldiff), self.bow_pressure.state

        # obs += tuple(self.res.x) + tuple(self.res.v) + (self.bow.x, self.bow.v)
        # obs += tuple(self.baf_trace[-100:])

        return obs

        # wip Absolute veldiff here? we restore sign in apply_action()
        # to ensure a symmetric policy function.

    def log_spectrum(self, sig=None):
        if sig is None:
            W = self.get_buffer()
            self.W = W
        else:
            W = sig
        W = lfilter([1, -0.999], [0.999,-0.999], # block DC
                    x=np.hstack([np.zeros_like(W),W*self.w]))[-W.shape[0]:]
        y = 20*np.log10(np.abs(np.fft.fft(W)[:self.NFFT])+1e-14)
        return y

    def update(self):
        # Calculate loading forces (bow velocity target)
        self.loading_bow_force = float((self.target_bow_velocity.state - self.bow.v)
                                       * self.velocity_coupling)

        # And overcome the mass's linear damping
        self.loading_bow_force += self.bow.damping*self.bow.v

        self.blf_trace.append(self.loading_bow_force)

        # for energy conservation.. maybe it makes sense to not allow
        # arbitrary forces here but instead make it a scaling of the
        # bow internal force.. can't apply more force than is being
        # supplied by the user, who is dragging the bow
        # self.external_bow_force = self.external_bow_force * self.loading_bow_force
        # self.external_resonator_force = self.external_resonator_force * self.loading_bow_force
        # Don't think so.. this is too conservative, doesn't allow
        # resonator energy to contribute to friction forces properly.

        self.baf_trace.append(self.external_bow_force)
        self.rf_trace.append(self.external_resonator_force)

        # Step the independent systems
        self.bow.step(self.loading_bow_force + self.external_bow_force)
        self.res.step(self.external_resonator_force)

        # Output the resonator velocity
        self.output_sample(self.res.vPoints[1])

        # Update observations for next call to policty
        self.bow_pressure.update()
        self.target_bow_velocity.update()
        self.bow_resonator_veldiff = (self.bow.v - self.res.vPoints[0])
        self.veldiff_sign = np.sign(self.bow_resonator_veldiff)

        self.bv_trace.append(self.bow.v)
        self.rv_trace.append(self.res.vPoints[1])
        self.vd_trace.append(self.bow_resonator_veldiff)

    def inner_loop(self, actor, use_noisy_predict=False, noise=None,
                   steps=256, fade=128, out_file=None, perfect=None):

        # Observe
        # if noise is None:
        #     o = self.observe()
        # else:
        #     o = self.observe(float(noise[0,0]))
        o = self.observe()

        # Check for bad observation values
        if np.isinf(o).any() or np.isnan(o).any():
            self.reset()
            o = self.observe()

        step_trace = []
        action_trace_clean = []
        action_trace_noisy = []

        for k in range(steps):  # need at least one window

            # Choose an action
            a = actor.predict([o[:2]])[0]
            if perfect is not None:
                aperf = self.perfect_policy([[o[0],o[1]]])[0]
                if perfect == 'amp' or self.predefined_distribution:
                    a[0] = aperf[0]
                elif perfect == 'both':
                    a = aperf
                elif perfect == 'dist':
                    a[1:] = aperf[1:]
            a_clean = a.copy()

            # Choose a noisy action if provided
            if use_noisy_predict:
                a = actor.predict_noisy([o[:2]])[0]

            # Choose an action from the previous policy and cross-fading it
            crossfade = True
            if crossfade:
                a_prev = actor.predict_prev([o[:2]])[0]
                if perfect is not None:
                    aperf = self.perfect_policy([[o[0],o[1]]])[0]
                    if perfect == 'amp' or self.predefined_distribution:
                        a_prev[0] = aperf[0]
                    elif perfect == 'both':
                        a_prev = aperf
                    elif perfect == 'dist':
                        a_prev[1:] = aperf[1:]
                i = np.min([k/fade,1.0])
                a = a_prev*((np.cos(i*np.pi)+1)/2) + a*((np.cos(np.pi-i*np.pi)+1)/2)

            # Add observation-independent noise if provided
            if noise is not None:
                a += noise[k]

            # Apply limits to action space
            # a = (a * np.logical_and(a>=self.action_space.low, a<=self.action_space.high)
            #      + self.action_space.high*(a>self.action_space.high)
            #      + self.action_space.low*(a<self.action_space.low))

            action_trace_clean.append(a_clean)
            action_trace_noisy.append(a)

            # Apply action and calculate new sample
            samp = self.step_sample(a)

            # Observe after
            # if noise is None:
            #     o2 = self.observe()
            # else:
            #     o2 = self.observe(float(noise[k,0]))
            o2 = self.observe()

            # Check for bad observation values
            if np.isinf(o2).any() or np.isnan(o2).any():
                self.reset()
                continue

            # Remember the observation-action-observation tuple;
            # these will be paired with the resulting spectrum and
            # used to train the critic.
            step_trace.append((o, a, o2))

            # Update previous observation
            o = o2

            # Record a wav file if we are doing that
            if out_file is not None:
                print('% 20.11g % 20.11f'%(out_file_counter/self.fs, samp), file=out_file)
                out_file_counter += 1

        # Save the policy for cross-fading next time
        actor.update_prev_network(use_noisy_predict)

        return step_trace, action_trace_clean, action_trace_noisy

    def render(self, args, episode, step, env, dist, actor, critic,
               spectrum, reward, penalty, traces, batch,
               histogram, replay_buffer_size, use_noisy_predict):
        step_trace, reward_trace, action_trace_noisy, action_trace_clean, dist_trace = traces
        o_batch, a_batch, o2_batch, r_batch, s2_batch, y_i, predicted_q_value = batch

        fig = plt.figure(1, figsize=(12,8))
        fig.clear()
        ax = fig.add_subplot(4,3,1)
        f = np.linspace(0, self.NFFT/self.NWINDOW*self.fs, spectrum.shape[0])
        ax.plot(f, self.y_target)
        ax.plot(f, spectrum, label='r=%0.02f (%0.02f)' % (-dist.reconstruction_loss([spectrum]), reward))
        # # plot Itakura-Saito ratio
        # r = (10**(self.y_target/20))/((10**spectrum/20) + 1e-14)
        # r = -np.log(1/(2*np.pi)*(r-np.log(r)-1)*dist.weight + 1e-14) - 100
        # ax.plot(f, r)

        ax.legend()
        ax = fig.add_subplot(4,3,2)
        t = np.arange(np.shape(self.w_target)[0]) / self.fs
        xw = xW = 0
        for i in range(1,np.shape(self.w_target)[0]):
            if self.w_target[i] > 0 and self.w_target[i-1] <= 0:
                xw = i / self.fs
                break
        for i in range(1,np.shape(self.W)[0]):
            if self.W[i] > 0 and self.W[i-1] <= 0:
                xW = i / self.fs
                break
        ax.plot(t, self.w_target)
        string_magnitude = np.mean(self.W**2)
        ax.plot(t-xW+xw, self.W, label='%0.02f'%string_magnitude)
        ax.set_xlim(0, np.shape(self.W)[0] / self.fs)
        ax.legend()
        ax = fig.add_subplot(4,3,3)
        pw = lambda x: '%0.02g'%(np.mean((x - np.mean(x))**2))
        ax.plot(self.bv_trace[-self.NWINDOW:], label=pw(self.bv_trace[-self.NWINDOW:]))
        ax.text(ax.get_xlim()[1], ax.get_ylim()[1], 'bow vel',
                verticalalignment='top', horizontalalignment='right')
        ax.legend()
        ax = fig.add_subplot(4,3,6)
        ax.plot(self.vd_trace[-self.NWINDOW:], label=pw(self.vd_trace[-self.NWINDOW:]))
        ax.text(ax.get_xlim()[1], ax.get_ylim()[1], 'vel diff',
                verticalalignment='top', horizontalalignment='right')
        ax.legend()
        ax = fig.add_subplot(4,3,9)
        ax.plot(self.baf_trace[-self.NWINDOW:], label=pw(self.baf_trace[-self.NWINDOW:])
                + ' (penalty %0.02f)'%penalty)
        ax.text(ax.get_xlim()[1], ax.get_ylim()[1], 'bow action force',
        # \nnoise weight = %0.05f'
        #         % (actor.get_noise_weight()),
                verticalalignment='top', horizontalalignment='right')
        ax.legend()
        ax = fig.add_subplot(4,3,12)
        ax.plot(self.rf_trace[-self.NWINDOW:])
        ax.text(ax.get_xlim()[1], ax.get_ylim()[1], 'modal force',
                verticalalignment='top', horizontalalignment='right')

        ax = fig.add_subplot(4,3,4)
        if self.distributed_modal_forces and len(step_trace)>0:
            # visualize action
            # actions = [t[1] for t in step_trace]
            # visualize just the last action for now, power of each mode
            action_mean = np.mean([s[1] for s in step_trace], axis=0)
            action_std = np.std([s[1] for s in step_trace], axis=0)
            ax.bar(x=np.arange(action_mean.shape[0]-1), height=action_mean[1:], yerr=action_std[1:])
            ax.errorbar(x=np.arange(action_mean.shape[0]-1), y=action_mean[1:], yerr=action_std[1:],
                        fmt='none', ecolor='k', capsize=10)

        else:
            # visualize batch values r and y_i
            if False:
                ax.plot(r_batch)
                ax.plot(y_i)
            else:
                ax.scatter(o_batch[:,0], a_batch[:,0], marker='.',
                           c=np.array(y_i).reshape((-1,)))
                # ax.set_xlim(0.18,0.22)
                # ax.set_ylim(-0.07,0.3)

        ax = fig.add_subplot(4,3,5)
        ax.plot(action_trace_noisy[0], 'r', alpha=0.5,
                )#label='noisy=%0.01f'%long_trace_noisy[0][-1])
        ax.plot(action_trace_clean[0], 'b', alpha=0.5,
                label='%0.01f'%action_trace_clean[0][-1])
        ax.legend(loc=2)

        ax = fig.add_subplot(4,3,7)
        plt.plot(np.array(reward_trace))
        plt.plot(dist_trace[0], label='real')
        plt.plot(dist_trace[1], label='fake')
        #plt.legend()
        ax = fig.add_subplot(4,3,8)
        # TODO: how to visualize reconstruction error space as function of action
        # ax.plot(self.eval_fr, -dist.reconstruction_loss(self.eval_spec), alpha=0.4)
        # ax.plot(self.eval_fr, critic.predict(np.random.uniform(0,1,(self.eval_fr.shape[0],1)),
        #                                      self.eval_fr.reshape((-1,1))), alpha=0.4)
        # ax.plot(a_batch, np.array(y_i), 'yx', label='y_i')
        # ax.plot(a_batch, predicted_q_value, 'rx', label='predicted_q')

        # Single pressure value, one line for each mode action
        dv = np.linspace(-1,1,100)
        #for p in [0.01, 0.02, 0.05, 0.1, 0.2]:
        for p in [0.2]:
            pr = np.ones((100,1))*p
            sign = np.vstack([-np.ones((50,1)),np.ones((50,1))])
            nz = np.random.normal(0,0.1,(100,1)) * 2 #-1 to 1

            if self.veldiff_only:
                act = actor.predict(np.hstack([np.reshape(dv,(-1,1))])) # * pr # * sign
            else:
                act = actor.predict(np.hstack([np.vstack([dv,pr.T]).T])) # * pr # * sign

            nact = None
            if self.veldiff_only and use_noisy_predict:
                nact = actor.predict_noisy(np.hstack([np.reshape(dv,(-1,1))]))
            elif use_noisy_predict:
                nact = actor.predict_noisy(np.hstack([np.vstack([dv,pr.T]).T]))

            if not (self.distributed_modal_forces or self.predefined_distribution):
                [ax.plot(dv, act[:,i+1], 'bgrym'[i%5], alpha=p/0.2)
                 for i in range(act.shape[1]-1)]
            act_perf = self.perfect_policy(np.vstack([dv,pr.T]).T)
            ax.plot(dv, act_perf[:,0], 'r--', alpha=0.2)
            if use_noisy_predict:
                ax.plot(dv, nact[:,0], 'm', alpha=0.2)
            ax.plot(dv, act[:,0], 'k--')

        # Not so interesting since there is no hysteresis, it doesn't form a loop.
        # ax = fig.add_subplot(4,3,10) # dv / force curve
        # ax.plot([s[0][0] for s in step_trace], [s[1].sum() for s in step_trace], 'k', alpha=0.3)

        # Plot estimated reward as dv/f pair (keeping p constant)
        ax = fig.add_subplot(4,3,10)
        if not hasattr(self,'dvfgrid_o'):
            d, f = np.linspace(-1,1,30), np.linspace(-1,1,30)
            p = self.bow_pressure.target
            cartprod = lambda a,b: np.array([(i,p,j) for x,y in zip(*np.meshgrid(a,b))
                                             for i,j in zip(x,y)])
            grid = cartprod(d,f)
            self.dvfgrid_o = grid[:,:[2,1][self.veldiff_only]]
            self.dvfgrid_a = grid[:,2:]
        # x = np.random.uniform(0,1,(30*30,np.shape(self.action_space.low)[0]-1))
        x = np.zeros((30*30,np.shape(self.action_space.low)[0]-1))
        # y = np.random.uniform(-1,1,(30*30,np.shape(self.observation_space.low)[0]-3))
        y = np.zeros((30*30,np.shape(self.observation_space.low)[0]-2))
        r = critic.predict(np.hstack([self.dvfgrid_o,y]),
                           np.hstack([self.dvfgrid_a,x])).reshape((30,30))
        ax.imshow(r, origin='lower', extent=[-1,1,-1,1], aspect='auto')

        # ax = fig.add_subplot(4,3,11) # reward by dv
        # ax.plot(o_batch[:,0], y_i, 'xb', alpha=0.5)
        # ax.plot(o_batch[:,0], predicted_q_value, 'xr', alpha=0.5)
        # s = np.vstack([dv[50:],[0.1]*50]).T
        # ax.plot(dv[50:], critic.predict(s, actor.predict(s)), 'g', alpha=0.4)
        # ax.set_xlim(-1,1)
        # ax.set_ylim(-1,1)

        ax = fig.add_subplot(4,3,11) # reward by dv/f pair in batch
        
        #ax.scatter(o_batch[:,0], a_batch[:,0], c=r_batch)

        # d = np.array([b[0][0] for b in buffer])
        # f = np.array([b[1][0] for b in buffer])
        # r = np.array([b[3][0] for b in buffer])
        # print(r.shape)
        # ax.scatter(d, f, c=r, marker='.', alpha=0.003)

        # a 2d histogram incrementally generated during step by adding
        # each sample that is added to the replay buffer
        ax.imshow(histogram.T, origin='lower', extent=[-1,1,-1,1], aspect='auto')

        fig.suptitle('Episode %d, frame %d\nBow velocity %0.04f, bow pressure %0.04f\n%s'%(
            episode, step, self.target_bow_velocity.state, self.bow_pressure.state,
            args['description']))

        fig.savefig('now.png')
        fig.savefig(os.path.join(args['monitor_dir'],'frame%06d.png'%self.frame_counter))
        fig.savefig(os.path.join(args['monitor_dir'],'last.svg'))
        self.frame_counter += 1



class BowingCpp(Bowing):
    def __init__(self, fundamental_freq, window_width=2048, hop_width=64,
                 sample_rate=48000, initial_action=None, num_modes=None,
                 distributed_modal_forces=False, predefined_distribution=False,
                 veldiff_only=True):
        super().__init__(fundamental_freq, window_width, hop_width,
                         sample_rate, initial_action, num_modes,
                         distributed_modal_forces,
                         predefined_distribution, veldiff_only)
        print('distributed_modal_forces', self.distributed_modal_forces)
        print('predefined_distribution', self.predefined_distribution)
        print('veldiff_only', self.veldiff_only)

        import bowing_loop
        self.cpp_inner_loop = bowing_loop.inner_loop
        self.set_layer_weights = bowing_loop.set_layer_weights
        self.run_weight_set = bowing_loop.run_weight_set
        self.get_buffer = lambda: np.reshape(bowing_loop.get_buffer(), (-1,))

    def inner_loop(self, actor, use_noisy_predict=False, noise=None, steps=256, out_file=None):
        a = [0]*self.action_space.shape[0]
        a_clean, a_noisy = [a], [a]

        # Pass the weights to C++
        clean_weights = actor.sess.run(actor.clean_weights)
        [self.set_layer_weights(0, n, w, b) for n,(w,b) in enumerate(clean_weights)]
        if use_noisy_predict:
            noisy_weights = actor.sess.run(actor.noisy_weights)
            [self.set_layer_weights(1, n, w, b) for n,(w,b) in enumerate(noisy_weights)]

        obs = np.reshape(self.observe(), (1,-1))

        # Must use an empty array because passing None doesn't work with pybind11 and Eigen
        if noise is None:
            trace = self.cpp_inner_loop(use_noisy_predict, [], steps)
        else:
            trace = self.cpp_inner_loop(use_noisy_predict, noise, steps)

        a_dim = self.action_space.shape[0]
        o_dim = self.observation_space.shape[0]
        step_trace = list(zip(trace[:,:o_dim],
                              trace[:,o_dim:o_dim+a_dim],
                              trace[:,o_dim+a_dim:]))

        return step_trace, a_clean, a_noisy



def view_bowing_policy(policy=None):
    b = Bowing(220.0, num_modes=100)
    if policy is None:
        policy = b.perfect_policy
    dv = np.linspace(-10,10,1000)
    p = np.ones(dv.shape[0])*0.2
    #f = np.sum(policy(np.vstack([dv,p]).T), axis=1) # if outputting forces
    f = policy(np.vstack([dv,p]).T)[:,0] # if outputting force and dist
    from matplotlib import pyplot as plt
    plt.figure(2).clear()
    plt.clf()
    plt.plot(dv,f)

def test_bowing():
    b = Bowing(110.0, num_modes=10, distributed_modal_forces=True,
               predefined_distribution=False, veldiff_only=False)
    steps = 48000
    rv = np.zeros(steps)
    bv = np.zeros(steps)
    bp = np.zeros(steps)
    dv = np.zeros(steps)
    for k in range(steps):
        dv[k], bp[k] = b.observe()
        rv[k] = b.step_sample(b.perfect_policy([b.observe()])[0])
        bv[k] = b.bow.v
    with open('out.dat','w') as out:
        print('; Sample Rate 48000', file=out)
        print('; Channels 1', file=out)
        m = rv.max()*1.2
        for i in range(steps):
            print(' %0.08f %0.08g'%(i/48000.0, rv[i]/m), file=out)

    plt.figure(1).clear()
    plt.subplot(421)
    plt.plot(rv, label='resonator vel')
    plt.legend()
    plt.subplot(423)
    plt.plot(bv, label='bow vel')
    plt.legend()
    plt.ylim(bv[1000:].min(), bv[1000:].max())
    plt.subplot(422)
    sig = rv[-b.NWINDOW:]
    f = (np.arange(b.NWINDOW//2+1)/(b.NWINDOW//2+1)*(b.fs/2))[:b.NFFT]
    spec = b.log_spectrum(sig)
    plt.plot(f, spec, label='resonator freq resp')
    fntime, fnspec = ['bowing_%02d_modes_%s.txt'%(b.NFREQS,s) for s in ('time','spec')]
    # np.savetxt(fntime, sig, delimiter=',')
    # np.savetxt(fnspec, spec, delimiter=',')
    # print('Wrote',fnspec,'and',fntime)
    plt.legend()
    plt.subplot(424)
    plt.plot(np.sum(b.rf_trace, axis=1), label='resonator action force')
    plt.ylim(np.min(b.rf_trace[1000:]), np.max(b.rf_trace[1000:]))
    plt.legend()
    plt.subplot(425)
    plt.plot(b.baf_trace, label='bow action force')
    plt.ylim(np.min(b.baf_trace[1000:]), np.max(b.baf_trace[1000:]))
    plt.legend()
    plt.subplot(426)
    plt.plot(b.blf_trace, label='bow loading force')
    plt.ylim(np.min(b.blf_trace[1000:]), np.max(b.blf_trace[1000:]))
    plt.legend()
    plt.subplot(427)
    plt.plot(dv, label='velocity diff')
    plt.ylim(np.min(dv[1000:]), np.max(dv[1000:]))
    plt.legend()
    plt.subplot(428)
    btf = np.array(b.baf_trace) + b.blf_trace
    plt.plot(btf, label='bow total force')
    plt.ylim(np.min(btf[1000:]), np.max(btf[1000:]))
    # plt.scatter(dv[1000:], b.baf_trace[1000:], marker='.')
    plt.legend()

if False:
    from matplotlib import pyplot as plt
    r1 = ResonatorBank([1.0, 1.0], [300, 600], 0.5, [0.1], 48000)
    r2 = ResonatorBank([0.1, 0.3], [300, 600], 0.5, [0.1], 48000)
    N = 1000
    y1, y2 = np.zeros((2,N))
    r1.step(np.array([1/0.1, 1/0.3]))
    r2.step(1.0)
    for i in range(N):
        y1[i] = r1.step(0.0)
        y2[i] = r2.step(0.0)
    plt.plot(y1)
    plt.plot(y2)
    plt.show()

if True and __name__=='__main__':
    from matplotlib import pyplot as plt
    # test_resonator_bank()
    view_bowing_policy()
    # test_bowing()
    plt.show()
