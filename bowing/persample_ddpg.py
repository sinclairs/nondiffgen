""" 
Adapted from Patrick Emami's
Implementation of DDPG - Deep Deterministic Policy Gradient
"""
import tensorflow as tf
import numpy as np
import gym
from gym import wrappers
import tflearn
import sys, argparse
import pprint as pp

from replay_buffer import ReplayBuffer

import importlib
import persample_env
importlib.reload(persample_env)
import autogan
importlib.reload(autogan)

# ===========================
#   Actor and Critic DNNs
# ===========================

class ActorNetwork(object):
    """
    Input to the network is the state, output is the action
    under a deterministic policy.

    The output layer activation is a tanh to keep the action
    between -action_bound and action_bound
    """

    def __init__(self, sess, state_dim, state_bound, action_dim, action_bound,
                 learning_rate, tau, batch_size):
        self.sess = sess
        self.s_dim = state_dim
        self.a_dim = action_dim
        self.state_bound = state_bound
        self.action_bound = action_bound
        self.learning_rate = learning_rate
        self.tau = tau
        self.batch_size = batch_size

        # Actor Network
        nvar = len(tf.trainable_variables())
        self.inputs, self.out, self.scaled_out = self.create_actor_network()

        self.network_params = tf.trainable_variables()[nvar:]

        # Target Network
        self.target_inputs, self.target_out, self.target_scaled_out = self.create_actor_network()

        self.target_network_params = tf.trainable_variables()[
            len(self.network_params)+nvar:]

        # Op for periodically updating target network with online network
        # weights
        self.update_target_network_params = \
            [self.target_network_params[i].assign(tf.multiply(self.network_params[i], self.tau) +
                                                  tf.multiply(self.target_network_params[i], 1. - self.tau))
                for i in range(len(self.target_network_params))]

        # This gradient will be provided by the critic network
        self.action_gradient = tf.placeholder(tf.float32, [None, self.a_dim])

        # Combine the gradients here
        self.unnormalized_actor_gradients = tf.gradients(
            self.scaled_out, self.network_params, -self.action_gradient)
        self.actor_gradients = list(map(lambda x: tf.div(x, self.batch_size), self.unnormalized_actor_gradients))

        # Optimization Op
        self.optimize = tf.train.AdamOptimizer(self.learning_rate).\
            apply_gradients(zip(self.actor_gradients, self.network_params))

        self.num_trainable_vars = len(
            self.network_params) + len(self.target_network_params)

    def create_actor_network(self):
        inputs = tflearn.input_data(shape=[None, self.s_dim])#*0
        net = (inputs - self.state_bound[0]) / (self.state_bound[1] - self.state_bound[0])*2-1
        net = tflearn.fully_connected(net, 400)
        net = tflearn.layers.normalization.batch_normalization(net)
        net = tflearn.activations.relu(net)
        net = tflearn.fully_connected(net, 300)
        net = tflearn.layers.normalization.batch_normalization(net)
        net = tflearn.activations.relu(net)
        # Final layer weights are init to Uniform[-3e-3, 3e-3]
        w_init = tflearn.initializations.uniform(minval=-0.003, maxval=0.003)
        out = tflearn.fully_connected(
            net, self.a_dim, activation='sigmoid', weights_init=w_init)
        # Scale output to -action_bound to action_bound
        scaled_out = (tf.multiply(out, (self.action_bound[1]-self.action_bound[0]))
                      + self.action_bound[0])
        return inputs, out, scaled_out

    def train(self, inputs, a_gradient):
        self.sess.run(self.optimize, feed_dict={
            self.inputs: inputs,
            self.action_gradient: a_gradient
        })

    def predict(self, inputs):
        return self.sess.run(self.scaled_out, feed_dict={
            self.inputs: inputs
        })

    def predict_target(self, inputs):
        return self.sess.run(self.target_scaled_out, feed_dict={
            self.target_inputs: inputs
        })

    def update_target_network(self):
        self.sess.run(self.update_target_network_params)

    def get_num_trainable_vars(self):
        return self.num_trainable_vars


class CriticNetwork(object):
    """
    Input to the network is the state and action, output is Q(s,a).
    The action must be obtained from the output of the Actor network.

    """

    def __init__(self, sess, state_dim, state_bound, action_dim, action_bound,
                 learning_rate, tau, gamma, num_actor_vars):
        self.sess = sess
        self.s_dim = state_dim
        self.a_dim = action_dim
        self.state_bound = state_bound
        self.action_bound = action_bound
        self.learning_rate = learning_rate
        self.tau = tau
        self.gamma = gamma

        # Create the critic network
        nvar = len(tf.trainable_variables())
        self.inputs, self.action, self.out = self.create_critic_network()

        self.network_params = tf.trainable_variables()[nvar:]

        # Target Network
        self.target_inputs, self.target_action, self.target_out = self.create_critic_network()

        self.target_network_params = tf.trainable_variables()[(len(self.network_params) + nvar):]

        # Op for periodically updating target network with online network
        # weights with regularization
        self.update_target_network_params = \
            [self.target_network_params[i].assign(tf.multiply(self.network_params[i], self.tau) \
            + tf.multiply(self.target_network_params[i], 1. - self.tau))
                for i in range(len(self.target_network_params))]

        # Network target (y_i)
        self.predicted_q_value = tf.placeholder(tf.float32, [None, 1])

        # Define loss and optimization Op
        self.loss = tflearn.mean_square(self.predicted_q_value, self.out)
        self.optimize = tf.train.AdamOptimizer(
            self.learning_rate).minimize(self.loss)

        # Get the gradient of the net w.r.t. the action.
        # For each action in the minibatch (i.e., for each x in xs),
        # this will sum up the gradients of each critic output in the minibatch
        # w.r.t. that action. Each output is independent of all
        # actions except for one.
        self.action_grads = tf.gradients(self.out, self.action)

    def create_critic_network(self):
        inputs = tflearn.input_data(shape=[None, self.s_dim])#*0
        action = tflearn.input_data(shape=[None, self.a_dim])
        scaled_act = (action - self.action_bound[0])*2/(self.action_bound[1]-self.action_bound[0])-1

        net = tflearn.fully_connected(inputs, 400)
        net = tflearn.layers.normalization.batch_normalization(net)
        net = tflearn.activations.relu(net)

        # Add the action tensor in the 2nd hidden layer
        # Use two temp layers to get the corresponding weights and biases
        t1 = tflearn.fully_connected(net, 300)
        t2 = tflearn.fully_connected(scaled_act, 300, activation='relu')
        if 't2' not in dir(self):
            self.t2 = t2
        # t2 = tflearn.layers.normalization.batch_normalization(t2)
        # t2 = tflearn.fully_connected(t2, 300, activation='relu')

        net = tflearn.activation(
            tf.matmul(net, t1.W) + tf.matmul(scaled_act, t2.W) + t2.b, activation='relu')

        # linear layer connected to 1 output representing Q(s,a)
        # Weights are init to Uniform[-3e-3, 3e-3]
        w_init = tflearn.initializations.uniform(minval=-0.003, maxval=0.003)
        out = tflearn.fully_connected(net, 1, weights_init=w_init)
        return inputs, action, out

    def train(self, inputs, action, predicted_q_value):
        a,b= self.sess.run([self.out, self.optimize], feed_dict={
            self.inputs: inputs,
            self.action: action,
            self.predicted_q_value: predicted_q_value
        })

        return a,b

    def predict(self, inputs, action):
        return self.sess.run(self.out, feed_dict={
            self.inputs: inputs,
            self.action: action
        })

    def predict_target(self, inputs, action):
        return self.sess.run(self.target_out, feed_dict={
            self.target_inputs: inputs,
            self.target_action: action
        })

    def action_gradients(self, inputs, actions):
        return self.sess.run(self.action_grads, feed_dict={
            self.inputs: inputs,
            self.action: actions
        })

    def update_target_network(self):
        self.sess.run(self.update_target_network_params)

# Taken from https://github.com/openai/baselines/blob/master/baselines/ddpg/noise.py, which is
# based on http://math.stackexchange.com/questions/1287634/implementing-ornstein-uhlenbeck-in-matlab
class OrnsteinUhlenbeckActionNoise:
    def __init__(self, mu, sigma=0.3, theta=.15, dt=1e-2, x0=None):
        self.theta = theta
        self.mu = mu
        self.sigma = sigma
        self.dt = dt
        self.x0 = x0
        self.reset()

    def __call__(self):
        x = self.x_prev + self.theta * (self.mu - self.x_prev) * self.dt + \
                self.sigma * np.sqrt(self.dt) * np.random.normal(size=self.mu.shape)
        self.x_prev = x
        return x

    def reset(self):
        self.x_prev = self.x0 if self.x0 is not None else np.zeros_like(self.mu)

    def __repr__(self):
        return 'OrnsteinUhlenbeckActionNoise(mu={}, sigma={})'.format(self.mu, self.sigma)

# ===========================
#   Tensorflow Summary Ops
# ===========================

def build_summaries():
    episode_reward = tf.Variable(0.)
    tf.summary.scalar("Reward", episode_reward)
    episode_ave_max_q = tf.Variable(0.)
    tf.summary.scalar("Qmax_Value", episode_ave_max_q)

    summary_vars = [episode_reward, episode_ave_max_q]
    summary_ops = tf.summary.merge_all()

    return summary_ops, summary_vars

# ===========================
#   Agent Training
# ===========================

def train(sess, env, args, actor, critic, actor_noise, dist):

    # Set up summary Ops
    summary_ops, summary_vars = build_summaries()

    sess.run(tf.global_variables_initializer())
    writer = tf.summary.FileWriter(args['summary_dir'], sess.graph)

    # Initialize target network weights
    actor.update_target_network()
    critic.update_target_network()

    # Initialize replay memory
    replay_buffer = ReplayBuffer(int(args['buffer_size']), int(args['random_seed']))

    # Needed to enable BatchNorm. 
    # This hurts the performance on Pendulum but could be useful
    # in other environments.
    tflearn.is_training(True)

    action_trace_clean = [list() for _ in range(actor.a_dim)]
    action_trace_noisy = [list() for _ in range(actor.a_dim)]
    reward_trace = []
    out_file = None #open('waveout.dat', 'w')
    out_file_counter = 0
    # print('; Sample Rate 48000', file=out_file)
    # print('; Channels 1', file=out_file)


    for episode in range(int(args['max_episodes'])):

        env.reset()

        ep_reward = 0
        ep_ave_max_q = 0

        for step in range(int(args['max_episode_len'])):

            is_viz_step = step % 100 == 1

            tflearn.is_training(False)

            # Pick a random action offset for each window
            offset_noise = actor_noise() * (env.action_space.high - env.action_space.low)

            # Step forward by two windows + warmup time while applying
            # the policy on each sample.
            # To test: can we get away with a shorter hop size here?
            step_trace = []
            for k in range(env.hop + env.NWINDOW*4):

                # Observe
                o = env.observe()

                # Choose an action
                a = actor.predict([o])[0]
                a_clean = a.copy()

                # Add exploration noise only when not evaluating for visualization
                if not is_viz_step:
                    a += offset_noise

                # Apply limits to action space
                a = (a * np.logical_and(a>=env.action_space.low, a<=env.action_space.high)
                     + env.action_space.high*(a>env.action_space.high)
                     + env.action_space.low*(a<env.action_space.low))

                # Apply action and calculate new sample
                samp = env.step_sample(a)

                # Observe after
                o2 = env.observe()

                # Remember the observation-action-observation tuple;
                # these will be paired with the resulting spectrum and
                # used to train the critic.
                step_trace.append((o, a, o2))

                ## cheating: train on post-filtered actions; although,
                ## this may actually harm the ability to zoom in on
                ## the correct value since variance does not change
                ## the mean, so no need for precision.
                # trace.append((o, [np.abs(env.freq.get_last())], o2))

                # Update previous observation
                o = o2

                # Record a wav file if we are doing that
                if out_file is not None:
                    print('% 20.11g % 20.11f'%(out_file_counter/env.fs, samp), file=out_file)
                    out_file_counter += 1

            [action_trace_clean[l].append(a_clean[l]) for l in range(actor.a_dim)]
            [action_trace_noisy[l].append(a[l]) for l in range(actor.a_dim)]

            # Sample the spectrum after, all samples of window share
            # the spectrum and reward.
            s2 = env.log_spectrum()

            # The current spectrum is considered the "result" of the
            # policy during the last window, thus all
            # observation/action pairs in the trace have the same
            # reward.  We save the spectrum as well as the reward
            # directly since it needs to be recalculated as needed.
            # It must be purely a function of the spectrum, but we
            # recalculate since that function is subject to change as
            # more data becomes available.
            r = -dist.reconstruction_loss(s2.reshape((1,-1)))
            for t in step_trace[-env.NWINDOW:]:
                replay_buffer.add(t[0], t[1], s2, r, t[2])

            # Keep adding experience to the memory until
            # there are at least minibatch size samples
            if replay_buffer.size() > int(args['minibatch_size']):

                # Note change of notation from original source, we use
                # 'o' for observation (intead of calling it state) and
                # use 's' for spectrum (where before it was 'r' for
                # reward, which is calculated below.)
                o_batch, a_batch, s2_batch, t_batch, o2_batch = \
                    replay_buffer.sample_batch(int(args['minibatch_size']))

                # Train distance function.
                tflearn.is_training(True)
                dist.train(s2_batch)

                # Calculate rewards using updated function
                tflearn.is_training(False)
                r_batch = -dist.reconstruction_loss(s2_batch)

                # Calculate targets
                target_q = critic.predict_target(
                    o2_batch, actor.predict_target(o2_batch))

                # always non-terminal so we never give non-discounted estimate
                y_i = r_batch + critic.gamma * target_q[:,0]

                # Update the critic given the targets
                tflearn.is_training(True)
                predicted_q_value, _ = critic.train(
                    o_batch, a_batch, np.reshape(y_i, (int(args['minibatch_size']), 1)))

                ep_ave_max_q += np.amax(predicted_q_value)

                # Update the actor policy using the sampled gradient
                a_outs = actor.predict(o_batch)
                grads = critic.action_gradients(o_batch, a_outs)
                actor.train(o_batch, grads[0])

                # Update target networks
                actor.update_target_network()
                critic.update_target_network()

            tflearn.is_training(False)

            ep_reward += r
            reward_trace.append(r)

            if is_viz_step:

                if args['render_env']:
                    env.render(episode, step, env, dist, actor, critic, s2,
                               [step_trace, reward_trace, action_trace_noisy,
                                action_trace_clean],
                               a_batch, y_i, predicted_q_value)

                summary_str = sess.run(summary_ops, feed_dict={
                    summary_vars[0]: float(ep_reward),
                    summary_vars[1]: float(ep_ave_max_q) / float(step)
                })

                writer.add_summary(summary_str, episode)
                writer.flush()

                print('| Reward: {:d} | Episode: {:d} | Step: {:d} | Qmax: {:.4f}'.
                      format(int(ep_reward), episode, step, (ep_ave_max_q / float(step))))

def main(args, env=None):

    if env is None:
        if hasattr(persample_env, args['env']):
            env = getattr(persample_env, args['env'])()
        else:
            print('No environment.')
            return 1

    with tf.Session() as sess:

        np.random.seed(int(args['random_seed']))
        tf.set_random_seed(int(args['random_seed']))
        env.seed(int(args['random_seed']))

        if env.observation_space:
            state_dim = env.observation_space.shape[0]
            state_bound = env.observation_space.low, env.observation_space.high
        else:
            state_dim = 0
            state_bound = 0, 1

        action_dim = env.action_space.shape[0]
        action_bound = env.action_space.low, env.action_space.high

        # dist = autogan.AutoEncoderDistance(sess, state_dim=env.NFFT, latent_dim=10,
        #                                    learning_rate=0.0001, real_sampler=env.sample_real)
        dist = autogan.LogItakuraSaitoDistance(real_sampler=env.sample_real)
        env.set_dist(dist)

        actor = ActorNetwork(sess,
                             state_dim, state_bound,
                             action_dim, action_bound,
                             float(args['actor_lr']), float(args['tau']),
                             int(args['minibatch_size']))

        critic = CriticNetwork(sess,
                               state_dim, state_bound,
                               action_dim, action_bound,
                               float(args['critic_lr']), float(args['tau']),
                               float(args['gamma']),
                               actor.get_num_trainable_vars())
        
        actor_noise = OrnsteinUhlenbeckActionNoise(mu=np.zeros(action_dim),
                                                   sigma=np.ones(action_dim)*0.1)

        if args['use_gym_monitor']:
            if not args['render_env']:
                env = wrappers.Monitor(
                    env, args['monitor_dir'], video_callable=False, force=True)
            else:
                env = wrappers.Monitor(env, args['monitor_dir'], force=True)

        try:
            train(sess, env, args, actor, critic, actor_noise, dist)
        except KeyboardInterrupt:
            pass

        if args['use_gym_monitor']:
            env.monitor.close()

def parse_args():
    parser = argparse.ArgumentParser(description='provide arguments for DDPG agent')

    # agent parameters
    parser.add_argument('--actor-lr', help='actor network learning rate', default=0.0001)
    parser.add_argument('--critic-lr', help='critic network learning rate', default=0.001)
    parser.add_argument('--gamma', help='discount factor for critic updates', default=0.99)
    parser.add_argument('--tau', help='soft target update parameter', default=0.001)
    parser.add_argument('--buffer-size', help='max size of the replay buffer', default=1000000)
    parser.add_argument('--minibatch-size', help='size of minibatch for minibatch-SGD', default=64)

    # run parameters
    parser.add_argument('--env', help='choose the env', default='FrequencyTest')
    parser.add_argument('--random-seed', help='random seed for repeatability', default=1234)
    parser.add_argument('--max-episodes', help='max num of episodes to do while training', default=50000)
    parser.add_argument('--max-episode-len', help='max length of 1 episode', default=1000)
    parser.add_argument('--render-env', help='render the gym env', action='store_true')
    parser.add_argument('--use-gym-monitor', help='record gym results', action='store_true')
    parser.add_argument('--monitor-dir', help='directory for storing gym results', default='./results/gym_ddpg')
    parser.add_argument('--summary-dir', help='directory for storing tensorboard info', default='./results/tf_ddpg')

    parser.set_defaults(render_env=True)
    #parser.set_defaults(use_gym_monitor=True)
    
    args = vars(parser.parse_args())
    
    pp.pprint(args)
    return args

if __name__ == '__main__':
    sys.exit(main(parse_args()))
