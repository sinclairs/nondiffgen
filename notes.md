
# Notes (19/03/18)

This project is a summary of work intended to investigate methods for
optimisation over non-differentiable objectives, that is to say, I am
interesting in figuring out how to efficiently train non-differntiable
generative models.  The example I am starting with is a basic
"drawing" algorithm, found in `draw-mnist`, which represents the a
brush stroke as several parameters normalized to the range [-1,1].

A series of such strokes drives a drawing algorithm, and I want to
train the strokes to match not just one digit, but all digits in MNIST
(for example), in the style of a neural network (or whatever)
generative model that draws pixels from a distribution -- however, I
don't want it to generate the image on a pixel-per-pixel basis, but
rather have the generation be based on a prior, physical model, which
is hard to express in a differentiable manner.  Thus the brush
strokes, which are represented as a location and linear and angular
velocity, with polynomial representations for the line width, can be
controlled in quite an expressive way, but it turns out to be quite
difficult for an algorithm to guess "just" the parameters needed to
draw, e.g., an 8.

I am starting by applying several non-differentiable optimisation
algorithms to the problem of drawing a single digit; the idea is to
start with a few simple but known algorithms such as genetic
algorithms, particle swarm optimisation, stochastic search, and the
like.  Eventually I want to train it to learn a parameterised model
that can control the brush strokes, conditioned on e.g. which digit I
want and what style it should be drawn in.  Since this is quite
difficult as a gradient-free problem, and even in the gradient case is
likely (I think) to have sharp, local minima, I am curious if a global
optimsation approach might have some advantages if it can be made
efficient.

An idea is to see if it's possible to make use of adversarial methods
to do just that, in the style of a GAN (Generative Adversarial
Network).  Similar tricks from the GAN literature can likely be used
to encourage it to find solutions that avoid mode collapse, etc.  I
surmise that a good deal of that theory is applicable outside the pure
ANN context, and is more about generative models in general, and thus
I'd like to know if I can learn physically-based generative models
using the same tricks.

One thing I found today is that there is a disadvantage to using
pre-made algorithms.  I also added the scipy.optimise toolkit, and now
added PSO via pyswarm (after a not having much success with fst-pso),
but then I realized that I don't have access to population statistics,
etc., which I'll need later.  Therefore, I expect I'll be writing
myself most algorithms or hacking some of this code out of necessity.
So far the genetic algorithm seems to be performing the best, which is
somehow not so surprising, as this particular problem, where the
features to learn are a series of independent instructions, seems
quite nicely adapted with how the cross-over mutation operation works;
for example, one agent might be drawing the bottom of an 8 and the
other the top.

I'll note though that my hope is that by having a physical model
present, I can also end up with a "drawing" agent that has some
physical sense to it eventually.  If I add some energy model for
example, hopefully it would figure out that drawing the top and bottom
of the 8 is much less efficient than drwaing it as two overlapping "S"
strokes, or some such.  I'm thinking perhaps that if the same
network/algorithm is trying to efficiently represent a way to draw a
whole bunch of things, an "energy efficient" model might just pop out
anyway simply due to architectural priors.
