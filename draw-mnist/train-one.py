#!/usr/bin/env python3

import tensorflow as tf
import numpy as np
from matplotlib import pyplot as plt
import time

import brush_vector as bv
import cost_model as cost

import importlib; importlib.reload(cost)

def draw(ims):
    plt.clf()
    N = 1+ims.shape[0]
    plt.subplot(int(np.sqrt(N)),int(np.sqrt(N))+2,1)
    plt.imshow(target, cmap=plt.cm.gray)
    plt.xticks([])
    plt.yticks([])
    for i in range(ims.shape[0]):
        plt.subplot(int(np.sqrt(N)),int(np.sqrt(N))+2,i+2)
        plt.imshow(ims[i], cmap=plt.cm.gray)
        plt.xticks([])
        plt.yticks([])
    plt.draw()
    plt.pause(0.0001)

def train_one_random_walk(cost, N=10000):
    im = np.zeros_like(cost.target)
    strokes = np.random.uniform(-1, 1, size=(5, bv.stroke_param_range.shape[0]))
    bv.draw_stroke_series(im, bv.decode_series(strokes))
    im2 = [im]
    best = cost.cost(im)
    for i in range(N):
        vec = strokes + np.random.uniform(-0.1,0.1, size=strokes.shape)
        im[:,:] = 0
        dec = bv.decode_series(vec)
        bv.draw_stroke_series(im, dec)
        y = cost.cost(im)
        if y <= best:
            best = y
            strokes = vec
            im2[0] = im.copy()
        print(i, i*100/N, y, best)
        if i%100==0:
            draw(im.reshape((-1,)+im.shape))
    return im2[0]
    plt.xticks([])
    plt.yticks([])

def train_one_genetic(cost, population_size=300, survivors=40, generations=100):
    strokes = np.random.uniform(-1, 1, size=(population_size,
                                             5*bv.stroke_param_range.shape[0]))
    new_strokes = np.zeros_like(strokes)
    images = np.zeros((population_size,) + cost.target.shape)
    scores = np.zeros((population_size,), dtype=float)

    n = (population_size - survivors)//4

    for g in range(generations):
        for i in range(population_size):
            images[i][:,:] = 0.0
            bv.draw_stroke_series(images[i],
                                  bv.decode_series(strokes[i].reshape(
                                      (-1,bv.stroke_param_range.shape[0]))))
            scores[i] = cost.cost(images[i], images)
        best = scores.argsort(axis=0)[:survivors]
        if g%10==0:
            draw(images[best])
        print(g, '%%%d'%(g*100//generations), scores[best[0]])
        new_strokes[:survivors,:] = strokes[best,:]
        for i in range(survivors,survivors+n):
            a, b = np.random.randint(0, survivors, size=(2,))
            new_strokes[i,:] = (new_strokes[a] + new_strokes[b]) / 2
        for i in range(survivors+n,survivors+n*2):
            a = np.random.randint(0, survivors)
            new_strokes[i,:] = np.tanh(
                (new_strokes[a] + np.random.uniform(-0.2, 0.2, strokes.shape[1])))
        for i in range(survivors+n*2,survivors+n*3):
            new_strokes[i,:] = np.random.uniform(-1.0, 1.0, strokes.shape[1])
        for i in range(survivors+n*3, population_size):
            a, b = np.random.randint(0, survivors, size=(2,))
            j = np.random.randint(new_strokes.shape[1])
            new_strokes[i,:] = np.hstack([new_strokes[a,:j], new_strokes[b,j:]])
        strokes[:,:] = new_strokes[:,:]
    return images[best[0]]
    plt.xticks([])
    plt.yticks([])

# Not python3 compatible and base packages (fuzzy, antlr3) not up to date :(
# https://github.com/aresio/fst-pso
def train_one_fstpso(cost, population_size=300, survivors=40, generations=100):
    strokes = np.random.uniform(-1, 1, size=(population_size,
                                             5*bv.stroke_param_range.shape[0]))

    def fitness(stroke):
        im = np.zeros(cost.target.shape)
        bv.draw_stroke_series(im,
                              bv.decode_series(strokes[i].reshape(
                                  (-1,bv.stroke_param_range.shape[0]))))
        draw(im)
        return cost.cost(images[i], images)

    dims = strokes.reshape((-1,1)).shape[0]
    import fstpso
    pso = fstpso.FuzzyPSO()
    pso.set_search_space([[-1,1]]*dims)
    pso.set_fitness(fitness)

    result = pso.solve_with_fstpso()
    im = np.zeros(cost.target.shape)
    bv.draw_stroke_series(im,
                          bv.decode_series(strokes[i].reshape(
                              (-1,bv.stroke_param_range.shape[0]))))
    return im


def train_one_pyswarm(cost, population_size=3000, generations=50):
    t = [time.time()]
    def fitness(stroke):
        im = np.zeros(cost.target.shape)
        bv.draw_stroke_series(im,
                              bv.decode_series(stroke.reshape(
                                  (-1,bv.stroke_param_range.shape[0]))))
        if (time.time() - t[0]) > 2:
            t[0] = time.time()
            draw(im.reshape((-1,)+im.shape))
        return cost.cost(im)

    dims = 5*bv.stroke_param_range.shape[0]
    import pyswarm
    stroke, result = pyswarm.pso(fitness, [-1]*dims, [1]*dims,
                                 swarmsize=population_size,
                                 phip=0.5, phig=0.9,
                                 maxiter=generations, debug=True)
    print(result, stroke)
    im = np.zeros(cost.target.shape)
    bv.draw_stroke_series(im,
                          bv.decode_series(stroke.reshape(
                              (-1,bv.stroke_param_range.shape[0]))))
    return im

def train_one_dlib(cost, maxcalls=1000):
    """Train to reproduce one image using dlib's global optimisation
    routine.  Requires a modified dlib to handle 45 parameters."""
    p = 5*bv.stroke_param_range.shape[0]
    count = [0]
    im = np.zeros((2,)+cost.target.shape)
    bester = [float('inf')]
    def dlibcost(p0,p1,p2,p3,p4,p5,p6,p7,p8,
                 p9,p10,p11,p12,p13,p14,p15,p16,p17,
                 p18,p19,p20,p21,p22,p23,p24,p25,p26,
                 p27,p28,p29,p30,p31,p32,p33,p34,p35,
                 p36,p37,p38,p39,p40,p41,p42,p43,p44):
        strokes = np.array([[p0,p1,p2,p3,p4,p5,p6,p7,p8],
                            [p9,p10,p11,p12,p13,p14,p15,p16,p17],
                            [p18,p19,p20,p21,p22,p23,p24,p25,p26],
                            [p27,p28,p29,p30,p31,p32,p33,p34,p35],
                            [p36,p37,p38,p39,p40,p41,p42,p43,p44]])
        im[1,:,:] = 0
        bv.draw_stroke_series(im[1], bv.decode_series(strokes))
        if count[0]%100==0:
            draw(im)
        er = cost.cost(im[1])
        if er < bester[0]:
            bester[0] = er
            im[0] = im[1]
        print(count[0], '%%%d'%(count[0]*100//maxcalls), er, bester[0])
        count[0] += 1
        return er

    import dlib
    strokes, error = dlib.find_min_global(dlibcost, [-1]*p, [1]*p, 1000)
    print(error)
    im = np.zeros_like(target)
    bv.draw_stroke_series(im, bv.decode_series(
        np.array(strokes).reshape((-1,bv.stroke_param_range.shape[0]))))

    return im

def train_one_scipy(cost, maxcalls=1000):
    """Train to reproduce one image using global optimisation functions
    from the scipy.optimize library."""
    p = 5*bv.stroke_param_range.shape[0]
    count = [0]
    im = np.zeros((2,)+cost.target.shape)
    bester = [float('inf')]
    def optcost(p):
        strokes = p.reshape((-1,bv.stroke_param_range.shape[0]))
        im[1,:,:] = 0
        bv.draw_stroke_series(im[1], bv.decode_series(strokes))
        if count[0]%100==0:
            draw(im)
        er = cost.cost(im[1])
        if er < bester[0]:
            bester[0] = er
            im[0] = im[1]
        print(count[0], '%%%d'%(count[0]*100//maxcalls), er, bester[0])
        count[0] += 1
        return er

    import scipy.optimize as opt
        # - 'Nelder-Mead' :ref:`(see here) <optimize.minimize-neldermead>`
        # - 'Powell'      :ref:`(see here) <optimize.minimize-powell>`
        # - 'CG'          :ref:`(see here) <optimize.minimize-cg>`
        # - 'BFGS'        :ref:`(see here) <optimize.minimize-bfgs>`
        # - 'Newton-CG'   :ref:`(see here) <optimize.minimize-newtoncg>`
        # - 'L-BFGS-B'    :ref:`(see here) <optimize.minimize-lbfgsb>`
        # - 'TNC'         :ref:`(see here) <optimize.minimize-tnc>`
        # - 'COBYLA'      :ref:`(see here) <optimize.minimize-cobyla>`
        # - 'SLSQP'       :ref:`(see here) <optimize.minimize-slsqp>`
        # - 'dogleg'      :ref:`(see here) <optimize.minimize-dogleg>`
        # - 'trust-ncg'   :ref:`(see here) <optimize.minimize-trustncg>`
    x = opt.minimize(
        optcost, x0=np.random.uniform(-1,1,size=p),
        bounds=[(-1,1)]*p, method='Nelder-Mead', options={'maxiter': maxcalls})
    # x = opt.basinhopping(
    #     cost, x0=np.random.uniform(-1,1,size=p), niter=maxcalls)
    strokes = x.x
    print(x.fun)
    im = np.zeros_like(target)
    bv.draw_stroke_series(im, bv.decode_series(
        np.array(strokes).reshape((-1,bv.stroke_param_range.shape[0]))))

    return im

def train_one_narrowing(cost, population_size=300, generations=100):
    # stochastic narrowing search -- choose at random but narrow the
    # bounds towards the best point found so far by some percentage at
    # each round
    p = 5*bv.stroke_param_range.shape[0]
    strokes = np.random.uniform(-1, 1, size=(population_size, p))
    im = np.zeros(cost.target.shape)
    def drawcost(strokes):
        im[:,:] = 0.0
        bv.draw_stroke_series(im,
                              bv.decode_series(strokes.reshape(
                                  (-1,bv.stroke_param_range.shape[0]))))
        return cost.cost(im)

    bester = float('inf')
    center = np.array([0.0]*p)
    bound = np.array([1.0]*p)
    precision = 0.01
    scaling = np.exp(np.log(precision)/generations)
    print('scaling =', scaling)
    t = [time.time()]
    for g in range(generations):
        for i in range(population_size):
            strokes = np.tanh(np.random.uniform(-1, 1, size=p)*bound + center)
            er = drawcost(strokes)
            if er < bester:
                bester = er
                best = strokes.copy()
            if (time.time() - t[0]) > 5:
                t[0] = time.time()
                im2 = np.zeros_like(im)
                bv.draw_stroke_series(im2,
                                      bv.decode_series(best.reshape(
                                          (-1,bv.stroke_param_range.shape[0]))))
                draw(np.array([im2, im]))
            print(g, '%%%0d'%(g*100//generations), bester, bound.mean())
        center = best
        bound *= scaling

    print(bester)
    im[:,:] = 0.0
    bv.draw_stroke_series(im,
                          bv.decode_series(best.reshape(
                              (-1,bv.stroke_param_range.shape[0]))))
    return im

if __name__=='__main__':
    # Load training and eval data
    if 'mnist' not in globals():
        mnist = tf.contrib.learn.datasets.load_dataset("mnist")
        train_data = mnist.train.images  # Returns np.array
        train_labels = np.asarray(mnist.train.labels, dtype=np.int32)
        eval_data = mnist.test.images  # Returns np.array
        eval_labels = np.asarray(mnist.test.labels, dtype=np.int32)

    plt.ion()
    fig=plt.figure(1)
    fig.set_rasterized(True)

    target = (mnist.train.images[2000].reshape((28,28)) > 0.5).astype(np.float32)
    #c = cost.TargetMSE(target, regularizers=[cost.NegativeMeanPopulationCost])
    c = cost.TargetMSE(target)
    im = train_one_narrowing(c)
    draw(im.reshape((1,)+im.shape))
    plt.ioff()
    plt.show()
