#!/usr/bin/env python3

# Summary: Extension of the cost model to discriminators, typically
# based on neural networks, in the style of the discrimintor/critic
# half of a GAN-type architecture.

import time
import numpy as np

try:
    import keras
except:
    import tensorflow.contrib.keras as keras
import tensorflow as tf

import neuralnetwork_model as nn
import cost_model as cost

class Critic(cost.Cost):
    """A critic's job is to return a judgement for a sampled population
    compared to a dataset."""
    def __init__(self, dataset, population=None, generator=None,
                 batch_size=100, n_epochs=5):
        """We assume that datset is an unrandomize training set.  Population
        is a matrix of model parameters, generator is a sampler for
        such models (or None if population models are the samples
        themselves), and batch_size and n_epochs are how big the
        batches are during training and how many epochs to pass on the
        dataset.  batch_size is divided into half dataset and half
        generated data.

        """
        super().__init__()
        self.dataset = dataset
        self.ds_index = np.arange(self.dataset.shape[0])
        np.random.shuffle(self.ds_index)
        self.batch_size = np.min([batch_size, self.dataset.shape[0]*2])
        self.n_epochs = n_epochs
        self.ds_counter = 0
        self.epoch_counter = 0
        self.update_counter = -1
        self.time = time.time()
        if population is not None:
            self.batch_size = np.min([batch_size, self.dataset.shape[0]*2,
                                      self.population.shape[0]*2])
            self.update_population(0, population, generator)
        self.batch_data = np.zeros((self.batch_size,) + self.dataset.shape[1:])
        self.batch_target = np.zeros((self.batch_size, 1))

    def prepare_batch_data(self, population, generator=None):
        if generator is None:
            generator = self.generator
        if generator is None:
            assert(generator) # TODO
        else:
            for i in range(self.batch_size//2):
                self.batch_data[i] = generator.sample(
                    population[self.pop_index[self.pop_counter]])[0]
                self.pop_counter += 1
                if self.pop_counter >= population.shape[0]:
                    self.pop_counter = 0
                    np.random.shuffle(self.pop_index)
            for i in range(self.batch_size//2,self.batch_size):
                self.batch_data[i] = self.dataset[self.ds_index[self.ds_counter]]
                self.ds_counter += 1
                if self.ds_counter >= self.dataset.shape[0]:
                    self.ds_counter = 0
                    self.epoch_counter += 1
                    if time.time() - self.time > 2 and hasattr(self, 'last_loss'):
                        print('Epochs:',self.epoch_counter, 'Loss:', self.last_loss)
                        self.time = time.time()
                    np.random.shuffle(self.ds_index)
            self.prepare_batch_target()

    def prepare_batch_target(self):
        pass

    def reset(self):
        pass

    def train_batch(self):
        pass # must set last_loss

    def update_population(self, current_generation, population, generator=None):
        """Update the population and retrain the critic. If generator is
        provided, population will be sampled, otherwise batch_size is
        limited by population size."""
        self.population = population
        self.generator = generator
        self.ds_counter = 0
        self.epoch_counter = 0
        self.update_counter += 1
        if self.update_counter % 1 == 0:
            # if current_generation % 1 == 0:
            #     self.reset()
            # TODO try not resetting the network
            self.pop_index = np.arange(population.shape[0])
            np.random.shuffle(self.pop_index)
            self.pop_counter = 0
            while self.epoch_counter < self.n_epochs:
                self.prepare_batch_data(population)
                self.prepare_batch_target()
                self.train_batch()

    def sample_cost(self, x):
        """Cost of a single sample according to the critic function."""
        pass

class Discriminator(Critic):
    def __init__(self, network_class, dataset,
                 population=None, generator=None,
                 batch_size=100, n_epochs=5, relgan=False):
        self.network_class = network_class
        self.network = None
        self.relgan = relgan
        super().__init__(dataset, population, generator,
                         batch_size, n_epochs)

    def init_network(self):
        if self.relgan==True:
            import tensorflow as tf
            import tensorflow.contrib.keras as keras
            self.network = self.network_class(self.batch_data.shape[1:],
                                              np.array(self.batch_data.shape[1:])//2,
                                              1, outact='linear')
            # source: https://github.com/AlexiaJM/RelativisticGAN
            # paper: https://arxiv.org/abs/1807.00734
            self.network.output_layer = keras.layers.Lambda(
                lambda x: keras.backend.sigmoid(
                    keras.backend.concatenate((
                        x[0:1,:],
                        x[1:tf.maximum(1,keras.backend.shape(x)[0]//2),:]-x[keras.backend.shape(x)[0]//2+1:,:],
                        x[keras.backend.shape(x)[0]//2:,:]-x[:keras.backend.shape(x)[0]//2,:]),
                        axis=0))
                    # (x * self.batch_target) - (x * (1-self.batch_target)))
            )(self.network.output_layer)
            self.network.model = keras.models.Model(self.network.input_layer,
                                                    self.network.output_layer)
        else:
            self.network = self.network_class(self.batch_data.shape[1:],
                                              np.array(self.batch_data.shape[1:])//2,
                                              1, outact='iinear')
        self.network.model.compile(optimizer='adam', loss='binary_crossentropy')

    def prepare_batch_target(self):
        if self.network is None:
            self.init_network()
        self.batch_target[:self.batch_size//2,:] = 1.0 # Fake
        self.batch_target[self.batch_size//2:,:] = 0.0 # Real

    def reset(self):
        if self.network is not None:
            print('reset_weights()')
            self.network.reset_weights()
        #self.network = None

    def train_batch(self):
        self.last_loss = self.network.train(self.batch_data, self.batch_target)
        w = self.network.model.get_weights()
        # print('weights min',[np.min(x) for x in w])
        # print('weights max',[np.max(x) for x in w])
        # print('self.ds_counter:',self.ds_counter,'loss:',self.last_loss)

    def sample_cost(self, x):
        """Cost of a single sample according to the critic function."""
        # Note: should this be done on the mean?
        # TODO Is mean((0.5-x)**2) == (0.5-mean(x))**2 ?
        # return (0.5-self.network.sample(x))**2
        return self.network.sample(x)

class WassersteinCritic(Discriminator):
    def __init__(self, network_class, dataset,
                 population=None, generator=None,
                 batch_size=100, n_epochs=5):
        super().__init__(network_class, dataset, population, generator,
                         batch_size, n_epochs)
        class WeightClip(keras.constraints.Constraint):
            """Clips the weights incident to each hidden unit to be inside a range.
            From: https://stackoverflow.com/questions/42264567/keras-ml-library-how-to-do-weight-clipping-after-gradient-updates-tensorflow-b"""
            def __init__(self, c=2):
                self.c = c
            def __call__(self, p):
                return keras.backend.clip(p, -self.c, self.c)
            def get_config(self):
                return {'name': self.__class__.__name__, 'c': self.c}
        self.weightclip = WeightClip

    def init_network(self):
        self.network = self.network_class(self.batch_data.shape[1:],
                                          np.array(self.batch_data.shape[1:])//2,
                                          1, outact='linear')
        #, Kc=keras.constraints.max_norm(1.0)) # self.weightclip(0.01))
        # self.network.model.compile(optimizer='adam',
        #                            loss='mse')
        #loss='binary_crossentropy')
        # https://gist.github.com/zhaoyj1122/27101f739ce22aaf5a0039cde6ef4b4e
        # basically return mean(y_pred),
        # but with ability to inverse it for minimization (when y_true == -1)
        def wasserstein(y_true, y_pred):
            return keras.backend.mean(y_true * y_pred)
        self.network.model.compile(optimizer='rmsprop', loss=wasserstein)

    def prepare_batch_target(self):
        if self.network is None:
            self.init_network()
        self.batch_target[:self.batch_size//2,:] = 1.0 # Fake
        self.batch_target[self.batch_size//2:,:] = -1.0 # Real

    def train_batch(self):
        w = self.network.model.get_weights()
        # print(np.max([np.max(np.abs(x)) for x in w]))
        self.network.model.set_weights([np.clip(x, -0.01, 0.01) for x in w])
        self.last_loss = self.network.train(self.batch_data, self.batch_target)
        # n = self.batch_data.shape[0]//2
        # self.last_loss = self.network.train(self.batch_data[:n], self.batch_target[:n])
        # print(self.batch_target[n-1:n], self.last_loss, end=' -- ')
        # L = self.network.train(self.batch_data[n:], self.batch_target[n:])
        # print(self.batch_target[n:n+1], L, end = ' -- ')
        # self.last_loss += L
        # print(self.last_loss, np.mean([np.mean(x) for x in w]))

    def sample_cost(self, x):
        """Cost of a single sample according to the critic function."""
        return -self.network.sample(x)

class WassersteinImprovedCritic(Discriminator):
    def init_network(self):
        self.network = self.network_class(self.batch_data.shape[1:],
                                          np.array(self.batch_data.shape[1:])//2,
                                          1)
        def wasserstein(y_true, y_pred):
            return (keras.backend.mean(y_true * y_pred)
                    + self.gradient_penalty_loss(y_true, y_pred, 10))
        self.network.model.compile(optimizer='adam', loss=wasserstein)

    def gradient_penalty_loss(self, y_true, y_pred, averaged_samples):
        """
        Computes gradient penalty based on prediction and weighted real / fake samples
        https://github.com/eriklindernoren/Keras-GAN/blob/master/improved_wgan/improved_wgan.py
        """
        gradients = keras.backend.gradients(y_pred, averaged_samples)[0]
        # compute the euclidean norm by squaring ...
        gradients_sqr = keras.backend.square(gradients)
        #   ... summing over the rows ...
        gradients_sqr_sum = keras.backend.sum(gradients_sqr,
                                  axis=np.arange(1, len(gradients_sqr.shape)))
        #   ... and sqrt
        gradient_l2_norm = keras.backend.sqrt(gradients_sqr_sum)
        # compute lambda * (1 - ||grad||)^2 still for each single sample
        gradient_penalty = keras.backend.square(1 - gradient_l2_norm)
        # return the mean as loss over all the batch samples
        return keras.backend.mean(gradient_penalty)

    def train_batch(self):
        self.last_loss = self.network.train(self.batch_data,
                                            [self.batch_target,
                                             np.zeros(self.batch_target.shape)])

if __name__=='__main__':
    from matplotlib import pyplot as plt
    x1 = np.random.normal(1,1,size=(1000,2))
    x2 = np.random.normal(-1,1,size=(1000,2))
    x = np.vstack([x1,x2])
    i = np.arange(x2.shape[0])
    np.random.shuffle(i)
    train = i[:i.shape[0]//2]
    test = i[i.shape[0]//2:]

    import generator_model as gen
    g = gen.IdentityModelGenerator()
    critic = WassersteinCritic(FullyConnectedNetwork, x1)
    critic.update_population(0, x2[train], g)

    x3 = np.vstack([x1[::2],x2[test]])
    y = np.array([critic.cost(i) for i in x3])

    plt.figure(1).clear()
    plt.subplot(121)
    plt.scatter(x1[:,0], x1[:,1], c='r', marker='.')
    plt.scatter(x2[train,0], x2[train,1], c='b', marker='.')
    plt.subplot(122)
    plt.scatter(x3[:,0], x3[:,1], c=y, marker='.')
    plt.show()
