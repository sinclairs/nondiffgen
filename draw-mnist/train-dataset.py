#!/usr/bin/env python3

import tensorflow as tf
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import time, sys, json

import brush_vector as bv
import cost_model as cost
import generator_model as gen
import optimiser_model as opt
import critic_model as critic
import neuralnetwork_model as nn

import importlib
importlib.reload(cost)
importlib.reload(gen)
importlib.reload(bv)
importlib.reload(opt)
importlib.reload(critic)

def draw(ims, target=None, examples=None):
    plt.clf()
    N = ims.shape[0] + (target is not None)

    extra = 0
    if examples is not None: extra = 2
    if target is not None:
        plt.subplot(int(np.sqrt(N)),int(np.sqrt(N))+2,1)
        plt.imshow(target, cmap=plt.cm.gray)
        plt.xticks([])
        plt.yticks([])
    for i in range(ims.shape[0]):
        plt.subplot(int(np.sqrt(N))+extra,int(np.sqrt(N)),i+1+(target is not None))
        plt.imshow(ims[i], cmap=plt.cm.gray)
        plt.xticks([])
        plt.yticks([])
    if examples is not None:
        j = (int(np.sqrt(N))+extra)//2
        plt.subplot(j,1,j)
        im = np.zeros((300,1500))
        h,w = 300-28, 1500-28
        mx = np.max(examples[1])
        mn = np.min(examples[1])
        if mn==mx: mx+=0.00001
        for img,score,label in zip(*examples):
            x = int((score-mn)/(mx-mn)*(w-1))+14
            y = np.random.randint(h/28+1)*28+14
            im[y-14:y+14,x-14:x+14] = img*label
        plt.imshow(im, cmap=plt.cm.terrain)
        plt.yticks([])
        if mx >= 1.1:
            plt.xticks([14,w/(mx-mn)+14,w+14],['%0.3f'%mn,0,'%0.3f'%mx])
        else:
            plt.xticks([14,w+14],['%0.3f'%mn,'%0.3f'%mx])
    plt.draw()
    plt.pause(0.01)

def find_closest_mse(target, dataset):
    scores = np.array([cost.mse(d.reshape(target.shape), target) for d in dataset])
    return scores.argmin()

def find_closest_dict(target, dataset):
    t = tuple(target.reshape((-1,)).tolist())
    if t in dataset:
        return dataset[t]
    scores = np.array([cost.mse(np.array(d).reshape(target.shape), target)
                       for d in dataset])
    return scores.argmin()

def time_find_closest(target, dataset1, dataset2):
    target = (dataset1[2000].reshape((28,28)) > 0.5).astype(np.float32)
    print('hashing..')
    hashed_dataset1 = {tuple((t > 0.5).tolist()): n
                         for n,t in enumerate(dataset1)}
    hashed_dataset2 = {tuple((t > 0.5).tolist()): n
                        for n,t in enumerate(dataset2)}
    print('closest 1..')
    t = time.time()
    for j in range(10):
        i = find_closest(target, dataset2)
    print(i, time.time()-t)
    print('closest 2..')
    t = time.time()
    for j in range(10):
        i = find_closest_dict(target, hashed_dataset2)
    print(i, time.time()-t)

    im = np.zeros((2,)+target.shape)
    im[0,:,:] = target
    im[1,:,:] = train_data[i].reshape(target.shape)
    draw(im)

class DrawingGenerator(gen.Generator):
    """Generator that takes a neural-network-like model of 525 parameters
    (input size 2, hidden layer size 20), computes a series of 5
    strokes, and returns the generated image as a sample."""

    def __init__(self, model_params=None, latent_vector=None):
        super().__init__(model_params, latent_vector)
        self.image = np.zeros((28,28))

    # def strokes(self, model, latent_vector):
    #     """Calculate stroke based on a hidden-layer ANN-like calculation."""
    #     h1 = (np.dot(model[:20].reshape((10,2)), latent_vector)
    #           + model[20:30])
    #     h1 = np.tanh(h1)
    #     # h1 = h1*(h1>0) # relu
    #     h2 = (np.dot(model[30:480].reshape(45,10), h1)
    #           + model[480:525])
    #     h2 = np.tanh(h2)
    #     return h2

    def strokes(self, model, latent_vector):
        """Calculate stroke based on a hidden-layer ANN-like calculation."""
        h1 = (np.dot(model[:20].reshape((10,2)), latent_vector)
              + model[20:30])
        h1 = np.tanh(h1)
        # h1 = h1*(h1>0) # relu
        h2 = (np.dot(model[30:210].reshape(18,10), h1)
              + model[210:210+18])
        h2 = np.tanh(h2)
        return h2

    def take_sample(self, model=None, latent_vector=None):
        if model is None: model = self.model_params
        if latent_vector is None: latent_vector = self.sample_params
        strokes = self.strokes(model, latent_vector)
        self.image[:,:] = 0.0
        bv.draw_stroke_series(self.image, bv.decode_series(
            strokes.reshape((-1,bv.stroke_param_range.shape[0]))))
        return self.image

class NoiseGenerator(gen.Generator):
    """Generator that takes a dataset and generates noisy samples of it
    such that the noise amplitude is determined by a combination of
    model parameters.  Thus the optimiser should attempt to find model
    parameters that minimize the noise in order to match the dataset.
    A scalar sample parameter [0,1] determines the index of the
    dataset value to return."""

    def __init__(self, dataset, model_params=None, sample_params=None):
        super().__init__(model_params, sample_params)
        self.dataset = dataset
        self.image = np.zeros((28,28))

    def take_sample(self, model=None, sample_params=None):
        if model is None: model = self.model_params
        if sample_params is None: sample_params = self.sample_params
        assert(sample_params >= 0.0)
        assert(sample_params < 1.0)
        i = int(sample_params*self.dataset.shape[0])
        sample = self.dataset[i]
        # minimum should be x=-3.0, y=0.01
        prob = 0.005*model**2 + 0.03*model + 0.055
        noise = np.random.uniform(0,1,size=sample.shape)
        return ((sample + (noise < prob)).reshape((28,28)) > 0.5)*1.0

if __name__=='__main__':
    # Load training and eval data
    if 'mnist' not in globals():
        mnist = tf.contrib.learn.datasets.load_dataset("mnist")
        train_data = mnist.train.images  # Returns np.array
        train_labels = np.asarray(mnist.train.labels, dtype=np.int32)
        test_data = mnist.test.images  # Returns np.array
        test_labels = np.asarray(mnist.test.labels, dtype=np.int32)

    # plt.ion()
    fig=plt.figure(1)
    fig.set_rasterized(True)

    #data = ((train_data[train_labels!=1] > 0.5)*1.0)[:5]
    # data = ((train_data[train_labels==8] > 0.5)*1.0)[:500]
    data = (train_data[:500] > 0.5)*1.0

    def make_noise_generator():
        m = gen.ParametersUniform([-10], [10])
        p = gen.ParametersUniform([0], [1])
        # Note: this lambda is a work-around for a mistake in how
        # generators are defined, must fix.  It should be possible to
        # declare without model_params and sample_params and provide
        # these during .sample() instead of creating a generator for a
        # specific set of model params.
        n = lambda m, p: NoiseGenerator(data, m, p)
        return n, m, p

    def make_drawing_generator():
        m = gen.ParametersUniform([-1]*228, [1]*228)
        p = gen.ParametersUniform([-1]*2, [1]*2)
        g = DrawingGenerator
        return g, m, p

    # Test generator
    g, m, p = make_drawing_generator()
    # g, m, p = make_noise_generator()
    plt.figure(1).clear()
    x, y = g(m.sample(), p).sample()
    plt.imshow(x)
    # plt.show()

    # # Test cost against a target
    # target = (mnist.train.images[2000].reshape((28,28)) > 0.5).astype(np.float32)
    # c = cost.ExpectedValueCost(cost.TargetMSE(target))
    # print(c.generator_cost(g(m.sample(), p)))

    # Default configuration
    config = {
        'dist': 'disc', # disc, wass
        'relgan': True, # "relativistic GAN"
        'net': 'cnn', # fc, cnn
        'lc': False, # false=cnn, true=lcnn
        'dense': True,
        'bn': 'false', # last, all, false
        'bias': True,
        'dropout': False,
        'surr': False,
    }

    # Update config from command line
    if len(sys.argv)>1:
        config.update( json.loads(sys.argv[1]) )
    json.dump(config, open('config.txt','w'), indent=True)

    def network(inp_size, mid_size, out_size, outact='linear'):
        if config['net']=='cnn':
            return nn.ConvolutionalNetworkMNIST(
                inp_size, mid_size, out_size, outact=outact,
                with_lc=config['lc'], with_dense=config['dense'],
                with_bn=config['bn'], with_bias=config['bias'],
                with_dropout=config['dropout'])
        elif config['net']=='fc':
            return nn.FullyConnectedNetwork(
                inp_size, mid_size, out_size, outact=outact,
                with_bn=config['bn'], with_bias=config['bias'],
                with_dropout=config['dropout'])

    if config['dist'] == 'wass':
        # Test cost against a Wasserstein distance
        c = critic.WassersteinCritic(network,
                                     data.reshape((-1,28,28)),
                                     n_epochs=1,
                                     batch_size=200)
    elif config['dist'] == 'disc':
        # Test cost against a discriminator target
        c = critic.Discriminator(network,
                                 data.reshape((-1,28,28)),
                                 n_epochs=1,
                                 batch_size=200,
                                 relgan=config['relgan'])

    # pop = np.array([m.sample() for _ in range(10)])
    # c.update_population(pop, g(None, p))

    # Surrogate cost function, we try to map model parameters directly
    # to critic cost and then maybe we can back-propagate it instead
    # of guessing randomly
    surr=None
    if config['surr']:
        surr = cost.NeuralNetworkSurrogateCost(
            nn.FullyConnectedNetworkTF((m.sample().shape[0],),
                                       (m.sample().shape[0]*2,), 1,
                                       with_bias=True, with_bn=False,
                                       with_dropout=False,
                                       act='tanh', outact='linear'),
            train_epochs = 10000,
            minimize_epochs = 10000)

    def drawit1d(generation, idx, model, score, population=None):
        im = np.zeros((8,28,28))
        x = np.linspace(0.0,1.0-1e-14,8)
        for i in range(8):
            p = np.array([x[i]])
            im[i,:,:], _ = g(model, p).sample()
        draw(im)
        plt.savefig('generation-%04d.png'%generation)
        plt.savefig('generation-now.png')
        print(generation, idx, 'best score is',score,'at',model)
    def drawit2d(generation, idx, model, score, population=None):
        print(generation, idx, 'best score is',score)
        # if generation % 5 != 4:
        #     return
        im = np.zeros((16,28,28))
        x,y = np.meshgrid(np.linspace(-1,1,4), np.linspace(-1,1,4))
        closest_mse = []
        for i in range(4):
            for j in range(4):
                n = i*4+j
                par = np.array([x[i,j],y[i,j]])
                im[n,:,:], _ = g(model, par).sample()
                closest_mse.append(np.min([cost.mse(im[n,:,:], d.reshape((28,28)))
                                            for d in data]))
        print('mean_closest_mse,',np.mean(closest_mse))
        Ns = 20
        samples_best = [g(model, p).sample()[0] for _ in range(Ns)]
        if population is not None:
            samples_pop = [g(population[np.random.randint(population.shape[0])], p)
                           .sample()[0] for _ in range(Ns)]
        else:
            samples_pop = []
        n = np.random.randint(data.shape[0]-Ns)
        samples_im = np.vstack([data[n:n+Ns].reshape((-1,28,28)),
                                samples_pop, samples_best])
        scores = [c.cost(i) for i in samples_im]
        labels = np.hstack([np.ones(Ns)*0.2, np.ones(Ns)*0.6, np.ones(Ns)])
        draw(im, target=None, examples=(samples_im, scores, labels))
        plt.suptitle('generation {}, best score is {}'.format(generation, score))
        plt.savefig('generation-%04d.png'%generation)
        plt.savefig('generation-now.png')
    opt = opt.GeneticOptimiser(
        survivors=200,
        model_params=m,
        sample_params=p,
        cost=c,
        generator=g,
        tracer=drawit2d,
        population_size=500,
        surrogate_cost=surr,
        generations=300)
    best = opt.run()
    # print('run:', best)
