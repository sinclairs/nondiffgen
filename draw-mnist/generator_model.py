#!/usr/bin/env python3

import numpy as np
import scipy.stats as stats

# Summary of goals here: Need to represent population of generators
# that can be sampled with parameters taken from a distribution.

class Parameters(object):
    """Represents a distribution of parameters for a generator."""
    def __init__(self, dims):
        self.dims = dims
    def sample(self):
        pass

class ParametersNormal(Parameters):
    """Represents a distribution of parameters for a generator."""

    def __init__(self, means, stds):
        self.means = np.array(means)
        self.stds = np.array(stds)
        assert(self.means.shape == self.stds.shape
               and len(self.means.shape)==1)
        super().__init__(self.means.shape[0])

    def sample(self):
        return np.random.normal(0, 1, self.dims) * self.stds + self.means

class ParametersUniform(Parameters):
    """Represents a distribution of parameters for a generator."""
    def __init__(self, lower, upper):
        self.lower = np.array(lower)
        self.upper = np.array(upper)
        assert(self.lower.shape == self.upper.shape
               and len(self.lower.shape)==1)
        super().__init__(self.lower.shape[0])

    def sample(self):
        return np.random.uniform(0, 1, self.dims)*(self.upper - self.lower)+self.lower

class Generator(object):
    """Represents a single generator that can be sampled."""
    # TODO Generator should take a population and return a set of
    # samples, and cost should take a population and return a set of
    # scores.
    def __init__(self, model_params=None, sample_params=None):
        """Create a generator which is a model with parameters, taking a
        parameter distribution and producing a resulting signal."""
        self.model_params = model_params
        self.sample_params = sample_params
        if self.model_params is not None:
            self.model_params = np.array(model_params)
            assert(len(self.model_params.shape)==1)
        if sample_params is not None:
            if isinstance(sample_params,Parameters):
                self.sample_dims = sample_params.sample().shape[0]
            else:
                self.sample_dims = sample_params.shape[0]

    def sample(self, model=None, params=None):
        """If params is provided, sample this generator with the given
        parameters, which are either an array or a Parameters object
        that generates such an array.  Otherwise, sample using the
        parameters or function already associated with this generator."""
        if model is None:
            model = self.model_params
        if params is None:
            params = self.sample_params
        if isinstance(params,Parameters):
            p = params.sample()
        elif params is None:
            p = None
        else:
            p = np.array(params)
            assert(len(p.shape)==1)
        if hasattr(model,'shape') and len(model.shape)==2:
            return np.array([self.take_sample(m, p) for m in model]), p
        else:
            return self.take_sample(model, p), p

    def take_sample(self, model, params):
        """Return a sample from a single params array."""
        pass

class MeanGenerator(Generator):
    """This generator returns the mean of the parameters."""
    def take_sample(self, model, params):
        return np.mean(params)

class IdentitySampleGenerator(Generator):
    """This generator returns the sample parameters themselves."""
    def take_sample(self, model, params):
        return params

class IdentityModelGenerator(Generator):
    """This generator returns the model parameters themselves."""
    def take_sample(self, model, params):
        return model

class NormalGenerator(Generator):
    """This generator a sample of the normal distribution defined by the
    parameters interpreted as interleaved mean and stds."""
    def take_sample(self, model, params):
        x = stats.norm.ppf(params, loc=model[0], scale=model[1])
        return x

class Population(object):
    """A collection of generator parameters that each have an individual
    cost, but population statistics can also be calculated.  The
    population can be updated."""
    pass # maybe this just needs to be a plain matrix

if __name__=='__main__':
    p = ParametersUniform([0,1], [2,1.5])
    print(p.sample())
    print(p.sample())

    p = ParametersNormal([1,10], [0.1,2])
    print(p.sample())
    print(p.sample())

    g = MeanGenerator(None, p)
    print(g.sample())
    print(g.sample(params=[1,2]))
    print(g.sample(params=p))

    from matplotlib import pyplot as plt
    p = ParametersUniform([0.0], [1.0])
    g = NormalGenerator([2.5,1.2], p)
    plt.figure(1).clear()
    y, x = map(np.array,zip(*[g.sample() for _ in range(1000)]))
    plt.subplot(211)
    plt.plot(x[:,0],y, '.')
    plt.subplot(212)
    plt.hist(y, bins=30)
    plt.show()
