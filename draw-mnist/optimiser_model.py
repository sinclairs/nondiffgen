#!/usr/bin/env python3

import numpy as np
import cost_model as cost
import generator_model as gen

import importlib; importlib.reload(cost); importlib.reload(gen)

# An optimiser may or may not have a concept of a population, but must
# have a cost function and a generator model.  It then evaluates the
# population scores and updates the population parameters accordingly.
# A non-population-based optimiser simply has a population size of 1.

# The optimiser finds a generator's model parameters that minimize a
# cost.  The cost is some aggregate score calculated from the
# generator output while varying the sample parameters.

class Optimiser(object):
    def __init__(self, model_params, sample_params, cost, generator,
                 population_size=300, generations=100, surrogate_cost=None,
                 tracer=None):
        self.model_params = model_params
        dims = model_params.sample().shape[0]
        self.sample_params = sample_params
        self.cost = cost
        self.generator = generator
        self.population_size = population_size
        self.generations = generations
        self.surrogate_cost = surrogate_cost
        self.tracer = tracer
        # TODO make sample() return multiple samples
        self.population = np.array([self.model_params.sample()
                                    for _ in range(self.population_size)])
        self.scores = np.array([float('inf')]*population_size)
        self.best_score = float('inf')
        self.best_index = 0
        # TODO try updating critic with only best-N population
        self.cost.update_population(0, self.population,
                                    self.generator(None, self.sample_params))

    def next_generation(self):
        pass

    def run(self):
        for j in range(self.generations):
            self.current_generation = j
            if j>0:
                self.next_generation()
                n = (self.population.shape[0] - self.survivors)//4
                self.cost.update_population(self.current_generation,
                                            self.population[:self.survivors+n*3],
                                            self.generator(None, self.sample_params))
            for i in range(self.population_size):
                g = self.generator(self.population[i], self.sample_params)
                # e = cost.ExpectedValueCost(self.cost, 3)
                e = cost.MeanCost(self.cost, 10)
                self.scores[i] = e.generator_cost(g)
                # self.scores[i] = self.cost.generator_cost(g)
                #print('self.scores[%d]: %f for %s'%(i,self.scores[i],str(self.population[i])))
                #print('self.scores[%d]: %f'%(i,self.scores[i]))
            best = self.scores.argmin()
            if self.tracer is not None:
                self.tracer(j, best, self.population[best], self.scores[best],
                            self.population)
            if self.surrogate_cost:
                print('retraining surrogate cost')
                self.surrogate_cost.reset()
                self.surrogate_cost.update_population_scores(self.population,
                                                             self.scores)
        return self.population[best], self.scores[best]

class KeepNBestOptimiser(Optimiser):
    def __init__(self, n_best, model_params, sample_params,
                 cost, generator, population_size=300, generations=300,
                 surrogate_cost=None, tracer=None):
        self.n_best = n_best
        super().__init__(model_params, sample_params, cost, generator,
                         population_size, generations, surrogate_cost,
                         tracer)

    def next_generation(self):
        best = []
        pop = np.zeros_like(self.population)
        scores = self.scores.copy()
        for n in range(self.n_best):
            i = scores.argmin()
            best.append(i)
            scores[i] = float('inf')
            pop[n] = self.population[i]
        for j in range(self.n_best, self.population_size):
            pop[j] = self.model_params.sample()
        self.population = pop

class GeneticOptimiser(Optimiser):
    def __init__(self, model_params, sample_params, cost, generator,
                 population_size=300, survivors=10, generations=300,
                 surrogate_cost=None, tracer=None):
        self.survivors = survivors
        super().__init__(model_params, sample_params, cost, generator,
                         population_size, generations, surrogate_cost,
                         tracer)

    def next_generation(self):
        pop = np.zeros_like(self.population)
        best = self.scores.argsort(axis=0)
        n = (self.population.shape[0] - self.survivors)//4

        # Select survivors
        if False:
            # keep best vectors
            pop[:self.survivors] = best[:self.survivors]
        else:
            # keep best vectors subject to swap rule if they are too
            # similar (by euclidean distance for now) -- trying to
            # encourage diversity
            j = 0
            selected = []
            for i in range(pop.shape[0]):
                s = self.population[best[i]]
                this_score = self.scores[best[i]]
                dist = np.array([np.average((s-p)**2) for p in pop[:j]])
                distord = dist.argsort(axis=0)
                if j>0:
                    closest_score = self.scores[selected[distord[0]]]
                    # print('closest {}, closest dist, {:0.3g}, closest_score, {}, this_score, {}'
                    #       .format(selected[distord[0]], dist[distord[0]],
                    #               closest_score, this_score))
                if j==0 or dist[distord[0]] > 1e-4:
                    pop[j] = s
                    selected.append(best[i])
                    j += 1
                    #print('appended',i)
                elif this_score < closest_score:
                    if dist[distord[0]] < 1e-7:
                        print(s)
                        print(pop[distord[0]])
                    pop[distord[0]] = s
                    #print('swapped',i)
                else:
                    pass
                    #print('rejecting',i)
                if j >= self.survivors:
                    print('got enough survivors')
                    break

        # Report overall population and survivor statistics
        pop_std = np.average(self.population.std(axis=0))
        best_std = np.average(pop[:self.survivors].std(axis=0))
        print('mean population std, {:0.3g}, best, {:0.3g}'
              .format(pop_std, best_std), flush=True)

        # random mean best vectors
        for j in range(self.survivors, self.survivors+n):
            a = np.random.randint(0, self.survivors)
            b = np.random.randint(0, self.survivors)
            pop[j] = (pop[a] + pop[b]) / 2
        # random gene-swap best vectors
        for j in range(self.survivors+n, self.survivors+n*2):
            a = np.random.randint(0, self.survivors)
            b = np.random.randint(0, self.survivors)
            # naive
            # k = np.random.randint(self.population.shape[1])
            # pop[j] = np.hstack([pop[a,:k], pop[b,k:]])

            # knowing that we have two matrices and two biases
            # from model in train-dataset.py
            which = np.random.randint(0,4)
            if which==0:
                k = np.random.randint(10)+20
                pop[j] = np.hstack([pop[a,:k], pop[b,k:]])
            elif which==1:
                k = np.random.randint(18)+210
                pop[j] = np.hstack([pop[a,:k], pop[b,k:]])
            elif which==2:
                x,y = np.random.randint(0,10), np.random.randint(0,10)
                k,l = np.random.randint(0,2),  np.random.randint(0,2)
                pop[j] = pop[a]
                g = pop[a][:20].reshape((10,2))
                g[x:y,k:l] = pop[b][:20].reshape((10,2))[x:y,k:l]
                pop[j][:20] = g.reshape(20)
            elif which==3:
                x,y = np.random.randint(0,18), np.random.randint(0,18)
                k,l = np.random.randint(0,10), np.random.randint(0,10)
                pop[j] = a
                g = pop[a][30:210].reshape((18,10))
                g[x:y,k:l] = pop[b][30:210].reshape((18,10))[x:y,k:l]
                pop[j][30:210] = g.reshape(180)

        if False:
            # random mutation one gene
            c = 1.0 - self.current_generation / self.generations
            c = 0.001
            m = self.survivors+n*3
            # if self.current_generation >= 0:
            #     m = self.population.shape[0]
            for j in range(self.survivors+n*2, m):
                a = np.random.randint(0, self.survivors)
                #p = np.random.uniform(0, 1, self.population.shape[1])
                #pop[j] = pop[a] + self.model_params.sample()*c*(p<=0.1)
                p = np.random.randint(0, self.population.shape[1])
                pop[j] = pop[a]
                pop[j][p] += (self.model_params.sample()[p]-pop[j][p])*c
        else:
            # random mutation 10% probability of small change
            m = self.survivors+n*3
            for j in range(self.survivors+n*2, m):
                a = np.random.randint(0, self.survivors)
                v = np.random.uniform(0, 0.01, self.population.shape[1])
                p = np.random.uniform(0, 1, self.population.shape[1]) < 0.01
                pop[j] = pop[a] + v*p

        # surrogate model solutions
        if self.surrogate_cost is not None:
            print('drawing {} minimized surrogates'.format(
                self.population.shape[0] - (self.survivors+n*3)))
            for j in range(self.survivors+n*3, self.population.shape[0]):
                a = np.random.randint(0, self.survivors)
                z, w = self.surrogate_cost.minimize_from(self.population[a])
                pop[j] = w
        # new vectors
        elif self.current_generation > 0:
            for j in range(self.survivors+n*3, self.population.shape[0]):
                pop[j] = self.model_params.sample()
        self.population = pop

if __name__=='__main__':
    class CostQuadratic(cost.Cost):
        def sample_cost(self, x):
            # answer should be x=-0.2, y=2.8
            return 5*x**2 + 2*x + 3
    def printit(gen, idx, model, score):
        print(gen, idx, 'best score for',model,'is',score)
    opt = GeneticOptimiser(
        survivors=10,
        model_params=gen.ParametersUniform([-10.0, 0],[10.0, 10.0]),
        sample_params=gen.ParametersUniform([0.0],[1.0]),
        cost=CostQuadratic(),
        generator=gen.NormalGenerator,
        tracer=printit)
    print('run:',opt.run())
