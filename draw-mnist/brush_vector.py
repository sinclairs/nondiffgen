#!/usr/bin/env python3

import skimage.draw as draw
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm

def gen_stroke_poly(initial_pos, angle, velocity, N, width):
    """Generate the points of a polygon defining a brush stroke
       parameterized by angle and width as polynomials.  Stroke
       direction, velocity, and number of points are also required.
    """
    pos = np.array(initial_pos,dtype=np.float64)[::-1]
    coords = np.zeros((N*2,2))
    for i in range(0,N):
        w = np.polyval(width, i/N)
        ang = np.polyval(angle, i/N)
        vel = np.array([np.cos(ang), np.sin(ang)])*velocity/N
        ortho = vel[::-1] * [1,-1]
        fat = ortho / (np.linalg.norm(ortho)+0.00001) * w
        coords[i] = pos + fat
        coords[N*2-i-1] = pos - fat
        pos += vel
    return coords

def draw_test_stroke(img):
    """Test function for gen_stroke_poly."""
    outline_coords = gen_stroke_poly([10,10],
                                     angle=[0,np.pi*3/7,1],
                                     velocity=30, N=10,
                                     width=[-5,6,1])
    fill_coords = draw.polygon(outline_coords[:,0], outline_coords[:,1],
                               shape=img.shape)
    img[fill_coords] = 1

def draw_stroke_series(img, series):
    """Given a series of stroke specifications as a numpy matrix, call
       gen_stroke_poly for each one and render it."""
    def draw_stroke(row):
        poly_coords = gen_stroke_poly(initial_pos = row[:2],
                                      velocity = row[2],
                                      angle = row[3:6],
                                      N=10,
                                      width=row[6:9])
        fill_coords = draw.polygon(poly_coords[:,0], poly_coords[:,1],
                                   shape=img.shape)
        img[fill_coords] = 1
    for stroke in series:
        draw_stroke(stroke)

# Ranges of each stroke parameter for normalization
stroke_param_range = np.array([[0,28],           # pos x
                               [0,28],           # pos y
                               [0, 100],         # velocity
                               [-10, 10],        # angle ceof^3
                               [-10, 10],        # angle coef^2
                               [-np.pi, np.pi],  # angle offset
                               [-10, 10],        # width ceof^3
                               [-10, 10],        # width coef^2
                               [-10, 10],        # width offset
                               ])

stroke_param_range_width = stroke_param_range[:,1]-stroke_param_range[:,0]

def encode_series(series):
    """Return the stroke series normalized to range [-1,1]"""
    return (series-stroke_param_range[:,0])*2/stroke_param_range_width-1

def decode_series(series):
    """Return stroke series parameters from a normalized range [-1,1]"""
    return (series+1)/2*stroke_param_range_width+stroke_param_range[:,0]

if __name__=='__main__':
    # plt.ion()
    # plt.clf()

    img = np.zeros((74,74))
    draw_test_stroke(img)
    test_series = np.array([[20,40,50,-2,np.pi*7/3,1,-5,6,1],
                            [50,60,50,-4,np.pi*7/3,1,-5,6,1]])
    draw_stroke_series(img, test_series)

    plt.imshow(img, cmap=cm.gray, interpolation='nearest')
    plt.show()
