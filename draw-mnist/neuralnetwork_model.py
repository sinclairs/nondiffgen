#!/usr/bin/env python3

# Summary: Various neural network models needed for critics and
# surrogate costs.

import numpy as np
try:
    import keras
except:
    import tensorflow.contrib.keras as keras
import tensorflow as tf

keras.backend.set_session( tf.Session(
    config=tf.ConfigProto(intra_op_parallelism_threads=1,
                          inter_op_parallelism_threads=1)))

class NeuralNetwork(object):
    def train(self, data, target, epochs=None):
        assert(data.shape[0] == target.shape[0])
        if epochs is None:
            return self.model.train_on_batch(data.reshape(self.input_shape), target)
        else:
            return self.model.fit(data.reshape(self.input_shape), target,
                                  epochs=epochs, verbose=False)

    def sample(self, x):
        return float(self.model.predict(x.reshape(self.input_shape)))

    def reset_weights(self):
        session = keras.backend.get_session()
        for layer in self.model.layers:
            if hasattr(layer, 'kernel_initializer'):
                layer.kernel.initializer.run(session=session)

    def freeze_and_minimize(self, starting_values, epochs=1000):
        """This is special for surrogate models.  We append a new first linear
        layer with one that takes an initial value, which are set as
        weights of the new first layer.  The minimization
        back-propagation can then only change those weights, while the
        rest of the network is frozen.  This allows to find the local
        minimum closest to the initial values that minimizes the
        output."""
        pass

class FullyConnectedNetwork(NeuralNetwork):
    def __init__(self, input_size, hidden_size, output_size, act='lrelu',
                 outact='sigmoid', Kc=None,
                 with_bn='last', with_bias=False, with_dropout=True):
        import functools
        input_dims = functools.reduce(lambda x,y: x*y, input_size)
        self.input_shape = (-1,input_dims)
        hidden_dims = functools.reduce(lambda x,y: x*y, hidden_size)
        output_dims = output_size
        self.output_shape = (-1,output_dims)
        print('network size', input_dims, hidden_dims, output_dims)
        inp = keras.layers.Input(shape=self.input_shape[1:])
        self.input_layer = inp
        h = keras.layers.Dense(hidden_dims, activation='linear',
                               kernel_constraint=Kc,
                               use_bias=with_bias)
        self.first_hidden_layer = h
        h = h(inp)
        if with_bn=='all' or with_bn=='last':
            h = keras.layers.BatchNormalization(momentum=0.8)(h)
        if act=='lrelu':
            h = keras.layers.LeakyReLU(0.2)(h)
        else:
            h = keras.layers.Activation(act)(h)
        if with_dropout:
            h = keras.layers.Dropout(0.3)(h)
        out = keras.layers.Dense(output_dims, activation=outact,
                                 kernel_constraint=Kc)(h)
        self.output_layer = out
        self.model = keras.models.Model(inp, out)

    def freeze_and_minimize(self, input_weights):
        # Note that this function did NOT work for unknown reasons,
        # weights of the network would change despite setting no
        # non-trainable.  Probably wrong use of Keras but
        # implementation below directly in TensorFlow works fine, so
        # ignore this code for now.

        self.stub_input = keras.layers.Input(shape=(1,))
        # assert(len(input_weights.shape)==1)
        # print(input_weights.shape[1:])
        self.dense_input = keras.layers.Dense(input_weights.shape[0],
                                              activation='linear',
                                              use_bias=False)
        # print(self.model.get_weights())
        inp = self.dense_input(self.stub_input)

        self.new_model = keras.models.Model(self.stub_input, self.model(inp))
        # print('new weights', self.new_model.get_weights()[0].shape)
        # print('input_weights', input_weights.shape)
        #self.new_model.layers[1].set_weights(inp.reshape(1,1,inp.shape))
        # todo do this by layer name?
        self.new_model.layers[1].set_weights(input_weights.reshape(1,1,-1))
        # prop = [input_weights] + self.model.get_weights()
        # print('proposing', [x.shape for x in prop])
        # self.new_model.set_weights(prop)

        # freeze shared model weights
        self.model.trainable = False

        self.new_model.compile(optimizer='adam', loss='mse')


class FullyConnectedNetworkTF(NeuralNetwork):
    """Reimplementation of a dense network in TF.  This is a bit messy but
    layer freezing did not seem to work well in Keras; in principle it
    is possible with the functional API, but did not work, to be
    investigated.  This is thus a bit messy since we supply a fake
    Keras-like model class."""
    def __init__(self, input_size, hidden_size, output_size, act='lrelu',
                 outact='sigmoid', Kc=None,
                 with_bn='last', with_bias=False, with_dropout=True):
        import functools
        input_dims = functools.reduce(lambda x,y: x*y, input_size)
        input_shape = self.input_shape = (-1,input_dims)
        hidden_dims = functools.reduce(lambda x,y: x*y, hidden_size)
        output_dims = output_size
        output_shape = self.output_shape = (-1,output_dims)
        print('network size', input_dims, hidden_dims, output_dims)
        with tf.variable_scope('tf', reuse=tf.AUTO_REUSE) as scope:
            inp = self.inp = tf.placeholder(tf.float32, name='input',
                                            shape=(None,input_dims))
            target = self.target = tf.placeholder(tf.float32, name='target',
                                                  shape=(None,output_size))
            w1 = tf.get_variable('w1', shape=(input_dims,hidden_dims), dtype=tf.float32)
            b1 = tf.get_variable('b1', shape=hidden_dims, dtype=tf.float32)
            w2 = tf.get_variable('w2', shape=(hidden_dims,output_dims),dtype=tf.float32)
            b2 = tf.get_variable('b2', shape=output_size, dtype=tf.float32)

            inpw = tf.get_variable('inpw', shape=(1,input_dims), dtype=tf.float32)

            # observation: tanh minimizes more precisely than relu due to curve
            # (on test example in 1D! don't draw conclusions)
            act = getattr(tf.nn, act, lambda x: x)
            outact = getattr(tf.nn, outact, lambda x: x)

            out  = outact(tf.matmul(act(tf.matmul(inp, w1)+b1),w2)+b2)
            outw = outact(tf.matmul(act(tf.matmul(inpw,w1)+b1),w2)+b2)

        class M(object):
            def __init__(self):
                self.session = keras.backend.get_session(); #tf.Session()

            def reset_weights(self):
                self.session.run(tf.global_variables_initializer())

            def compile(self, optimizer=None, loss=None):
                assert loss=='mse'
                self.target_loss = tf.reduce_mean(tf.square(out-target))
                self.target_train = tf.train.AdamOptimizer().minimize(
                    loss=self.target_loss)
                self.minimize_train = tf.train.AdamOptimizer().minimize(
                    loss=outw, var_list=[inpw])

            def fit(self, input_data, target_data, epochs=1, verbose=False):
                for i in range(epochs):
                    x = self.session.run([self.target_train, self.target_loss],
                        feed_dict={inp: input_data.reshape(input_shape),
                                   target: target_data.reshape(output_shape)})
                    if verbose:
                        print(x[1])
                return x[1]

            def predict(self, input_data):
                return self.session.run(out, feed_dict={inp: input_data})

            def minimize(self, input_data, epochs=1, verbose=False):
                self.session.run(inpw.assign(input_data.reshape(1,-1)))
                for i in range(epochs):
                    x = self.session.run([self.minimize_train, outw, inpw, w1])
                    if verbose:
                        print(x[1:])
                return self.session.run([outw, inpw])

            def get_weights(self):
                return self.session.run([inpw,w1,b1,w2,b2])

        self.model = M()

    def reset_weights(self):
        self.model.reset_weights()

    def freeze_and_minimize(self, starting_values, epochs=1000):
        return self.model.minimize(starting_values, epochs=epochs)

class ConvolutionalNetworkMNIST(NeuralNetwork):
    """A CNN for discriminating MNIST, adapted from Keras ACGAN example.
    Removed auxiliary output, and skipping dropout for now.
    https://github.com/keras-team/keras/blob/master/examples/mnist_acgan.py
    See https://arxiv.org/abs/1610.09585 for more details."""

    def __init__(self, input_size, hidden_size, output_size,
                 outact='sigmoid', Kc=None,
                 with_lc=False, with_dense=True, with_bn='last',
                 with_bias=False, with_dropout=True):
        print('network size (CNN)', input_size, '->', output_size)
        self.input_shape = (-1,) + input_size + (1,)
        init = keras.initializers.RandomNormal(mean=0.0, stddev=0.02, seed=None)
        inp = keras.layers.Input(shape=input_size + (1,))
        self.input_layer = inp
        size = 32
        conv = [keras.layers.Conv2D, keras.layers.LocallyConnected2D][with_lc]
        h = conv(size, 3, padding='valid', strides=2,
                 input_shape=input_size, kernel_constraint=Kc,
                 kernel_initializer=init, use_bias=with_bias)(inp)
        print(h)
        if with_bn=='all':
            h = keras.layers.BatchNormalization(momentum=0.8)(h)
        h = keras.layers.LeakyReLU(0.2)(h)
        if with_dropout:
            h = keras.layers.Dropout(0.3)(h)
        h = conv(size*2, 3, padding='valid', strides=1,
                 kernel_constraint=Kc,
                 kernel_initializer=init,
                 use_bias=with_bias)(h)
        print(h)
        if with_bn=='all':
            h = keras.layers.BatchNormalization(momentum=0.8)(h)
        h = keras.layers.LeakyReLU(0.2)(h)
        if with_dropout:
            h = keras.layers.Dropout(0.3)(h)
        h = keras.layers.Conv2D(size*4, 3, padding='valid', strides=2,
                                kernel_constraint=Kc,
                                kernel_initializer=init,
                                use_bias=with_bias)(h)
        print(h)
        if with_bn=='all':
            h = keras.layers.BatchNormalization(momentum=0.8)(h)
        h = keras.layers.LeakyReLU(0.2)(h)
        if with_dropout:
            h = keras.layers.Dropout(0.3)(h)
        h = conv(size*8, 3, padding='valid', strides=1,
                 kernel_constraint=Kc,
                 kernel_initializer=init,
                 use_bias=with_bias)(h)
        print(h)
        if with_bn=='all':
            h = keras.layers.BatchNormalization(momentum=0.8)(h)
        h = keras.layers.LeakyReLU(0.2)(h)
        if with_dropout:
            h = keras.layers.Dropout(0.3)(h)
        h = keras.layers.Flatten()(h)
        if with_dense:
            h = keras.layers.Dense(256, activation=outact,
                                   kernel_constraint=Kc,
                                   kernel_initializer=init,
                                   use_bias=with_bias)(h)
            print(h)
            if with_bn=='last' or with_bn=='all':
                h = keras.layers.BatchNormalization(momentum=0.8)(h)
            h = keras.layers.LeakyReLU(0.2)(h)
            h = keras.layers.Dropout(0.3)(h)
        h = keras.layers.Dense(1, activation=outact,
                               kernel_constraint=Kc,
                               kernel_initializer=init,
                               use_bias=with_bias)(h)
        print(h)
        # idea: mean of squares = radial wasserstein space?
        # out = keras.layers.Lambda(lambda x: keras.backend.mean(x ** 2, axis=1,
        #                                                        keepdims=True))(h)
        # print(out)
        out = h
        self.output_layer = out
        self.model = keras.models.Model(inp, out)
