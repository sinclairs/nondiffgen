#!/usr/bin/env python3

from itertools import product
import json, sys, os

configs = {
    'dist': ('disc', 'wass'),
    'net': ('fc','cnn'),
    'lc': (False, True),
    'dense': (True,), #(False, True),
    'bn': ('last', 'all', False),
    'bias': (False, True),
    'dropout': (False, True),
}

# Return True for configurations that don't make sense
def reject(conf):
    if conf['net'] == 'fc':
        if conf['lc']==True:
            return True
        if conf['dense']==False:
            return True
        if conf['bn']=='all':
            return True
    return False

keys, values = [], []
[(keys.append(k), values.append(v)) for k,v in configs.items()]

all_configs = [dict(zip(keys,c)) for c in product(*values)
               if not reject(dict(zip(keys,c)))]

print(len(all_configs), 'configs', file=sys.stderr)

def configstr(conf):
    s = ''
    for k,v in conf.items():
        if v == True:
            s += '-' + str(k)
        elif v == False:
            pass
        elif k == 'bn':
            s += '-' + str(k) + str(v)
        else:
            s += '-' + str(v)
    return s[1:]

with open('run.sh','w') as out:
    for c in all_configs:
        d = configstr(c)
        print('mkdir -p config-{}; cd config-{}; cp -rl ../MNIST-data .'
              ';  stdbuf -o L ../train-dataset.py \'{}\' | tee output.txt'
              .format(d, d, json.dumps(c)),
              file=out)

os.system('cat run.sh | stdbuf -o L parallel --verbose -j4')
