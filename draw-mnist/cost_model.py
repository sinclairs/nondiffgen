#!/usr/bin/env python3

# Summary of goals here: Need to calculate statistics for a population
# of generators.  Need to be able to calculate the performance of a
# single sample or generator, but also apply reguralization across the
# population or regarding model parameters.  A generator can generate
# a single or many samples while varying the parameters.  A population
# of generators can thus be perturbed and the statistics calculated
# independently and jointly.

import numpy as np

def mse(x, target):
    """Mean-squared error between sample and a target."""
    return ((x-target)**2).mean()

class Cost(object):
    """A class encapsulating a cost function that may or may not consider
    population statistics.  The default implementation takes a target
    signal and returns the mean squared error."""

    def __init__(self, regularizers=None):
        if regularizers is None:
            self.regularizers = []
        else:
            self.regularizers = [r(self) for r in regularizers]

    def sample_target_cost(self, x, target):
        """Calculate the principle cost between a sample and a target signal."""
        assert False, "Not implemented."

    def sample_cost(self, x):
        """Calculate the principle cost associated with a sample."""
        assert False, "Not implemented."

    def cost(self, x, population=None, model=None):
        """Calculate the full cost for a sample including any regularizers."""
        return self.sample_cost(x) + sum([r.cost(x, population, model)
                                          for r in self.regularizers])

    def generator_cost(self, g, population=None, model=None):
        """Calculate the cost for a single sample from a generator with no
        parameters."""
        x,y = g.sample()
        return (self.cost(x, population, model)
                + sum([r.cost(x, population, model)
                       for r in self.regularizers]))

class TargetMSE(Cost):
    """A cost function that is just the mean squared error to a single
    target."""

    def __init__(self, target, regularizers=None):
        """Initialize a cost to a given target."""
        self.target = target
        super().__init__(regularizers)

    def sample_target_cost(self, x, target):
        """Return the MSE of the signal and the target."""
        return mse(x, target)

    def sample_cost(self, x):
        """Return the MSE of the signal and the target."""
        return mse(x, self.target)

class TargetBalancedBinaryMSE(TargetMSE):
    """A cost function to a binary target in which the MSE for on- and
    off-pixels are caluclated independently and summed according to
    their frequency ratio in the target.

    Note, I thought this might help avoid over-penalizing black and
    prefering just an empty the image, but it seems to encourage a
    single stroke that just covers the digit entirely; something
    in-between is needed?  Weigh by Proximity to pixels?
    """

    def __init__(self, target, regularizers=None):
        """Initialize a cost to a given target."""
        super().__init__(target, regularizers)
        self.on_mask = self.target > 0.5
        self.on_ratio = (self.on_mask.sum()
                         / (self.target.shape[0]*self.target.shape[1]))
        self.off_ratio = ((1-self.on_mask).sum()
                          / (self.target.shape[0]*self.target.shape[1]))

    def sample_cost(self, x):
        """Return the balanced binary MSE of the signal and the target."""
        return (mse(x*self.on_mask, self.target*self.on_mask) / self.on_ratio
                + (mse(x*(1-self.on_mask), self.target*(1-self.on_mask))
                   / self.off_ratio))

class ExpectedValueCost(Cost):
    """A cost that calculates the multivariate expected value of several
    samples of a generator driven by some distribution, and returns
    the error to a target, which often would be e.g. TargetMSE(target)."""

    def __init__(self, error, n_samples=10, regularizers=None):
        self.error = error
        self.n_samples = n_samples
        super().__init__(regularizers)

    def generator_cost(self, g, population=None, model=None):
        x = g.sample()[0]
        self.values = [x]
        for i in range(self.n_samples-1):
            self.values.append(g.sample()[0])
            x += self.values[-1]
        x = x/self.n_samples
        return self.error.cost(x, population, model)

class MeanCost(Cost):
    """A cost that is the mean error of several samples."""

    def __init__(self, error, n_samples=10, regularizers=None):
        self.error = error
        self.n_samples = n_samples
        super().__init__(regularizers)

    def generator_cost(self, g, population=None, model=None):
        self.values = []
        x = 0.0
        for i in range(self.n_samples):
            self.values.append(g.sample()[0])
            x += self.error.cost(self.values[-1], population, model)
        x = x/self.n_samples
        return x

class MedianValueCost(Cost):
    """A cost that calculates the median value of several samples of a
    generator driven by some distribution, and returns the error to a
    target, which often would be e.g. TargetMSE(target).  Works for
    univariate samples only."""

    def __init__(self, error, n_samples=10, regularizers=None):
        self.error = error
        self.n_samples = n_samples
        super().__init__(regularizers)

    def generator_cost(self, g, population=None, model=None):
        self.values = []
        for i in range(self.n_samples):
            self.values.append(g.sample()[0])
        return self.error.cost(np.median(self.values), population, model)

class MinimumCost(Cost):
    """A cost that calculates the score of several samples and returns the
    minimum."""

    def __init__(self, error, n_samples=10, regularizers=None):
        self.error = error
        self.n_samples = n_samples
        super().__init__(regularizers)

    def generator_cost(self, g, population=None, model=None):
        self.scores = np.zeros(self.n_samples)
        for i in range(self.n_samples):
            self.scores[i] = self.error.cost(g.sample()[0], population, model)
        return self.scores.min()

class MaximumCost(Cost):
    """A cost that calculates the score of several samples and returns the
    maximum."""

    def __init__(self, error, n_samples=10, regularizers=None):
        self.error = error
        self.n_samples = n_samples
        super().__init__(regularizers)

    def generator_cost(self, g, population=None, model=None):
        self.scores = np.zeros(self.n_samples)
        samples = []
        for i in range(self.n_samples):
            y, p = g.sample()
            samples.append(y)
            self.scores[i] = self.error.cost(y, population, model)
        s = np.std(samples, axis=0).mean()
        return self.scores.max() + self.scores.mean()*0.1 - s*0.1

class Regularizer(Cost):
    """A regularizer is called by a Cost and needs reference to it to
    potentially calculate more costs."""

    def __init__(self, cost):
        self.parent_cost = cost
        # Skip superconstructor, no regularizers for regularizers

class TargetRegularizer(Cost):
    """A regularizer is called by a Cost and needs reference to it to
    potentially calculate more costs.  Cost depends on a target
    signal."""

    def __init__(self, cost):
        self.sample_target_cost = cost.sample_target_cost
        self.target = cost.target
        super().__init__(self, cost)

class NegativeMeanPopulationCost(TargetRegularizer):
    """This regularizer is intended to help with population diverisity by
    subtracting the difference between a particular target and the
    population mean."""

    def cost(self, x, population, model=None):
        """Return the negative of the mean cost across the population of
        signals."""
        return -np.mean([self.sample_target_cost(x, population[i])
                         for i in range(len(population))])

    def generator_cost(self, x, population, cost, model=None):
        """Return the negative of the mean cost across the population of
        generators."""
        return -np.mean([self.sample_target_cost(x.sample()[0],
                                                 population[i].sample()[0])
                         for i in range(len(population))])

class MatchBrightness(TargetRegularizer):
    # ((im[1].mean()-target.mean())**2)

    def cost(self, x, population, model=None):
        """Return the negative of the mean cost across the population of
        signals."""
        return (x.mean() - self.target.mean())**2

    def generator_cost(self, x, population, cost, model=None):
        """Return the negative of the mean cost across the population of
        generators."""
        return (x.sample()[0].mean() - self.target.mean())**2

class SurrogateCost(Cost):
    """This cost takes not the signal, but the model, and attempts to
    predict what the cost will be for it without actually evaluating
    the model.  In most cases it requires training, therefore an
    update function is provided to take population and pre-calculated
    score information."""

    def update_population_scores(self, population, scores):
        pass

    def reset(self):
        pass

    def cost(self, x, population=None, model=None):
        pass

class NeuralNetworkSurrogateCost(SurrogateCost):
    """A surrogate cost using a neural network as the model."""

    def __init__(self, network, regularizers=None,
                 train_epochs=10000, minimize_epochs=10000):
        """Initialize a cost to a given target."""
        super().__init__(regularizers)
        self.network = network
        self.train_epochs = train_epochs
        self.minimize_epochs = minimize_epochs
        self.network.model.compile(optimizer='adam', loss='mse')

    def update_population_scores(self, population, scores):
        return self.network.train(population, scores,
                                  epochs=self.train_epochs)

    def minimize_from(self, start_at, epochs=None):
        if epochs==None: epochs = self.minimize_epochs
        return self.network.freeze_and_minimize(start_at, epochs)

    def reset(self):
        self.network.reset_weights()

    def cost(self, x, population=None, model=None):
        return self.network.model.predict(x)

if __name__=='__main__':
    import neuralnetwork_model as nn
    import importlib; importlib.reload(nn)
    class CostQuadratic(Cost):
        def sample_cost(self, x):
            # answer should be x=-0.2, y=2.8
            return 5*x**2 + 2*x + 3

    # Test the surrogate cost idea:

    # Define a known cost function
    c = CostQuadratic()
    x = np.random.uniform(-1,1,size=(1000,1))
    y = c.cost(x)

    from matplotlib import pyplot as plt
    plt.scatter(x,c.cost(x))

    # Train a surrogate function approximator on some examples of
    # models (population) and their associated costs (scores).
    n = NeuralNetworkSurrogateCost(
        nn.FullyConnectedNetworkTF((1,), (30,), 1,
                                 with_bias=True, act='relu', outact='linear'))

    print('training surrogate model')
    print(n.update_population_scores(x,y))
    plt.scatter(x,n.cost(x), marker='.')

    # Then freeze that network and set a model as the weights for the
    # first layer, and mimize the cost while only adjusting the model
    # layer.  This should predict which model minimizes the surrogate
    # cost starting from some given model.
    print('minimizing input to surrogate')
    z, w = n.network.freeze_and_minimize(np.array([0.8]), epochs=10000)
    print('estimate minimum location is', float(w), 'at', float(z),
          'was expecting -0.2 and 2.8')

    plt.scatter(w,z)
    plt.show()
